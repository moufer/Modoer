<?php
/**
* Project: Modoer(Mudder)
* version: 3.5
* Time: 2007-7-17 @ Create
* ===========================================
* @author moufer<moufer@163.com>
* @copyright Moufer Studio(www.modoer.com)
*/

if(!is_file(__DIR__.'/data/install.lock'))
{
	exit('<a href="install.php">Unsure whether the system of Modoer has been installed or not.</a><br /><br />If it has already been installed,under the folder of ./data , please create a new empty file named as "install.lock".');
}
//application init
require __DIR__.'/core/init.php';

if(S('index_module') && G('subsite')) 
{
	if(($_GET['m']=='index' && $_GET['act']=='index')||(!$_GET['m'] && !$_GET['act']))
	{
		unset($_GET['m'], $_GET['act']);
	}
}
if(S('index_module') && G('subsite') && !isset($_GET['m']) && !isset($_GET['act']))
{
	$m = _get('m', null, '_T');
	if(!$m || !preg_match("/^[a-z]+$/", $m))
	{
		$m = S('index_module');
		if(strposex($m, '/')) list($m,$_GET['act']) = explode('/', $m);
	}
	else
	{
		$m = 'index';
	}
}
else
{
	$m = _get('m', 'index', '_T');
}

if($m && $m != 'index')
{
	// if($_GET['unkown_sldomain'] && !$_G['in_ajax'] && $_GET['name'] != $_GET['unkown_sldomain'])
	// {
	// 	$url = $_GET['Rewrite'] ? ($_CFG['siteurl'] . $_GET['Rewrite']) : ($_GET['m']?url("city:0/$_GET[m]",'',true):$_GET['siteurl']);
	// 	header("HTTP/1.1 301 Moved Permanently");
	// 	header("Location: $url");
	// }

	if(check_module($m))
	{
		$f = $m . DS . 'common.php';
		if(!is_file(MUDDER_MODULE . $f)) show_error(lang('global_file_not_exist', ('./core/modules/' . $f)));
		include MUDDER_MODULE . $f;
	}
	else
	{
		http_404();
	}
}
else
{
	//if($_GET['unkown_sldomain'] && !$_G['in_ajax']) http_404();
	if(!$_G['subsite'] && (!$_GET['act']||$_GET['act']=='index'))
	{
		//不是单一城市，则进入城市选择页面
		if(!$_S_CITY = ms_city::form_single())
		{
			// 自定进入默认城市
			// if(!$_S_CITY = ms_city::form_default())
			// {
					// 手机浏览器首次进入
					if(is_mobile() && check_module('mobile') && S('mobile:auto_enter'))
					{
						header("Location:" . U("city:0/mobile/citys", true));
						exit;
					}
					include MUDDER_CORE  . 'modules' . DS . 'modoer' . DS . 'city.php';
					exit;
					//location('index.php?act=city');
			// }

		}
		$_G['subsite'] = new ms_subsite($_S_CITY);
		$_G['subsite']->init();
		unset($_S_CITY);
	}

	//如果页面当前分站域名或目录，则跳转到正确的域名或目录
	if(S('city_sldomain') && $_G['url']->is_global_url() && !$_GET['act'])
	{
		location(G('subsite')->url());
	}

	$_GET['m'] = $m = 'index';
	$acts = array('ajax','map','seccode','js','search','announcement','city','upload');
	
	if(isset($_GET['act']) && in_array($_GET['act'], $acts))
	{
		include MUDDER_CORE  . 'modules' . DS . 'modoer' . DS . $_GET['act'] . '.php';
		exit;
	} elseif(!$_GET['act'] || $_GET['act'] == 'index') {
		//登录首页，手机模块跳转
		if(is_mobile() && check_module('mobile') && S('mobile:auto_enter') && _cookie('auto_mobile')!='N')
		{
			header("Location:" . U("mobile/index", true));
			exit;
		}
		//page name
		define('SCRIPTNAV', 'index');
		//load template
		include template('modoer_index');
	}
	else
	{
		http_404();
	}
}

/** end **/