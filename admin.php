<?php
/**
* @author moufer<moufer@163.com>
* @copyright Moufer Studio(www.modoer.com)
*/
define('IN_ADMIN', TRUE);
define('SCRIPTNAV', 'admincp');

require './core/init.php';

if(!preg_match("/([a-z0-9\-\.\_])\.php$/i", SELF)) {
    show_error('无效的URL地址：'._T(SELF));
}

if(!ms_domain::compare_with()) {
    location($_G['web']['scheme'].ms_domain::get_domain(S('siteurl')).$_G['web']['reuri']);
}

$_G['loader']->helper('admincp');

define('MUDDER_ADMIN', MUDDER_CORE .  'admin' . DS);

$_G['loader']->model('admin',FALSE);
$_G['admin'] = $_G['loader']->model('cpuser');
$admin =& $_G['admin'];

if(_get('logout')) {
    $admin->logout();
    redirect('admincp_logout_wait', SELF);
}

if(empty($admin->access)) {
    if(!$_POST['loginsubmit']) {
        include MUDDER_ADMIN.'cplogin.inc.php';
        exit;
    } else {
        if($admin->login()) {
            redirect('admincp_login_wait', SELF);
        } else {
            redirect($admin->error());
        }
    }
} elseif($admin->access == '1') {
    //login timeout
    if(!_post('admin_pw') || (md5(_post('admin_pw')) != $admin->password)) {
        include MUDDER_ADMIN.'cplogin.inc.php';
        exit;
    } else {
        if($admin->login()) {
            redirect('admincp_login_wait', SELF);
        } else {
            redirect($admin->error());
        }
    }
} elseif($admin->access == '2') {
    //IP address
    redirect('admincp_login_op_without', SELF.'?logout=yes');
} elseif($admin->access == '3') {
    //User Disabled
    redirect('admincp_cpuser_colsed', SELF.'?logout=yes');
} elseif($admin->access == '4') {
    //User City Access
    redirect(lang('admincp_cpuser_city_access',$_CITY['name']), SELF.'?logout=yes');
}

if(empty($admin->adminid) || $admin->adminid < 0 || !$admin->isLogin) {
    redirect('admincp_not_login', SELF);
}

$module = _input('module');
$act = _input('act');
$in_ajax = _input('in_ajax');

include MUDDER_ADMIN . 'cpframe.inc.php';

/** end **/
?>