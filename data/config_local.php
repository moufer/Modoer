<?php
!defined('IN_MUDDER') && exit('Access Denied');
// 本地开发配置文件
// 自动覆盖config.php的配置参数

// 数据库服务器名字(host:port)
$_G['dns']['dbhost'] = '127.0.0.1';
// 数据库帐号
$_G['dns']['dbuser'] = 'root';
// 数据库密码
$_G['dns']['dbpw'] = 'root';
// 数据库名字
$_G['dns']['dbname'] = 'modoer_mc34';
// 数据表前缀
$_G['dns']['dbpre'] = 'modoer_';

// session实现方式
$_G['session_type'] = 'db'; //db,redis
// 内存缓存工具使用
$_G['cache_type'] = 'file';//file,redis,memcache,apc;
// Redis 配置
$_G['redis']['enabled'] = false;
$_G['redis']['host'] = '127.0.0.1';
$_G['redis']['port'] = 6379;
$_G['redis']['pconnect'] = false;
$_G['redis']['timeout'] = 1;
