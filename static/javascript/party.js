/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
website.party = {};
website.party.apply = {};
website.party.comment = {};

$(document).ready(function() {
    website.party.init();
});

//页面加载后进行JS调整
website.party.init = function() {

	var _detail = {
		init:function() {
			$('a[data-name="party_apply"]').click(function() {
				website.party.apply(this);
				return false;
			});
		}
	};

	if(website.module.page == 'detail') {
        _detail.init();
    }
}

//活动报名
website.party.apply = function(link) {

	var url = '';
	if(typeof(link)=='object') {
		link = $(link);
		var url = link.attr('href').url();
		//link.attr('href','javascript:');
	} else {
		url = link.url();
	}

	var _dialog = {
		dlg:null,
		show:function() {
			$.get(url, { in_ajax:1 }, function(data) {
		        if(data == null) {
					alert('信息读取失败，可能网络忙碌，请稍后尝试。');
		        } else if (data.is_message()) {
		            myAlert(data);
				} else {
					_dialog.dlg = dlgOpen('活动报名', data, 500);
					_dialog.bind(_dialog.dlg.container());
				}
			});
		},
		bind:function(container) {
			container.find('button[name="dosubmit"]').click(function() {
				_dialog.submit(container.find('form'));
			});
		},
		submit:function(form) {
			var post_url = form.attr('action').url();
			//表单增加必要隐藏字段
			form.form_hidden({in_ajax:1, in_json:1, dosubmit:'Y'});
			//提交表单
			$.post(post_url, form.serialize() , function(data) {
				if(data.is_message()) {
					myAlert(data);
				} else {
					_dialog.submit_after(parse_json(data));
				}
			});
		},
		submit_after:function(data) {
			if(data.code==200) {
				alert('报名成功！请继续关注本次活动详情。');
				_dialog.dlg.close(); //关闭对话框
			} else {
				alert(data.message+'('+data.code+')');
			}
		}
	}

	_dialog.show();

}

function party_tab(id) {
	$("#party-tab > li").each(function(i) {
		if(this.id != id) {
			$(this).removeClass('selected');
			$('#'+this.id+'_foo').addClass('none');
		} else {
			$(this).addClass('selected');
			$('#'+this.id+'_foo').removeClass('none');
		}
	});
}

function get_party_comment(partyid,page) {
    if (!is_numeric(partyid)) {
        alert('无效的ID'); 
		return;
    }
	if(!page) page = 1;
	$.post(Url('party/detail/partyid/'+partyid+'/op/comment/page/'+page), 
	{ in_ajax:1 },
	function(result) {
        if(result == null) {
			alert('信息读取失败，可能网络忙碌，请稍后尝试。');
        } else if (result.match(/\{\s+caption:".*",message:".*".*\s*\}/)) {
            myAlert(result);
		} else {
			$('#party-comment-all-foo').html(result);
		}
	});
	return false;
}

function reply_party_comment(commentid,page) {
	$("table tr td div").each(function(i) {
		if(this.className=='reply' && $(this).attr('reply')=='0') {
			$(this).addClass('none');
		}
	});
	var foo = $('#party_reply_'+commentid);
	foo.removeClass('none');
	foo.append($('#party-reply-form-foo'));
	$('#commentid').val(commentid);
	$('#party-reply-form-foo').removeClass('none');
}

function apply_party(partyid) {
	if (!is_numeric(partyid)) {
        alert('无效的ID'); 
		return;
    }
	$.get(Url('party/member/ac/apply/op/apply/id/'+partyid), 
	{ in_ajax:1 },
	function(result) {
        if(result == null) {
			alert('信息读取失败，可能网络忙碌，请稍后尝试。');
        } else if (is_message(result)) {
            myAlert(result);
		} else {
			dlgOpen('活动报名', result, 500);
		}
	});
	return false;
}

//移动层
function party_calendar_show(obj, day, not_move) {
	var s = $(obj);
	if($('#calendar_day_'+day).html().trim()=='') return;
	if($("#tipcalendar")[0] == null) {
		$(document.body).append("<div id=\"tipcalendar\" style=\"position:absolute;left:0;top:0;display:none;\"></div>");
	}
	var t = $("#tipcalendar");
	var one = false;
	s.mousemove(function(e) {
		if(not_move==1 && one) return;
		var mouse = get_mousepos(e);
		t.css("left", mouse.x + 10 + 'px');
		t.css("top", mouse.y + 10 + 'px');
		t.html($('#calendar_day_'+day).html());
		t.css("display", '');
		one = true;
	});
	$('#calendar_day_'+day).mouseout(function() {
		t.css("display", 'none');
		$("#tipcalendar").remove();
	});
}