weixin = {};

//数据新增和编辑对话框互交
weixin.form_dialog = function(options) {

	//默认配置参数
	var _d_options = {
		form:null,	//表单对象：包含完整的表单字段
		values:null,	//编辑值json
		dialog:{	//对话框配置
			width:500	//对话框宽度
		},
		onSetValue:null, //设置编辑值
		onSubmit:null,	//提交表单
		onClose:null	//关闭对话框
	}

	var _my = this;
	var _opts = $.extend({}, _d_options, options);
	var _form = _opts.form;
	var _dialog_id = 'news_'+getRandom();

	var _dialog = null;

	if(_opts.values) {
		setValue();
	}

	//提交操作处理
	_form.submit(function(event) {
		if(_opts.onSubmit) {
			return _opts.onSubmit(_dialog, _form);
		}
		return true;
	});

	function setValue() {
		if(_opts.onSetValue) {
			_opts.onSetValue(_dialog, _form, _opts.values);
		}
	}

	this.open = function() {
		_dialog = dlgOpen(_opts.values?'编辑':'新增', _form.removeClass('none').show(), _opts.dialog.width, 0, _dialog_id);
	}

	this.close = function() {
		_form.remove();
		if(_dialog) _dialog.close();
		if(_opts.onClose) {
			return _opts.onClose();
		}
	}

}

//公众号自定义指令互交
weixin.custom_cmd = function() {

	function switch_msgtype() {
		var type = $('input[name="msgtype"]:checked').val();
		$('.J_msgtype').hide();
		$('.msgtype-'+type).show();
	}

	function submit_check(form) {
		var params = {};
		$.each(form.serializeArray(), function(index, val) {
			var code = "params."+val.name+"="+(val.value?("'"+val.value.trim()+"'"):"''")+";";
			eval(code);
		});
		if(params.set_domain) delete(params.set_domain);
		//表单元素检查
		if(params.Title=='') {
			alert('未填写图文标题。');
			return false;
		} else if(params.Url==''||params.Url=='http://') {
			alert('未填写图文链接。');
			return false;
		}
		return params;
	}

	function news(data) {

		//上下移动排序
		function listorder(item, movetype) {
			if(movetype=='up') {
				var previtem = item.prev();
				if(previtem[0]) previtem.before(item);
			} else {
				var nextitem = item.next();
				if(nextitem[0]) nextitem.after(item);
			}
		}

		//编辑信息条目信息
		function edit(item) {
			var options = {
				values:data,
				form:$('form.J_form').clone(true),
				onSubmit:function(dialog, form) {
					var params = submit_check(form);
					if(!params) return false;
					var newitem = news(params);
					newitem.replaceAll(item);
					dialog.close();
					return false;
				},
				onSetValue:function(dialog, form, values) {
					form.find('[name="Title"]').val(values.Title);
					form.find('[name="Description"]').val(values.Description);
					form.find('[name="Url"]').val(values.Url);
					form.find('[name="picture"]').parents('tr').remove();
					form.append($('<input type="hidden" name="PicUrl" value="'+values.PicUrl+'" />'));
				}
			};

			var dialog = new weixin.form_dialog(options);
			dialog.open();
		}

		function del(item) {
			item.remove();
		}

		var item = $('<li class="msgtype-news-item"></li>').data(data);
		var rich = $('<div class="richtxt"></div>');
		rich.append($('<div class="fl"><img src='+urlroot +'/'+data.PicUrl+' class="richtxt-img" /></div>'));
		var body = $('<div class="richtxt-body"></div>');
		var operation = $('<div class="opt fr"></div>');
		operation.append($('<a class="" href="#">↑</a>').click(function(event) {
			event.preventDefault();
			listorder(item,'up');
		})).append('&nbsp;&nbsp;').append($('<a class="" href="#">↓</a>').click(function(event) {
			event.preventDefault();
			listorder(item,'down');
		})).append($('<a class="blk" href="#">编辑</a>').click(function(event) {
			event.preventDefault();
			edit(item);
		})).append($('<a class="blk" href="#">删除</a>').click(function(event) {
			event.preventDefault();
			del(item);
		}));
		var text = $('<div></div>');
		text.append($('<span class="Title">'+data.Title+'</span>'));
		text.append($('<span class="Description">'+data.Description+'</span>'));
		text.append($('<span class="class="Description"">'+data.Url+'</span>'));

		body.append(operation);
		body.append(text);
		rich.append(body);
		item.append(rich);

		return item;
	}

	var news_group = $('.msgtype-news-group');

	switch_msgtype();

	$('input[name="msgtype"]').click(function(e) {
		switch_msgtype();
	});

	$('#add_news_btn').click(function(event) {
		var options = {
			form:$('form.J_form').clone(true),
			onSubmit:function(dialog, form) {
				var params = submit_check(form);
				if(!params) return false;
				//无刷新上传图片，并获得图片的URL
				var iframe = new $.mframe_form(
					form, {}, {
						onSucceed:function(data) {
							//console.debug(data);
							params.PicUrl = data.trim(); //返回的图片地址
							news_group.append(news(params));
							//console.debug(params);
							dialog.close();
						}
					}
				);
				iframe.submit();
				return false; //不提交表单
			}
		};
		var form_obj = new weixin.form_dialog(options);
		form_obj.open();
	});

	$('form[name="cmd_save"]').submit(function(event) {
		var form = $(this);
		//检验
		if(form.find('[name="keyword"]').val().trim()=='') {
			alert('请填写指令关键字。');
			return false;
		}
		if(form.find('[name="intro"]').val().trim()=='') {
			alert('请填写指令介绍。');
			return false;
		}
		//图文内容，先合并数据，再进行提交
		if(form.find('[name="msgtype"]:checked').val()=='news') {
			var content = new Array();
			form.find('.msgtype-news-group > li.msgtype-news-item').each(function(index, el) {
				$.each($(this).data(), function(key, val) {
					form.append('<input type="hidden" name="content['+index+']['+key+']" value="'+val+'">');
				});
			});		
		} else {
			if(form.find('[name="content"]').val().trim()=='') {
				alert('请填写回复内容。');
				return false;
			}
		}
		return true;
		//检测表单内容
	});

	//拖动排序
	$.getScript(GLOBAL['js_root']+'jquery.dragsort.js', function(data, textStatus) {
		$("ul.msgtype-news-group").dragsort();
	});

	//新增一条导航数据
	this.add_item = function(data) {
		news_group.append(news(data));
	}

}

//微站图片和导航互交
weixin.site_nav_pic_form = function() {

	//添加菜单表单提交
	function submit_check(form) {
		var params = {};
		$.each(form.serializeArray(), function(index, val) {
			var code = "params."+val.name+"="+(val.value?("'"+val.value.trim()+"'"):"''")+";";
			eval(code);
		});
		if(params.set_domain) delete(params.set_domain);
		//表单元素检查
		if(!params.title||params.title=='') {
			alert('未填写菜单标题。');
			return false;
		} else if(!params.url||params.url==''||params.url=='http://') {
			alert('未填写菜单链接。');
			return false;
		}
		return params;
	}

	//生成一个导航项项
	function item(data) {

		var tr = $('<tr></tr>').addClass('J_data_item');
		var img = $('<td></td>').append('<img src="'+urlroot+'/'+data.picurl+'" width="60" width="60" />');
		var title = $('<td></td>').append(data.title+'<br />'+data.url);
		var op = $('<td></td>');
		op.append($('<a href="#">上移</a>').click(function(event) {
			event.preventDefault();
			listorder(tr,'up');
		}));
		op.append('&nbsp;&nbsp;');
		op.append($('<a href="#">下移</a>').click(function(event) {
			event.preventDefault();
			listorder(tr,'down');
		}));
		op.append('<br />');
		op.append($('<a href="#">编辑</a>').click(function(event) {
			event.preventDefault();
			edit();
		}));
		op.append('&nbsp;&nbsp;');
		op.append($('<a href="#">删除</a>').click(function(event) {
			event.preventDefault();
			del();
		}));
		tr.append(img).append(title).append(op).data(data);

		return tr;

		//移动菜单排列顺序
		function listorder(item, movetype) {
			if(movetype=='up') {
				var previtem = item.prev();
				if(previtem[0] && previtem.attr('class')=='J_data_item') previtem.before(item);
			} else {
				var nextitem = item.next();
				if(nextitem[0]) nextitem.after(item);
			}
		}

		//编辑一个导航
		function edit() {

			var options = {
				values:data,
				form:$('form.J_form').clone(true),
				onSubmit:function(dialog, form) {
					var params = submit_check(form);
					if(!params) return false;
					var newitem = item(params);
					newitem.replaceAll(tr);
					dialog.close();
					return false;
				},
				onSetValue:function(dialog, form, values) {
					form.find('[name="title"]').val(values.title);
					form.find('[name="url"]').val(values.url);
					form.find('[name="picture"]').parents('tr').remove();
					form.append($('<input type="hidden" name="picurl" value="'+values.picurl+'" />'));
				}
			};

			var form_obj = new weixin.form_dialog(options);
			form_obj.open();
		}

		//删除一个导航
		function del() {
			tr.remove();
		}
	}

	$('button.J_add_btn').click(function(event) {
		var options = {
			form:$('form.J_form').clone(true),
			onSubmit:function(dialog, form) {
				var params = submit_check(form);
				if(!params) return false;
				//无刷新上传图片，并获得图片的URL
				var iframe = new $.mframe_form(
					form, {}, {
						onSucceed:function(data) {
							//console.debug(data);
							params.picurl = data.trim(); //返回的图片地址
							table.append(item(params));
							//console.debug(params);
							dialog.close();
						}
					}
				);
				iframe.submit();
				return false; //不提交表单
			}
		};
		var form_obj = new weixin.form_dialog(options);
		form_obj.open();
	});

	var nav_form = $('[name="myform"]');
	var table = nav_form.find('table.J_data_table');

	//提交修改
	nav_form.submit(function(event) {
		table.find('tr.J_data_item').each(function(index) {
			$.each($(this).data(), function(key, val) {
				nav_form.append('<input type="hidden" name="array['+index+']['+key+']" value="'+val+'">');
			});
		});
	});

	//新增一条导航数据
	this.add_item = function(data) {
		table.append(item(data));
	}

}

//自定义菜单互交
weixin.custom_menu = function(urls) {

	var _urls = {
		get:'',
		edit:'',
		post:''
	}

	var _menu_num_limit = {
		root:3,
		sub:5
	}

	var _lang = {
		form_dialog_title:"菜单添加/修改",
		form_root_menu_limit: '一级菜单只能添加[num]个。',
		form_sub_menu_limit: '二级级菜单只能添加[num]个。',
		form_name_empty:'请填写菜单名称。',
		form_type_click_key_empty:'请输入或选择指令标识。',
		form_type_view_url_empty:'请输入手机web页面url。',
		form_type_cannot_choose_root:'二级菜单类型不能选择为父菜单类型。'
	};

	// 菜单编辑对话框
	function menu_dialog(options) {

		this.values     = null;
		this.is_root    = true;
		this.form_src   = urls.edit;
		this.fn_result  = null;

		var _dlg        = null;
		var _form       = null;
		var _this       = this;

		var _open = function() {
			//获取菜单编辑表
			$.post(_this.form_src.url(), { in_ajax:1 }, function(data) {

				_dlg    = dlgOpen(_lang.form_dialog_title, data, 450);
				_form   = _dlg.area_obj().find('form');

				_form.find('select[name="type"]').change(function() {
					var type = $(this).val();
					$('div[data-type="option"]').hide();
					$('div[data-name="'+type+'"]').show();
				});

				if(!_this.is_root) {
					_form.find('[name="type"] > option:first').remove();
					_form.find('[name="type"]').change();
				}

				_form.find('[data-type="submit"]').click(function() {
					_submit();
				});

				//赋值
				if(_this.values) {
					_set_values();
				}

			});
		}

		//表单元素赋值
		var _set_values = function() {
			_form.find('[name="name"]').val(_this.values.name);
			_form.find('[name="type"]').val(_this.values.type).change();
			if(_this.values.type=='view') {
				_form.find('[name="url"]').val(_this.values.url);
			} else {
				_form.find('[data-name="'+_this.values.type+'"]').find('[name="key"]').val(_this.values.key);
			}
			_form.find('[data-type="submit"]').text('提交修改');
		}

		var _submit = function() {
			var params = {};
			var form = _dlg.area_obj().find('form');
			params.name = form.find('[name="name"]').val();
			params.type = form.find('[name="type"]').val();
			if(params.type == 'view') {
				params.url = form.find('#type_url').val().trim();
			} else if(params.type != 'root') {
				var id = '#type_'+params.type;
				params.key = form.find(id).val().trim();
			}
			// $.each(form.serializeArray(), function(index, val) {
			//     var code = "params."+val.name+"="+(val.value?("'"+val.value.trim()+"'"):"''")+";";
			//     eval(code);
			// });
			if(params.name=='') {
				alert(_lang.form_name_empty);
				return;
			}
			if(params.type=='url' && params.url=='') {
				alert(_lang.form_type_view_url_empty);
				return;
			} else if(params.type!='url' && params.key=='') {
				alert(_lang.form_type_click_key_empty);
				return;
			}
			if(!_this.is_root && params.type=='root') {
				alert(_lang.form_type_cannot_choose_root);
				return;
			}
			_dlg.close();
			_this.values = params;
			if(_this.fn_result) {
				_this.fn_result(_this.values);
			}
		}

		this.open = function() {
			_open();
		}
	}

	// 菜单列表控件
	function menu_list(container) {
		var _this = this;
		var _container = $(container);

		_item = function() {
			var _mythis = this;
			var item = $('<ul>').addClass('weixin-menu').data('this',this);
			this.addend = function(value, classname) {
				var length = item.find('> li').length;
				var element = $('<li>').html(value).attr('data-index', length);
				if(classname) element.addClass(classname);
				item.append(element);
				return element;
			};
			this.element = function(index) {
				return item.find('> li[data-index="'+index+'"]');
			};
			this.item = function() {
				return item;
			};
			this.remove = function() {
				item.remove();
			};
			this.clear = function() {
				item.empty();
			};
			this.value = function(index, value) {
				_mythis.element(index).html(value);
			}
		}

		_form = function() {
			var form = $('<form>');
			this.add_hidden = function(name,value) {
				form.append($('<input>').attr({type:'hidden',name:name,value:value}));
			}
			this.serialize = function() {
				return form.serialize();
			}
			this.form  = function() {
				return form;
			}
		}

		var _create = {
			add_link:function(title, click, dataName) {
				var a = $('<a>').attr('href','javascript:;').html(title).click(click);
				if(dataName) a.attr('data-name', dataName);
				return a;
			},
			add_item:function(item_cls, values) {
				item_cls.addend(values.name, 'name');
				item_cls.addend(values.type, 'type');

				var value = '-';
				if(values.type == 'view') {
					value = values.url;
				} else if(values.type != 'root') {
					value = values.key;
				}
				item_cls.addend(value, 'value');
				var operation = item_cls.addend('', 'op');

				item_cls.item().data(values);
				_create.op_bind(item_cls,operation,values);
			},
			op_bind: function(item_cls, operation, values) {
				operation.append(_create.add_link('上移', function() {
					_moveup(item_cls.item());
				}, 'moveup'));

				operation.append(_create.add_link('下移', function() {
					_movedown(item_cls.item());
				}, 'movedown'));

				operation.append(_create.add_link('编辑', function() {
					_edit(item_cls.item());
				}, 'edit'));

				operation.append(_create.add_link('删除', function() {
					if(item_cls.item().data('root')) {
						item_cls.item().parent().remove()
					} else {
						//var parent = _parent(item_cls.item());
						item_cls.remove();
						//_reset_index(parent);
					}
				}, 'delete'));

				if(values.type == 'root') {
					operation.append(_create.add_link('增加子菜单', function() {
						_add(item_cls.item());
					}, 'new'));
				}
			}
		}

		/*
		//索引INDEX重建
		var _reset_index = function(root) {
		}

		//某个菜单项的上级菜单项
		var _parent = function(item) {
		}
		*/

		//某个菜单项的下级菜单
		var _length = function(root_item) {
			if(!root_item) {
				return _container.find('div.weixin-menu-root').length;
			} else {
				return root_item.next().find('> .weixin-menu').length;
			}
		}

		var _moveup = function(item) {
			var box = item.data('root')?item.parent():item;
			var prevBox = box.prev();
			if(!prevBox[0]) return;
			box.slideUp('fast');
			prevBox.before(box);
			box.slideDown('fast');
		}

		var _movedown = function(item) {
			var box = item.data('root')?item.parent():item;
			var prevBox = box.next();
			if(!prevBox[0]) return;
			box.slideUp('fast');
			prevBox.after(box);
			box.slideDown('fast');
		}

		//编辑数据
		var _edit = function(item) {
			var values = item.data();
			var dialog = new menu_dialog();
			dialog.is_root = item.data('root');
			dialog.values = values;
			//添加表单确认时的回调
			dialog.fn_result = function(values) {
				_this.edit(values, item);
			}
			dialog.open();
		}

		//增加数据
		var _add = function(parent) {
			if(!create_check(false, parent)) return;
			var dialog = new menu_dialog();
			dialog.is_root = false;
			//添加表单确认时的回调
			dialog.fn_result = function(values) {
				_this.add(values, parent);
			}
			dialog.open();
		}

		//清空指定菜单旗下菜单
		var _clear = function(parent) {
			parent.next().remove();
		}

		var _export = function(root_item) {
			var data = new Array();
			if(!root_item) {
				_container.find('div.weixin-menu-root > .weixin-menu').each(function(index, el) {
					data[index] = $(this).data();
				});
			} else {
				root_item.next().find('> .weixin-menu').each(function(index, el) {
					data[index] = $(this).data();
				});
			}
			return data;
		}

		this.add = function(values, parent) {
			var item_cls = new _item();
			_create.add_item(item_cls, values);
			var item = item_cls.item().data('root', !parent);
			if(parent) {
				var parent_container = parent.next();
				if(!parent_container[0]) {
					parent_container = $('<div>').addClass('weixin-menu-parent');
					parent.after(parent_container);
				}
				parent_container.append(item);
			} else {
				_container.find('div.message').remove();
				$('<div>').addClass('weixin-menu-root').append(item).appendTo(_container);
			}
			return item;
		}

		this.edit = function(values, item) {
			var item_cls = item.data('this');
			item_cls.clear();
			_create.add_item(item_cls, values);
			//清空子菜单
			if(item.data('root') && values.type!='root') {
				_clear(item);
			}
		}

		//导出成post数据
		this.export = function() {
			var form_obj = new _form();
			_container.find('div.weixin-menu-root').each(function(index, el) {
				var data = $(this).find('> .weixin-menu').data();
				for(var k in data) {
					if(k=='this') continue;
					eval("var v=data."+k+";");
					form_obj.add_hidden('button['+index+']['+k+']', v);
				}
				if(data.type=='root') {
					$(this).find('>.weixin-menu-parent>.weixin-menu').each(function(index2, el2) {
						var data2 = $(this).data();
						for(var k2 in data2) {
							if(k2=='this') continue;
							eval("var v2=data2."+k2+";");
							form_obj.add_hidden('button['+index+'][sub_button]['+index2+']['+k2+']', v2);
						}
					});
				}
			});
			return form_obj.serialize();
		}

		//导入菜单数据
		this.import = function(buttons) {
			if(!buttons) return;
			$.each(buttons, function(index, val) {
				if(!val.type) val.type = 'root';
				var item = _this.add(val);
				if(val.type && val.sub_button.length > 0) {
					$.each(val.sub_button, function(index2, val2) {
						_this.add(val2, item);
					});
				}
			});
		}
	}

	//创建按钮验证
	function create_check(root, parent) {
		//菜单添加数量限制
		if(root && $('div.weixin-menu-root').length >= _menu_num_limit.root) {
			alert(_lang.form_root_menu_limit.replace('[num]', _menu_num_limit.root));
			return false;
		} else if(!root && parent.next().find('> .weixin-menu').length >= _menu_num_limit.sub) {
			alert(_lang.form_sub_menu_limit.replace('[num]', _menu_num_limit.sub));
			return false;
		}
		return true;
	}

	// 从微信服务器拉去自定义菜单信息
	function get_menus(url) {
		$('#menu_box').html('<div class="message">正在加载自定义菜单数据，请稍后...</div>');
		 $('#operation_button').hide();
		$.post(urls.get.url(), { in_ajax:1 }, function(data, textStatus, xhr) {
			if(data.is_message()) {
				myAlert(data);
			} else {
				if(!data.is_json()) {
					$('#menu_box').html('<div class="message">'+data+'</div>');
					return;
				}
				var json = parse_json(data);
				if(json.code > 200) {
					$('#menu_box').html(json.message);
					$('#operation_button').hide();
				} else {
					$('#operation_button').show();
					if(json.errcode||json.menu.button.length==0) {
						$('#menu_box').html('<div class="message">暂无数据</div>');
					} else {
						menus.import(json.menu.button);
					}
				}
			}
		});
	}
 
	//新增菜单项按钮事件绑定
	$('#add_root_menu_btn').click(function() {
		if(!create_check(true)) return;
		var dialog = new menu_dialog();
		dialog.is_root = true; //添加一级菜单
		//添加表单确认时的回调
		dialog.fn_result = function(values) {
			menus.add(values, 0);
		}
		dialog.open();
	});

   //提交菜单信息按钮事件绑定
	$('#submit_btn').click(function(event) {
		var params = menus.export();
		msgOpen('正在提交数据，请稍候...');
		$.post(urls.post.url(), params, function(data, textStatus, xhr) {
			msgClose();
			if(data.is_message()) {
				myAlert(data);
			} else {
				if(!data.is_json()) {
					alert('未知错误！');
					return;
				}
				var json = parse_json(data);
				if(json.code!=200) {
					alert(json.message);
					return;
				}
				alert('自定义菜单提交成功！');
			}
		});
	});

	var menus = new menu_list('#menu_box');
	//拉去菜单数据
	get_menus();

}