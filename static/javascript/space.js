//up_bg_img
website.module.space = {};
website.module.space.pageinit = {};

//加载js后执行
$(function() {
    if (website.module.uid > 0) {
        website.module.space.pageinit.common();      
    }
});

website.module.space.pageinit.common = function() {

	//上传背景
	function upload_background() {
		var url = Url('space/member/ac/config/op/upload_background');
		jslocation(url.url());
	}

	//上传背景按钮显示
	$('div.head-space a[data-name="up_bg_img"]').click(function(e) {
		e.preventDefault();
		upload_background();
	});

	//头像上传按钮显示
	$('div.head-user-face').mouseenter(function(event) {
		var face_con = $(this);
		face_con.find('.upload-link').show();
	}).mouseleave(function(event) {
		var face_con = $(this);
		(face_con).find('.upload-link').hide();
	});

}

