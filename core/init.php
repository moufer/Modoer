<?php
/**
* System init
* @author moufer<moufer@163.com>
* @copyright (C)2001-2012 Moufersoft
*/

//加载全局常量初始化
//启用本地配置和打开DEBUG功能，请进入core/defile.php文件设置
require __DIR__.'/define.php';

if(function_exists('set_magic_quotes_runtime')) {
	@set_magic_quotes_runtime(0);
}

//全局变量
$_G = $_C = $_CFG = $_HEAD = $_QUERY = $MOD = array();

//记录启动时内存情况
if(function_exists('memory_get_usage')) {
	$_G['memory_start'] = memory_get_usage();
}

//记录启动时间
$_G['mtime'] = explode(' ', microtime());
$_G['starttime'] = $_G['mtime'][1] + $_G['mtime'][0];

require MUDDER_DATA . 'config.php';
require MUDDER_CORE . 'version.php';

//本地开发加载本地配置文件
if(IN_LOCAL && is_file(MUDDER_DATA.'config_local.php')) {
	include MUDDER_DATA.'config_local.php';
}

//timezone
if(function_exists('date_default_timezone_set')) {
	if($_G['timezone'] == 8) $_G['timezone'] = 'Asia/Shanghai';
	@date_default_timezone_set($_G['timezone']);
}

define('TIMESTAMP', time());
$_G['timestamp'] = TIMESTAMP;

header('Content-type: text/html; charset=' . $_G['charset']);

//global function
require MUDDER_CORE . 'function.php'; 
require MUDDER_CORE . 'loader.php';

// web info
$_G['web'] 	= get_webinfo();
$_G['ip'] 	= get_ip();
$_G['random'] = random(5);

define('SELF', $_G['web']['self']);
define('URLROOT', get_urlroot());

if($_G['attackevasive'] && (!defined('IN_ADMIN') || SCRIPTNAV != 'seccode')) {
	include MUDDER_CORE . 'fense.php';
}

$_G['loader'] = new ms_loader();
$_G['loader']->helper('cache');

//database
$_G['db'] = new ms_activerecord($_G['dns']);
//dbcache
$_G['dbcache'] = $_G['loader']->model('dbcache');
$_G['dbcache']->findEx('comm_dbcache_expire,comm_task_nexttime');

//系统使用的Cookie 
$_G['cookie'] = $_G['loader']->cookie();
//【v3.5开始不建议直接使用 $_MODULES，建议用_cookie('参数')来获取cookies信息】
$_C =& $_G['cookie'];

//系统配置
$_G['cfg'] = $_G['loader']->variable('config');
//【v3.5开始不建议直接使用 $_CFG，建议用S('参数')来获取系统配置】
$_CFG =& $_G['cfg'];


//一启用模块列表
$_G['modules'] = $_G['loader']->variable('modules');
//【v3.5开始不建议直接使用 $_MODULES，建议用G('modules')来获取模块列表】
$_MODULES =& $_G['modules'];
/*
if(!$_CFG || !$_G['modules']) {
	include MUDDER_MODULE . 'modoer' . DS . 'inc' . DS . 'cache.php';
	$_G['modules'] = read_cache(MUDDER_CACHE . 'modoer_modules.php');
	foreach(array_keys($_G['modules']) as $flag) {
		$file = MUDDER_MODULE . $flag . DS . 'inc' . DS . 'cache.php';
		if(is_file($file)) include $file;
	}
	show_error('global_cache_succeed');
}
*/


$_CFG['siteurl'] = trim($_CFG['siteurl'], '/') . '/';

if($_CFG['siteclose'] && !defined('IN_ADMIN') && $_GET['act'] != 'seccode') {
	show_error($_CFG['closenote']);
}
if($_CFG['useripaccess'] && !check_ipaccess($_CFG['useripaccess'])) {
	show_error(lang('global_ip_without_list'));
}
if($_CFG['ban_ip'] && check_ipaccess($_CFG['ban_ip'])) {
	show_error(lang('global_ip_not_have_access'));
}

//session
//$_G['session'] = $_G['loader']->model('session');
$_G['session'] = call_user_func('ms_'.G('session_type').'_session::start');

//ob
if($_CFG['gzipcompress'] && function_exists('ob_gzhandler')) {
	@ob_start('ob_gzhandler');
} else {
	$_CFG['gzipcompress'] = 0;
	ob_start();
}

//hook
$_G['hook'] = $_G['loader']->lib('hook');

//url
$_G['url'] = $_G['loader']->lib('url');
if($_G['url']->is_404()) http_404();

//input strip
define('MAGIC_QUOTES_GPC', get_magic_quotes_gpc());
if(MAGIC_QUOTES_GPC) {
	$_POST = strip_slashes($_POST);
	$_GET = strip_slashes($_GET, TRUE);
	$_COOKIE = strip_slashes($_COOKIE, TRUE);
	$_REQUEST = strip_slashes($_REQUEST);
} else {
	$_GET = strip_ascii_low($_GET);
	$_COOKIE = strip_ascii_low($_COOKIE);
}

if(!defined('IN_ADMIN')) {
	$_POST = strip_sql($_POST);
	$_GET = strip_sql($_GET);
	$_COOKIE = strip_sql($_COOKIE);
	$_REQUEST = strip_sql($_REQUEST);
}

//ajax
if($_G['in_ajax'] = _input('in_ajax', 0, MF_TEXT)) {
	define('IN_AJAX', TRUE);
	$_G['output_charset'] = _input('output_charset', 0, MF_TEXT);
}
//json
if($_G['in_json'] = _input('in_json', 0, MF_TEXT)) {
	define('IN_JSON', TRUE);
}

//iframe
if($_G['in_iframe'] = _input('in_iframe', 0, MF_TEXT)) {
	define('IN_IFRAME', TRUE);
}

/*
if(!$in_ajax && function_exists('get_headers')) {
	if($headers = @get_headers($php_self, 1)) {
		$_G['in_ajax'] = $headers['X-Requested-With'] == 'XMLHttpRequest';
	}
}
*/

//系统初始化 HOOK
$_G['hook']->hook('init');

$_G['city'] = array();
$_CITY =& $_G['city'];

//尝试通过子域名，分站目录，cookie记录等信息创建分站类
if($_G['subsite'] = ms_subsite::create()) {
	$_G['subsite']->init(); //分站类初始化
}



//相关跳转
if(!$_G['in_ajax'] && !defined('IN_ADMIN'))  {
	//伪静态形式不是当前设置的形式时，通过301跳转到当前设置形式的URL形式
	if(S('rewrite_location') && $_G['url']->is_rewrite() && !$_G['url']->rewirte_mod_compare()) {
		location(U($_G['url']->get_url_exp(), true), true);
	}

	//当前URL使用了分站URL时，解析这个URL是否不需要分站URL（即是用全局URL）
	if(!$_G['url']->is_global_url() && $_G['url']->use_global_url($_GET['m'], $_GET['act'])) {
		//跳转到没有使用分站URL形式的URL
		location(U($_G['url']->get_url_exp(), true), true);
	}
	// //当前的URL没有使用分站URL时判断当前URL是否需要分站URL形式
	// if($_G['url']->is_global_url() && $_GET['m'] && !$_G['url']->use_global_url($_GET['m'], $_GET['act'])) {
	// 	//跳转到使用分站URL形式的URL
	// 	location(U($_G['url']->get_url_exp(), true), true);
	// }
}

//未知子域名 HOOK
if($_G['url']->is_unknow_subdomain()) {
	//模块没有使用未知子域名，则返回404
	if(!$_G['hook']->hook('init_sldomain', $_G['url']->get_unknow_subdomain(), true, MF_HOOK_RETURN_BREAK)) {
		http_404();
	}
}

if(!defined('IN_ADMIN')) {
	//前台会员登陆	
	$_G['user'] = $_G['loader']->model('member:user');
	$_G['user']->login->remember();
	//限制登陆
	if($_G['user']->get_access('member_forbidden') && $_GET['act'] != 'login') {
		show_error('member_access_forbidden');
	}
	//设为全局变量 【v3.5不建议直接使用$user，建议用G('user')来去的当前登陆会员变量】
	$user = $_G['user'];
}

//初始化完毕 Hook
$_G['hook']->hook('init_end');

//计划任务执行
if(!$_G['in_ajax']) {
	$cache_task_nexttime = $_G['dbcache']->fetch('comm_task_nexttime');
	if($cache_task_nexttime === false || $cache_task_nexttime < $_G['timestamp']) {
		$plan_task_obj = new msm_plan_task();
		$plan_task_obj->run();
		unset($plan_task_obj);
	}
	unset($cache_task_nexttime);
}

// datacall
$_G['datacall'] = $_G['loader']->model('datacall');
//$_G['datacall']->plan_delete();

// mutipage
$_GET['page'] = (int) _get('page');
$_GET['page'] = $_GET['page'] < 1 ? 1 : $_GET['page'];
$_GET['offset'] = (int) _get('offset');
$_GET['offset'] = $_GET['offset'] < 1 ? 20 : $_GET['offset'];

// header form
$_G['show_sitename'] = TRUE;
$_HEAD['title'] = $_CFG['sitename'] . $_CFG['titlesplit'] . $_CFG['subname'];
$_HEAD['keywords'] = $_CFG['meta_keywords'];
$_HEAD['description'] = $_CFG['meta_description'];
$_HEAD['css'] = '';
$_HEAD['js'] = '';

/* end */