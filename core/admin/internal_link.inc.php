<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
(!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied');

$ilink_mdl =& $_G['loader']->model('internal_link');
$op = _input('op', 'list');

switch($op) {
    case 'delete':
        if(empty($_POST['ids'])) redirect('global_op_unselect');
        $ilink_mdl->delete($_POST['ids']);
        redirect('global_op_succeed_delete',cpurl($module,$act,"list"));
        break;
    case 'update':
        if($_POST['links'] && !$ilink_mdl->update($_POST['links'])) {
            goto error;
        }
        if($_POST['new']['title']) {
            if(!$ilink_mdl->save($_POST['new'])) goto error;
        }
        redirect('global_op_succeed',cpurl($module,$act,"list"));

        error:
        redirect($ilink_mdl->error());
        break;
    default:
        $op = 'list';
        $offset = 20;
        $start = get_start($_GET['page'], $offset);
        $total = $ilink_mdl->count();
        if($total > 0) {
            $list = $ilink_mdl->find_all(null, null, null, $start, $offset);    
            if($total > $offset) {
                $multipage = multi($total, $offset, $_GET['page'], cpurl($module, $act, $op));
            }
        }

        $admin->tplname = cptpl('internal_link');
}

/** end */