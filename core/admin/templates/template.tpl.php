<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<div id="body">
<div class="sub-menu">
    <div class="sub-menu-heading"><?=$subtitle?></div>
    <a href="<?=cpurl($module,$act,$op)?>" class="sub-menu-item<?if(!$type)echo' selected';?>">已安装</a>
    <a href="<?=cpurl($module,$act,$op,array('type'=>'unerected'))?>" class="sub-menu-item<?if($type=='unerected')echo' selected';?>">未安装(<?=$unerected_total?>)</a>
</div>
<div class="space">
    <?if(!$type):?>
    <form method="post" action="<?=cpurl($module,$act,'',array('tpltype'=>$tpltype))?>">
    <table class="maintable" trmouse="Y">
        <tr class="altbg1">
            <td width="220">模板名称</td>
            <td width="215">所在目录</td>
            <?if($use_price):?>
            <td width="180">售价</td>
            <?endif;?>
            <td width="180">版权信息</td>
            <td width="*">操作</td>
        </tr>
        <? if($list):?>
        <? foreach($list as $val):?>
        <tr data-id="<?=$val['templateid']?>">
            <td>
                <?=form_input("templates[{$val['templateid']}][name]",$val['name'],'txtbox3')?>
            </td>
            <td>
                /templates/<?=$tpltype?>/<?=$val['directory']?>
                <input type="hidden" name="templates[<?=$val['templateid']?>][directory]" value="<?=$val['directory']?>" />
            </td>
            <?if($use_price):?>
            <td>
                <input type="text" name="templates[<?=$val['templateid']?>][price]" class="txtbox5" value="<?=$val['price']?>" />
                <?=display('member:point',"point/$selltype_pointtype")?>
            </td>
            <?endif;?>
            <td>
                <?=$val['copyright']?>
            </td>
            <td>
                [<a href="<?=cpurl($module,$act,'tpllist',array('type'=>$tpltype,'templateid'=>$val['templateid']))?>">模板文件管理</a>]&nbsp;
                <?if($tpltype=='main'):?>[<a href="<?=cpurl($module,$act,'tpllist',array('type'=>'datacall','templateid'=>$val['templateid']))?>">调用模板管理</a>]&nbsp;<?endif;?>
                <?if($val['directory']!='default'):?>
                [<a href="#" class="J_uninstall" data-templateid="<?=$val['templateid']?>">卸载</a>]
                <?endif;?>
            </td>
        </tr>
        <?endforeach;?>
        <?else:?>
        <tr><td colspan="5">暂无信息</td></tr>
        <?endif;?>
    </table>
    <? if($list):?>
    <center>
        <?=form_submit('dosubmit',lang('global_submit'),'Y','btn');?>
    </center>
    <? endif;?>
    </form>
    <?else:?>
    <table class="maintable" trmouse="Y">
        <tr class="altbg1">
            <td width="300">所在目录</td>
            <td width="*">操作</td>
        </tr>
        <?if($unerected_total > 0):?>
        <? foreach($dirs as $dir):?>
        <? if(in_array($dir, $installed)) continue;?>
        <tr>
            <td>/templates/<?=$tpltype?>/<?=$dir?></td>
            <td><a href="#" class="J_install" data-directory="<?=$dir?>">安装</a></td>
        </tr>
        <? endforeach;?>
        <?else:?>
        <tr><td colspan="2">暂无信息</td></tr>
        <?endif;?>
    </table>
    <?endif;?>
</div>
<form class="J_install_form none" method="post" action="<?=cpurl($module,$act,'install',array('tpltype'=>$tpltype))?>">
    <?=form_hidden('template[tpltype]', $tpltype)?>
    <table class="maintable">
        <tr>
            <td class="altbg1">模板名称：</td>
            <td><?=form_input('template[name]', '', 'txtbox3')?></td>
        </tr>
        <tr>
            <td class="altbg1">模板位置：</td>
            <td>
                /templates/<?=$tpltype?>/<span class="J_directory"></span>
                <?=form_hidden('template[directory]')?>
            </td>
        </tr>
        <tr>
            <td class="altbg1">版权信息：</td>
            <td>
                <?=form_input('template[copyright]', '', 'txtbox3')?>
            </td>
        </tr>
    </table>
    <center>
        <?=form_submit('dosubmit', '提交', 'Y', 'btn')?>
    </center>
</form>
<script type="text/javascript">
$(document).ready(function() {

    $('a.J_uninstall').each(function() {
        var a = $(this);
        a.click(function(event) {
            event.preventDefault();
            var data = a.data();
            uninstall(data, function() {
                a.parents('tr').remove();
            });
        });

    });

    $('a.J_install').each(function() {
        var a = $(this);
        a.click(function(event) {
            event.preventDefault();
            install_form(a.data());
        });
    });

    function uninstall(data, callback) {
        if(!confirm('您确定要删除当前模板吗？删除后会影响正在使用模板的对象？')) {
            return false;
        }
        var post_act = "<?=cpurl($module,$act,'uninstall')?>";
        data.in_ajax = 1;
        $.post(post_act.url(), {templateid:data.templateid}, function(result) {
            if(is_message(result)) {
                myAlert(result);
            } else if(result=='OK') {
                if(callback) {
                    callback();
                } else {
                    alert('删除完毕！');
                }
            } else {
                console.debug(result);
                alert('删除失败，可能网络忙碌，请稍后尝试。');
            }
        });
    }

    function install_form(data) {
        var form = $('form.J_install_form').clone();
        form.find('.J_directory').text(data.directory);
        form.find('[name="template[name]"]').val(data.directory);
        form.find('[name="template[directory]"]').val(data.directory);
        dlgOpen('新增模板', form.removeClass('none').show(), 500);
    }

});
</script>