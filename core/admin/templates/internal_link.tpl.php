<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<div id="body">
    <div class="sub-menu">
        <div class="sub-menu-heading">內链管理</div>
    </div>
    <div class="remind">
        <p>添加和删除链接后，并不会马上对相关使用內链功能的文本进行更新，只有用户在访问页面时进行更新。</p>
    </div>
    <form method="post" name="myform" action="<?=cpurl($module,$act)?>">
        <div class="space">
            <table class="maintable" border="0" cellspacing="0" cellpadding="0" trmouse="Y">
                <tr class="altbg1">
                    <td width="40">&nbsp;<a href="javascript:checkbox_checked('ids[]');">选</a></td>
                    <td width="250">链接标题</td>
                    <td width="400">链接地址</td>
                    <td width="300">链接提示</td>
                    <td width="80" title="点击链接时，浏览器将新窗口打开网址">新窗口[?]</td>
                    <td width="*">操作</td>
                </tr>
                <?php if($total > 0) {?>
                <?php while($val = $list->fetch_array()) {?>
                <tr>
                    <td><input type="checkbox" name="ids[]" value="<?=$val['id']?>" /></td>
                    <td><input type="text" name="links[<?=$val['id']?>][title]" class="txtbox3 width" value="<?=$val['title']?>" /></td>
                    <td><input type="text" name="links[<?=$val['id']?>][url]" class="txtbox2 width" value="<?=$val['url']?>" /></td>
                    <td><input type="text" name="links[<?=$val['id']?>][note]" class="txtbox3 width" value="<?=$val['note']?>" /></td>
                    <td><input type="checkbox" name="links[<?=$val['id']?>][target]" value="_blank"<?if($val['target'])echo'checked="checked"';?> /></td>
                    <td>-</td>
                </tr>
                <? } $list->free_result(); ?>
                <? } else {?>
                <tr>
                    <td colspan="4">暂无信息。</td>
                </tr>
                <?}?>
                <tr class="altbg1">
                    <td>增加:</td>
                    <td><input type="text" name="new[title]" class="txtbox3 width" /></td>
                    <td><input type="text" name="new[url]" class="txtbox3 width" /></td>
                    <td><input type="text" name="new[note]" class="txtbox3 width" /></td>
                    <td><input type="checkbox" name="new[target]" value="_blank" /></td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <?=$multipage?>
            <center>
                <input type="hidden" name="op" value="update" />
                <input type="hidden" name="dosubmit" value="yes" />
                <? if($total) : ?>
                <input type="button" name="dosubmit" value="删除所选" class="btn" onclick="easy_submit('myform', 'delete', 'ids[]');" />
                <? endif; ?>
                <input type="button" name="dosubmit" value="提交更新" class="btn" onclick="easy_submit('myform', 'update', null);" />
            </center>
        </div>
    </form>
</div>