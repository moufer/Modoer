<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<div id="body">
<div class="sub-menu">
    <div class="sub-menu-heading">搜索引擎优化</div>
</div>
<?=form_begin(cpurl($module,$act,$op))?>
    <div class="space">
        <table class="maintable">
            <tr>
                <td width="50%" class="altbg1"><strong>标题附加字:</strong>网页标题通常是搜索引擎关注的重点，本附加字设置将出现在标题中网站名称的后面，如果有多个关键字，建议用 "|"、","(不含引号) 等符号分隔</td>
                <td width="50%"><input type="text" name="setting[subname]" value="<?=$config['subname']?>" class="txtbox" />
                <br /><span class="font_2">网站标题不要太长，最好不要超过30个字符，搜索引擎对过长标题无爱。</span></td>
            </tr>
            <tr>
                <td class="altbg1"><strong>标题分隔符:</strong>标题和标题附加字之间的分隔符</td>
                <td><input type="text" name="setting[titlesplit]" value="<?=$config['titlesplit']?>" class="txtbox5" /></td>
            </tr>
            <tr>
                <td class="altbg1"><strong>使用SEO标签智能处理不存在标签内容:</strong>当用户自定义SEO标签时（标题，关键字，描述），如果遇到设置的标签不存在时，自动隐藏标签后面相关的文字，直到分隔符出现(,-|_)。</td>
                <td><?=form_bool('setting[auto_seo_tag]', (bool)$config['auto_seo_tag'])?></td>
            </tr>
            <tr>
                <td class="altbg1"><strong>Meta Keywords:</strong>Keywords 项出现在页面头部的 Meta 标签中，用于记录本页面的关键字，多个关键字间请用半角逗号 "," 隔开</td>
                <td><input type="text" name="setting[meta_keywords]" value="<?=$config['meta_keywords']?>" class="txtbox" /></td>
            </tr>
            <tr>
                <td class="altbg1"><strong>Meta Description:</strong>Description 出现在页面头部的 Meta 标签中，用于记录本页面的概要与描述</td>
                <td><input type="text" name="setting[meta_description]" value="<?=$config['meta_description']?>" class="txtbox" /></td>
            </tr>
            <tr>
                <td class="altbg1" valign="top"><strong>其它头部信息:</strong>如需在 &lt;head&gt;&lt;/head&gt; 中添加其它的 HTML 代码，可以使用本设置，否则请留空；请填写HTML代码，不要填写纯文字，否则会破环网页布局。</td>
                <td><textarea name="setting[headhtml]" rows="5" cols="40" class="txtarea"><?=$config['headhtml']?></textarea></td>
            </tr>
        </table>
    </div>
    <center><?=form_submit('dosubmit',lang('admincp_submit'),'yes','btn')?></center>
<?=form_end()?>
</div>