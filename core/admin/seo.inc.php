<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
(!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied');

$op = _input('op', null, MF_TEXT);
$seo_modules = get_seo_modules();

switch ($op) {
	case 'save':
		$module_flag = _post('module_flag', '', MF_TEXT);
		$seo_module = $seo_modules[$module_flag];
		if(!$seo_module) redirect('admincp_seo_not_found_func');
		$SEO = $_G['loader']->model($module_flag.':seosetting');
		$SEO->save_setting($_POST['modcfg']);
		redirect('global_op_succeed',cpurl($modules, $act, 'default', array('module_flag' => $module_flag)));
		break;
	
	default:
		$module_flag = _get('module_flag','item',MF_TEXT);
		if(isset($seo_modules[$module_flag])) {
			$seo_module = $seo_modules[$module_flag];
		} else {
			$module_flag = 'item';
			$seo_module = $seo_modules['item'];
		}
		if(!$seo_module) redirect('admincp_seo_not_found_func');
		$SEO = $_G['loader']->model($module_flag.':seosetting');
	    $admin->tplname = cptpl('seo_modules');
}

function get_seo_modules() {
	$modules = array();
	foreach (_G('modules') as $key => $value) {
		$filename = 'core/modules/'.$key.'/model/seosetting_class.php';
		if(is_file(MUDDER_ROOT . $filename)) {
			$modules[$key] = $value;
		}
	}
	return $modules;
}
/** end **/
?>