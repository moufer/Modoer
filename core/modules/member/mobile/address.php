<?php
!defined('IN_MUDDER') && exit('Access Denied');

$op=_input('op');
switch ($op) {

	case 'add':
	case 'edit':
		$service_obj = new ms_service();
		$address_obj = new mc_member_address();
		if(!$address_obj->set_uid(_G('user')->uid)) {
			$service_obj->add_error($address_obj);
		} else {
			if($op=='add') {
				$address_id = $address_obj->add();
			} else {
				$address_id = $address_obj->edit();
			}
			if(!$address_id) {
				$service_obj->add_error($address_obj);
			} else {
				$service_obj->data = $address_obj->get_one($address_id);
			}
		}
		echo $service_obj->fetch_all_attr('json');
		output();
		break;

	case 'set_default':
		$id = _post('id', null, MF_INT_KEY);
		$service_obj = new ms_service();
		$address_obj = new mc_member_address();
		if(!$address_obj->set_uid(_G('user')->uid)) {
			$service_obj->add_error($address_obj);
		} else {
			if(!$address_obj->set_default($id)) {
				$service_obj->add_error($address_obj);
			}
		}
		echo $service_obj->fetch_all_attr('json');
		output();
		break;

	case 'delete':
		$service_obj = new ms_service();
		$id = _post('id', null, MF_INT_KEY);
		$address_mdl = G('loader')->model('member_address');
		if(!$address_mdl->delete($id)) {
		    $service_obj->add_error($address_mdl);
		}
		echo $service_obj->fetch_all_attr('json');
		output();
		break;
	
	default:
		$header_title = '收货地址';
		$address_list = array();
		$address_mdl = G('loader')->model('member_address');
		$r = $address_mdl->get_list(G('user')->uid);
		if($r) while ($v = $r->fetch()) {
			$address_list[] = $v;
		}
		include mobile_template('member_address');
		break;
}
/* end */