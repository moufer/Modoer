<?php
!defined('IN_MUDDER') && exit('Access Denied');

if(check_submit('dosubmit')) {
	$upload = new ms_upload_file('face', 'jpg');
	if(!$upload->upload('temp')) {
		redirect('图片上传失败！');
	}
	$source_file = $upload->path.DS.$upload->filename;

	$face = new mc_member_face();
	$face->upload($user->uid, $source_file);

	location(url('member/mobile/do/face/r/'.random(3)));
}

//手机助手菜单获取
$header_title = '我的头像';
include mobile_template('member_face');

/* end */