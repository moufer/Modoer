<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

return array (
    'reg' => lang('membercp_point_reg'),
    'login' => lang('membercp_point_login'),
    'inviter' => lang('membercp_point_inviter'),
    'invitee' => lang('membercp_point_invitee'),
);
?>