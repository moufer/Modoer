<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');
define('SCRIPTNAV', 'city');

$city_id    = _input('city_id', null, MF_INT_KEY);
$name       = _input('name',    null, MF_TEXT);
$forward    = _get('forward',   null, MF_TEXT);

if($city_id || $name) {
    if($city_id) {
        $city = ms_city::form_id($city_id);
    }
    if(!$city && $name) {
        $city = ms_city::from_domain($name);
    }
    if(!$city) {
        redirect('global_area_city_id_invalid', U('city:0/index/city'));
    }
    G('subsite', new ms_subsite($city));
    G('subsite')->init();

    //页面跳转
    if($forward) $forward = rawurldecode($forward);
    $forward = str_replace('!', '/', $forward);
    $url = $forward ? U($forward) : G('subsite')->url();

    location($url);

} else {

    $sitename = S('sitename');
    $d_city = ms_city::form_default();
    if(!G('subsite')) {
        if($d_city) {
            G('subsite', new ms_subsite($d_city));
            G('subsite')->init();
        } else {
            show_error('global_area_city_empty');
        }
    }

    $A = $_G['loader']->model('area');
    //获取全部的城市列表（已启用的）
    $list = $A->get_list(0, TRUE);

    $total = 0;
    if($list) foreach($list as $k=>$v) {
        foreach($v as $_k => $_v) {
            if(!$_v['enabled']) {
                unset($list[$k][$_k]);
            } else {
                $total++;
            }
        }
        if(empty($list[$k])) unset($list[$k]);
    }
    ksort($list); //按字母排序

    if(!$forward) $forward = null;

    include template('modoer_city');
}

/** end */