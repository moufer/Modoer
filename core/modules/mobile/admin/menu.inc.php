<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
(!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied');

$menu_obj = new mc_mobile_menu();
$op = _input('op', null, MF_TEXT);

switch($op) {

	case 'reset':
		$menu_obj->reset();
		if($menu_obj->has_error()) redirect($menu_obj->error());
		redirect('global_op_succeed', cpurl($module, $act));
		break;
	
	case 'save':
		$menu_obj->save($_POST['menus']);
		if($menu_obj->has_error()) redirect($menu_obj->error());
		redirect('global_op_succeed', cpurl($module, $act));
		break;

	default:
		//标题
		$menus = $menu_obj->load();
		$links = $_G['hook']->hook('mobile_index_link',null,TRUE);
		//模板
		$admin->tplname = cptpl('menu_list', MOD_FLAG);
}

/** end */