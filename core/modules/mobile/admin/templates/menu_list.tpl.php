<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<div id="body">
	<div class="sub-menu">
		<div class="sub-menu-heading">菜单管理</div>
		<a href="<?=cpurl($module,$act,'list')?>" class="sub-menu-item selected">菜单列表</a>
	</div>
	<div class="remind">
		<p>当前设置的自定义指令为官方网站的微信号使用，商户的自定义指令请在前台商户管理处设置。</p>
	</div>
	<div class="space">
		<form class="J_form_data_save" method="post" action="<?=cpurl($module, $act, 'save')?>">
		<table class="maintable J_data_table" trmouse="Y">
			<tr class="altbg1">
				<td width="80">图标</td>
				<td width="200">标题</td>
				<td width="150">页面标识</td>
				<td width="*">链接</td>
				<td width="120">操作</td>
			</tr>
			<?if($list)foreach($list as $val):?>
			<tr>
				<td><?=$val['icon']?></td>
				<td><?=$val['title']?></td>
				<td><?=$val['url']?></td>
				<td><?=$val['flag']?></td>
				<td>
					<a href="#" data-name="edit">编辑</a>
					<a href="#" data-name="delete">删除</a>
				</td>
			</tr>
			<?endforeach;?>
			<?if(empty($list)):?><!--<tr class="J_data_empty"><td colspan="5">暂无信息</td></tr>--><?endif;?>
		</table>
		<center style="margin-top:10px;">
			<button type="submit" class="btn" name="dosubmit">提交编辑</button>
			<button type="button" class="btn secondary" data-name="add">添加菜单</button>
			<button type="button" class="btn secondary" data-name="reset">恢复默认菜单</button>
		</center>
		</form>
	</div>
	<form class="J_form_add none" method="post" enctype="multipart/form-data" action="<?=U("modoer/upload/in_ajax/1")?>">
		<table class="maintable">
			<tr>
				<td class="altbg1">内置链接：</td>
				<td>
					<select data-name="built_links">
						<option value="">==选择==</option>
						<?foreach ($links as $key => $link):?>
						<option value='{"title":"<?=$link['title']?>","url":"<?=$link['url']?>","flag":"<?=$link['flag']?>","iconurl":"templates/mobile/default/images/menu/<?=$link['icon']?>.png"}'>
							<?=$link['title']?>
						</option>
						<?endforeach;?>
					</select>
				</td>
			</tr>
			<tr>
				<td class="altbg1">菜单标题<span class="font_1">*</span>：</td>
				<td>
					<?=form_input('title', '', 'txtbox2')?>
				</td>
			</tr>
			<tr>
				<td class="altbg1">链接地址<span class="font_1">*</span>：</td>
				<td>
					<?=form_input('url', 'http://', 'txtbox2')?>
				</td>
			</tr>
			<tr>
				<td class="altbg1">页面标识：</td>
				<td><?=form_input('flag', '', 'txtbox3')?></td>
			</tr>
			<tr>
				<td class="altbg1">菜单图标：</td>
				<td>
					<?=form_file('picture', array('accept'=>'image/*'))?>
					<input type="hidden" name="set_domain" value="N" />
				</td>
			</tr>
		</table>
		<center style="margin:10px 0;">
			<button type="submit" class="btn" name="dosubmit" value="yes">提交</button>&nbsp;&nbsp;
			<button type="button" class="btn unimportant" data-type="close">关闭</button>
		</center>
	</form>
</div>
<script type="text/javascript">
$(document).ready(function() {

	//提交表单事件绑定
	$('form.J_form_data_save').submit(function(e) {
		var form = $(this);
		form.find('tr.J_data_item').each(function(index, el) {
			$.each($(this).data(), function(key, val) {
				form.append('<input type="hidden" name="menus['+index+']['+key+']" value="'+val+'">');
			});
		});
	});

	var item_list = new Array();
	<?php
	if($menus):
	foreach($menus as $key => $params):
	?>
	item_list.push({
		title:'<?=$params['title']?>',
		url:'<?=$params['url']?>',
		flag:'<?=$params['flag']?>',
		iconurl:'<?=$params['iconurl']?>'
	});
	<?php
	endforeach;
	endif;
	?>

	var menus = new mobile_menus();
	$.each(item_list, function(index, val) {
		menus.add_item(val);
	});

});

var mobile_menus = function() {
	//添加菜单表单提交
	function submit_check(form) {
		var params = {};
		$.each(form.serializeArray(), function(index, val) {
			var code = "params."+val.name+"="+(val.value?("'"+val.value.trim()+"'"):"''")+";";
			eval(code);
		});
		if(params.set_domain) delete(params.set_domain);
		//表单元素检查
		if(!params.title||params.title=='') {
			alert('未填写菜单标题。');
			return false;
		} else if(!params.url||params.url==''||params.url=='http://') {
			alert('未填写菜单链接。');
			return false;
		}
		return params;
	}

	//添加菜单项到表格
	function item(data) {
		var tr = $('<tr></tr>').addClass('J_data_item');
		var img = $('<td></td>').append('<img src="'+urlroot+'/'+data.iconurl+'" width="50" width="50" />');
		var title = $('<td></td>').append(data.title);
		var link = $('<td></td>').append(data.url);
		var flag = $('<td></td>').append(data.flag);
		var op = $('<td></td>');
		op.append($('<a href="#">上移</a>').click(function(event) {
			event.preventDefault();
			listorder(tr,'up');
		}));
		op.append('&nbsp;&nbsp;');
		op.append($('<a href="#">下移</a>').click(function(event) {
			event.preventDefault();
			listorder(tr,'down');
		}));
		op.append('<br />');
		op.append($('<a href="#">编辑</a>').click(function(event) {
			event.preventDefault();
			edit();
		}));
		op.append('&nbsp;&nbsp;');
		op.append($('<a href="#">删除</a>').click(function(event) {
			event.preventDefault();
			del();
		}));
		tr.append(img).append(title).append(flag).append(link).append(op).data(data);

		return tr;

		//移动菜单排列顺序
		function listorder(item, movetype) {
			if(movetype=='up') {
				var previtem = item.prev();
				if(previtem[0] && previtem.attr('class')=='J_data_item') previtem.before(item);
			} else {
				var nextitem = item.next();
				if(nextitem[0]) nextitem.after(item);
			}
		}

		//编辑一个导航
		function edit() {
			var options = {
				values:data,
				onSubmit:function(dialog, form) {
					dialog_form_submit(dialog, form, tr);
					return false; //不提交表单
				},
				onSetValue:function(dialog, form, values) {
					dialog_form_set_values(form, values);
				}
			};
			src_form.clone(true).form_dialog(options).open();
		}

		//删除一个导航
		function del() {
			tr.remove();
		}
	}

	function dialog_form_set_values(form, values) {
		form.find('[name="title"]').val(values.title);
		form.find('[name="url"]').val(values.url);
		form.find('[name="flag"]').val(values.flag);
		var pic_tr = form.find('[name="picture"]').parents('tr');
		if(values.iconurl) {
			var img = $('<img width="50" height="50" />').attr('src',getImageUrl(values.iconurl));
			pic_tr.find('img').remove();
			pic_tr.find('[name="iconurl"]').remove();
			pic_tr.find('[name="picture"]').hide();
			pic_tr.find('td:last')
				.append($('<input type="hidden" name="iconurl" value="'+values.iconurl+'" />'))
				.append(img);
		} else {
			form.find('[name="picture"]').show();
		}
	}

	function dialog_form_submit(dialog, form, item_obj) {
		var params = submit_check(form);
		console.debug(params);
		if(!params) return false;
		if(params.iconurl) {
			console.debug(item_obj);
			if(item_obj) {
				var newitem = item(params);
				newitem.replaceAll(item_obj);
			} else {
				table.append(item(params));
			}
			dialog.close();
		} else {
			//无刷新上传图片，并获得图片的URL
			var iframe = new $.mframe_form(
				form, {}, {
					onSucceed:function(data) {
						params.iconurl = data.trim(); //返回的图片地址
						if(item_obj) {
							var newitem = item(params);
							newitem.replaceAll(item_obj);
						} else {
							table.append(item(params));
						}
						dialog.close();
					}
				}
			);
			iframe.submit();			
		}
	}

	function dialog_form_set_links(dialog, form, value) {
		if(!value) return;
		json = parse_json(value);
		dialog.set_value(json);
	}

	var table = $('table.J_data_table');
	var src_form = $('form.J_form_add');

	//添加新的
	$('button[data-name="add"]').click(function() {
		var form = src_form.clone(true);
		var dialog = form.form_dialog({
			dialog:{
				width:550,
				title:'添加'
			},
			onSubmit:function(dialog, form) {
				dialog_form_submit(dialog, form);
				return false; //不提交表单
			},
			onSetValue:function(dialog, form, values) {
				dialog_form_set_values(form, values);
			}
		});
		form.find('[data-name="built_links"]').change(function(e) {
			dialog_form_set_links(dialog, form, $(this).val());
		});
		dialog.open();
	});

	//恢复默认
	$('button[data-name="reset"]').click(function() {
		if(confirm('是否恢复默认菜单，这将覆盖当前的全部菜单数据。')) {
			var url = "<?=cpurl($module,$act,'reset')?>";
			jslocation(url);
		}
	});

	//新增一条导航数据
	this.add_item = function(data) {
		table.append(item(data));
	}

}
</script>