<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

function mobile_template($filename, $templateid = '', $update = false)
{
    if(!$templateid) $templateid = (int)S('mobile:templateid');
    return template($filename, 'mobile', $templateid, '', $update);
}

function mobile_page($count, $offset, $page, $url)
{
    static $page_obj = null;
    if(!$page_obj) {
        $page_obj = new mc_mobile_page;
    }
    $page_obj->count    = $count;
    $page_obj->offset   = $offset;
    $page_obj->page     = $page;
    $page_obj->url      = $url;

    return $page_obj->create();
}

function mobile_location($url)
{
    if(!$url) return;
    $url = str_replace(array('&amp;','&#38;'), '&', $url);
    include mobile_template('jslocation');
    exit;
}
?>