<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

class hook_article extends ms_base {

    function __construct(&$hook) {
        parent::__construct();
        $hook->register(
        	array('admincp_subject_edit_link','subject_detail_link','mobile_subject_link',
        		'mobile_index_link', 'mobile_member_link', 
        		'space_nav_link'), 
        	$this
        );
    }

    //后台主题编辑页导航
	function admincp_subject_edit_link($sid) {
		$url = cpurl('article','article','list',array('sid'=>$sid));
		return array(
			'flag' => 'article:list',
			'url' => $url,
			'title'=> '资讯管理',
		);
	}

	//主题页导航
	function subject_detail_link(&$params) {
		extract($params);
		$title = '资讯';
		return array (
			'flag' => 'article',
			'url' => url('article/item/sid/'.$sid),
			'title'=> $title,
		);
	}

	//手机主题页导航
	function mobile_subject_link($params) {
		$title = '资讯';
		return array (
			'flag' => 'article',
			'url' => url('article/mobile/do/item/sid/'.$params['sid']),
			'title'=> $title,
		);
	}

	//手机web首页导航
	function mobile_index_link() {
		$result[] = array (
			'flag' => 'article/list',
			'url' => url('article/mobile/do/list'),
			'title'=> '新闻资讯',
			'icon' => 'article',
		);
		return $result;
	}

	//手机助手导航
	function mobile_member_link() {
		$result[] = array (
			'flag' => 'article/member/my',
			'url' => url('article/mobile/do/my'),
			'title'=> '我的文章',
		);
		return $result;
	}

	//个人空间导航
	function space_nav_link($space) {
		$result = array();
		$result[] = array (
			'flag' => 'article',
			'url' => url("space/{$space->uid}/pr/article"),
			'title'=> "文章",
		);
		return $result;
	}

}
/** end */