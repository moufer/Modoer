<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
class display_article {

    //取得分类的名称或其它
    //参数 catid,keyname
    function category($params) {
        extract($params);
        if(!$keyname) $keyname = 'name';
        if(!$catid) return '';
        $loader =& _G('loader');
        $category = $loader->variable('category','article');
        if(!isset($category[$catid][$keyname])) return '';
        return $category[$catid][$keyname];
    }

    //下一篇
    function next($params) {
        extract($params);
        $result = '<a href="'.U('article/index',true).'">没有了，返回栏目</a>';
        if(!$articleid) return $result;
        $article_mdl = G('loader')->model(':article');
        $where = array();
        $where['status']=1;
        if($city_id) $where['city_id'] = $city_id;
        $where['articleid'] = array('where_more', array($articleid, 0));
        $detail = $article_mdl->db->from($article_mdl->table)
            ->where($where)
            ->order_by('articleid','ASC')
            ->get_one();
        if(!$detail) return $result;
        return '<a href="'.U('article/detail/id/'.$detail['articleid'],true).'">'.$detail['subject'].'</a>';
    }

    //上一篇
    function forward($params) {
        extract($params);
        $result = '<a href="'.U('article/index',true).'">没有了，返回栏目</a>';
        if(!$articleid) return $result;
        $article_mdl = G('loader')->model(':article');
        $where = array();
        $where['status']=1;
        if($city_id) $where['city_id'] = $city_id;
        $where['articleid'] = array('where_less', array($articleid, 0));
        $detail = $article_mdl->db->from($article_mdl->table)
            ->where($where)
            ->order_by('articleid','DESC')
            ->get_one();
        if(!$detail) return $result;
        return '<a href="'.U('article/detail/id/'.$detail['articleid'],true).'">'.$detail['subject'].'</a>';
    }

}
?>