<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

class msm_article_data_cache extends ms_model {

    var $table = 'dbpre_article_data_cache';
    var $key = 'articleid';
    var $model_flag = 'article';

    function __construct() {
        parent::__construct();
        $this->model_flag = 'article';
        $this->modcfg = $this->variable('config');
    }

    function get_cache($article_data) {
        //是否启用了內链功能
        if(!S('article:use_internal_link')) {
            return false;
        }
        $ilink_mdl = _G('loader')->model('internal_link');
        if(!$links = $ilink_mdl->fetch_all()) {
            return false;
        }

        $articleid = $article_data['articleid'];
        //获取内容缓存
        $cache_data = $this->read($articleid); 
        if(!$cache_data) goto update_cache;

        //內链数据最后更新时间
        $last_update_time = $ilink_mdl->last_update_time();

        //如果内容缓存为空，或者缓存时间比內链数据最后更新时间早，就重新生成内容缓存
        if($cache_data['cache_time'] < $last_update_time) goto update_cache;

        return array_merge($article_data, $cache_data);

        //生成新的内容缓存
        update_cache:
            $ilink_obj = new ms_internal_link($article_data['content'], $links);
            $article_data['content'] = $ilink_obj->process();

            $post = array(
                'content' => $article_data['content'],
            );
            $this->save($post, $articleid);
            return $article_data;
    }

    function save($post, $articleid)
    {
        $this->db->from($this->table)
            ->set('articleid', $articleid)
            ->set('cache_time', _G('timestamp'))
            ->set($post)
            ->replace();
    }
    
}

/** end */