<?php
//发表的点评
$article_mdl = $_G['loader']->model(':article');

$where = array();
$where['uid'] = $space->uid;
$where['status'] = 1;

$offset = 10;
$start = get_start($_GET['page'], $offset);

$select = '*';

list($total, $articles) = $article_mdl->find($select, $where, array('dateline' => 'DESC'), $start, $offset, TRUE);

//分页
if($total) {
    $multipage = multi($total, $offset, $_GET['page'], url("space/$space->uid/pr/article/page/_PAGE_"));
}

$flag = 'article';

//页面SEO
$_HEAD['title']	= $space->username.'发布的文章' . $_CFG['titlesplit'] . $space->spacename;

//设置模板ID
$templateid = $space->space_styleid;

//载入模型的内容页模板
include space_template('article_list', (int)$templateid);

/** end */