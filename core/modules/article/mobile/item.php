<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');
define('SCRIPTNAV', 'article');

$sid = _get('sid', null, MF_INT); //主题ID

$S = $_G['loader']->model('item:subject');
$subject = $S->read($sid);
if(empty($subject)) redirect('item_empty');
$fullname = $subject['name'] . ($subject['subname']?"($subject[subname])":'');
$subject_field_table_tr = $S->display_sidefield($subject);

//分类
$A =& $_G['loader']->model(':article');

//筛选
$select = 'articleid,subject,a.dateline,pageview,comments,digg,uid,author,copyfrom,thumb,picture,introduce';
$orderby = array('dateline'=>'DESC');
$offset = $MOD['list_num'] > 0 ? $MOD['list_num'] : 10;
$start = get_start($_GET['page'],$offset);
$_GET['status'] = 1;
list($total, $list) = $A->search($select, $orderby, $start, $offset);
if($total) {
	$multipage = mobile_page($total, $offset, $_GET['page'], 
    	url("article/mobile/do/item/sid/$sid/page/_PAGE_"));
}

if($_G['in_ajax']) {
	include mobile_template('article_list_li');
	output();
}

if (strposex($_SERVER['HTTP_REFERER'],'filter') || strposex($_SERVER['HTTP_REFERER'],'detail')){
    $header_forward = url("article/mobile/do/list");
} elseif(strposex($_SERVER['HTTP_REFERER'],'list')){
    $header_forward = url("mobile/index");
} elseif($_SERVER['HTTP_REFERER']) {
    //$header_forward = $_SERVER['HTTP_REFERER'];
}

include mobile_template('article_item');