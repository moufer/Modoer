<?php
!defined('IN_MUDDER') && exit('Access Denied');
define('SCRIPTNAV', 'article');

$A = G('loader')->model(':article');

$op = _input('op',MF_TEXT);
switch ($op) {
	case 'add':
		$role = 'member';
		$sid = _input('sid', MF_INT_KEY, 0);
		if($sid > 0) {
			//判断是否主题管理员
			$subjectModel = G('loader')->model('item:subject');
			if(!G('loader')->model('item:subject')->is_mysubject($sid, G('user')->uid))
				redirect('对不起，您不是本主题的管理员。');
			if(!$subject  = $subjectModel->read($sid))
				redirect('item_empty');
			$role = 'owner';
		}

		if(check_submit('dosubmit')) {
		    $post = $A->get_post($_POST);
		    $articleid = $A->save($post, null, $role);
		    redirect('global_op_succeed', U('article/mobile/do/my'));
		}

		$editor = new ms_editor('content');
		$editor->item = 'mobile';
		$editor->pagebreak = true;
		$edit_html = $editor->create_html();

		include mobile_template('article_post');
		break;
	case 'edit':
		$articleid = _input('articleid',MF_INT_KEY,0);
		if(!$articleid) redirect('未指定文章ID。');

		$detail = $A->read($articleid);
		if(!$detail) redirect('article_empty');

		//判断是否主题管理员
		$sids = G('loader')->model('item:subject')->mysubject(G('user')->uid);
		$role = $sids?'owner':'member';
		//判断用户是否有编辑文章的权限
		$access = G('user')->check_access('article_post', $A, false);
		if(!$sids && !$access) {
			redirect('global_op_access');
		}

		if(check_submit('dosubmit')) {
			$post = $A->get_post($_POST);
		    $articleid = $A->save($post, $articleid, $role);
		    redirect('global_op_succeed', U('article/mobile/do/my'));
		}

		$editor = new ms_editor('content');
		$editor->item = 'mobile';
		$editor->pagebreak = true;
		$editor->content = $detail['content'];
		$edit_html = $editor->create_html();

		include mobile_template('article_post');
		break;
	default:

		redirect('global_op_unknow');
		break;
}


/* end */