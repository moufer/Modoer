<?php
!defined('IN_MUDDER') && exit('Access Denied');
define('SCRIPTNAV', 'article');


$select = 'articleid,subject,dateline,pageview,comments,digg,uid,author,copyfrom,thumb,picture,introduce';

$where = array();
$where['uid'] = G('user')->uid;
$where['status'] = 1;

$orderby = array('dateline'=>'DESC');

$offset =  $MOD['list_num'] > 0 ? $MOD['list_num'] : 10;
$start = get_start($_GET['page'],$offset);

$A = G('loader')->model(':article');
list($total, $list) = $A->find($select, $where, $orderby, $start, $offset, true);
if($total) {
	$multipage = mobile_page($total, $offset, $_GET['page'], 
    	url("article/mobile/do/my/page/_PAGE_"));
}

if($_G['in_ajax']) {
	include mobile_template('article_list_li');
	output();
}

include mobile_template('article_my');

/* end */