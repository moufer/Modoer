<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
(!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied');

$op = _input('op');

switch ($op) {
    case 'enable':
        $payment = _input('payment', '', MF_TEXT);
        if(!mc_pay::enable($payment)) {
            $err = mc_pay::get_last_error();
            redirect($err['msg']);
        }
        redirect('global_op_succeed', cpurl($module,$act));
        break;

    case 'disable':
        $payment = _input('payment','',MF_TEXT);
        mc_pay::disable($payment);
        redirect('global_op_succeed', cpurl($module,$act));
        break;

    case 'setting':
        $payment = _input('payment', '', MF_TEXT);
        if(!mc_pay::exists($payment)) {
            redirect('支付接口未指定或不存在。');
        }
        if(check_submit('dosubmit')) {
            if(mc_pay::save_setting($payment)) redirect('global_op_succeed', cpurl($module,$act));
            $err = mc_pay::get_last_error();
            redirect($err['msg']);
        } else {
            $payment_cls = mc_pay::factory($payment);
            if(!$payment_cls) {
                $err = mc_pay::get_last_error();
                redirect($err['msg']);
            }
            $admin->tplname = cptpl('payment_setting', MOD_FLAG);
        }
        break;

    default:
        $payments = mc_pay::get_payments();
        $admin->tplname = cptpl('payment_list', MOD_FLAG);
}

/* end */