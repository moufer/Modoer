<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
(!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied');

$WD =& $_G['loader']->model('pay:withdraw');
$op = _input('op', 'list');

switch($op) {
    case 'update':
        $id = _post('id', 0, MF_INT);
        $status = _post('status', '', MF_TEXT);
        $status_des = _post('status_des', '', MF_TEXT);
        $WD->update_status($id, $status, $status_des);
        echo 'succeed';
        output();
        break;
    default:
        $status = _get('status', 'wait', MF_TEXT);
        $where = array();
        $where['status'] = $status=='cancel'?array('cancel','fail'):$status;
        $offset = 20;
        $start = get_start($_GET['page'], $offset);
        list($total, $list) = $WD->find('*',$where,array('applytime'=>'DESC'), $start, $offset, true);
        if($total) {
            $multipage = multi($total, $offset, $_GET['page'], cpurl($module, $act, 'list', array('status'=>$status)));
        }
        $admin->tplname = cptpl('withdraw_list', MOD_FLAG);
}
?>