<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<div id="body">
    <form method="post" action="<?=cpurl($module,$act,'setting')?>" name="myform">
    <div class="space">
        <div class="subtitle">支付接口管理</div>
        <table class="maintable" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td class="altbg1" width="45%">
                    <strong>支付接口:</strong>
                    <span class="font_1">请确保您的服务器PHP环境已经加载curl和SSL模块，否则将无法正常使用支付接口。</span>
                </td>
                <td width="*"><?=$payment::$name?></td>
            </tr>
            <?php $elements = $payment_cls::create_setting_from();?>
            <?php if($elements) foreach($elements as $item):?>
            <tr>
                <td class="altbg1"><strong><?=$item['title']?>:</strong><?=$item['des']?></td>
                <td><?=$item['content']?></td>
            </tr>
            <?php endforeach;?>
        </table>
    </div>
    <center>
        <input type="hidden" name="payment" value="<?=$payment?>">
        <?=form_submit('dosubmit',lang('admincp_submit'),'yes','btn')?>
        <button type="button" class="btn" onclick="history.go(-1);">返回</button>
    </center>
</form>
</div>