<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<div id="body">
    <div class="space">
        <div class="subtitle">支付接口管理</div>
        <table class="maintable" border="0" cellspacing="0" cellpadding="0" trmouse="Y">
            <tr class="altbg1">
                <td width="40"><center>使用</center></td>
                <td width="210">名称</td>
                <td width="80">使用平台</td>
                <td width="*">介绍</td>
                <td width="120">操作</td>
            </tr>
            <?foreach($payments as $payment) {?>
            <tr>
                <?php $enable = S('pay:'.substr($payment, 8))?>
                <td><center><?=($enable?'√':'×')?></center></td>
                <td><?=$payment::$name?></td>
                <td><?=implode('，', $payment::$platform)?></td>
                <td><?=$payment::$intro?></td>
                <td>
                    <a href="<?=cpurl($module, $act, 'setting', array('payment'=>$payment))?>">配置</a>&nbsp;
                    <a href="<?=cpurl($module, $act, ($enable?'disable':'enable'), array('payment'=>$payment))?>"><?=$enable?'<span class="font_2">停用</span>':'启用'?></a>
                </td>
            </tr>
            <?}?>
        </table>
    </div>
</div>