<?php
!defined('IN_MUDDER') && exit('Access Denied');

$cz_type = S('pay:cz_type') ? unserialize(S('pay:cz_type')) : false;
if(!in_array('rmb', $cz_type)) {
	redirect('对不起，网站没有打开现金充值功能。');
}

if(!mc_pay::get_payments(true,'mobile')) {
	redirect('对不起，网站没有打开支付接口。');
}

$log_obj = $_G['loader']->model('pay:log');

if(check_submit('pay')) {
	
	if($log_obj->create(_post('pay'), null, true)) {
		redirect('pay_succeed',url('member/mobile/do/point'));
	}

} elseif($op=='return') {

    $orderid = _get('orderid', 0, 'intval');
    $order = $log_obj->read($orderid);
    if(empty($order)) redirect('订单不存在！');

    $flash_message = '充值完毕！';
    $header_title = '我的积分';
    include mobile_template('member_point');

} else {

	$header_title = '在线充值';
	G('loader')->helper('form','pay');
	include mobile_template('pay_recharge');

}