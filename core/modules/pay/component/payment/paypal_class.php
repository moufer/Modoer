<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

class payment_paypal extends mc_pay_payment {

    public static $name = 'PayPal';
    public static $intro = 'PayPal是倍受全球亿万用户追捧的国际贸易支付工具，即时支付，即时到账，全中文操作界面，能通过中国的本地银行轻松提现，为您解决外贸收款难题，助您成功开展海外业务，决胜全球。网址：www.paypal.com';
    public static $platform = array('pc','mobile');
    public static $setting_keys = array('paypal_email', 'paypal_currency');

    var $gateway = "https://www.paypal.com/row/cgi-bin/webscr";
    var $mysgin = '';

    var $notify_return = false;
    var $test = false; //sandbox

    public function __construct() {
        parent::__construct();
        $this->notify_url = S('siteurl').'pay.php?act=notify&api=paypal';//?act=notify&api=paypal
        $this->return_url = S('siteurl').'pay.php?act=return&api=paypal';//
        if($this->test = !empty($this->config['paypal_test'])) {
            $this->gateway = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        }
    }

    public function create_setting_from()
    {
        G('loader')->helper('form');
        $elements = array();
        $elements[] = array(
            'title' => 'PayPal国际帐号',
            'des' => '到款帐号，请先到 www.paypal.com 申请帐号(Email)。',
            'content' => form_input('paypal_email', S('pay:paypal_email'), 'txtbox2'),
        );
        $elements[] = array(
            'title' => '支付币种',
            'des' => 'PayPal的支付币种，中文站的帐号只能收取人民币(CNY)，输入代号有：CNY,USD等；同时您必须在PayPal开启接受当前设置的支付币种。',
            'content' => form_input('paypal_currency', S('pay:paypal_currency'), 'txtbox2'),
        );
        $elements[] = array(
            'title' => '使用PayPal的测试平台',
            'des' => '使用测试平台，可以进行虚拟帐号充值，用于环境测试和调试，并不会产生真实的资金变动。测试帐号注册地址：www.sandbox.paypal.com',
            'content' => form_bool('paypal_test',S('pay:paypal_test')),
        );
        return $elements;
    }

    function check_setting_from()
    {
        $post = parent::check_setting_from();
        if(!$post) return false;
        $post['paypal_test'] = (int)$_POST['paypal_test'];
        return $post;
    }

    function get_unid() {
        $payid = $_POST['item_number']? $_POST['item_number'] : $_GET['item_number'];
        return trim($payid);
    }

    function get_payment_orderid() {
        $orderid = $_POST['trade_no']? $_POST['trade_no'] : $_GET['trade_no'];
        return trim($orderid);
    }

    function goto_pay($payid,$unid) {
        if(!$pay = $this->pay->read($payid)) redirect('pay_order_empty');
        $price = $pay['price'];
        $title = $pay['order_name'];
        $goods = $pay['goods'] ? unserialize($pay['goods']) : array();
        $content = $this->create_payurl($title, $price, $unid, $goods);
        if(!$content) redirect('pay_tenpay_url_empty');
        echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
        echo '<html><head>';
        echo '<meta http-equiv="Content-Type" content="text/html; charset='.$this->charset.'">';
        echo '</head>';
        echo '<body onload="javascript:document.paysubmit.submit();">';
        echo $content;
        echo '</body></html>';
        exit();
    }

    function create_payurl($title, $price, $unid, $goods=array()) {
        //create param
        $parameter = array(
            "cmd"           => "_xclick", // "_xclick" buy
            "business"      => $this->config['paypal_email'], //PayPal acc
            "currency_code" => $this->config['paypal_currency'], //USD,EUR,GBP,CAD,JPY
            "amount"        => $price,
            "item_name"     => $title,
            "item_number"   => $unid,
            "notify_url"    => $this->notify_url,
            "return"        => $this->return_url . '&item_number='.$unid.'&total='.$price, //callback
            "charset"       => $this->charset,

            "on0"           => $this->create_sign($unid . $this->config['paypal_email'] . $this->config['paypal_currency']),
        );
        //create order
        $content = "<form id='paysubmit' name='paysubmit' action='".$this->gateway."' method='post'>\r\n";
        foreach($parameter as $key => $val) {
            $content .= "<input type='hidden' name='".$key."' value='".$val."'/>\r\n";
        }
        $content .= '<span>Please wait a moment.....</span>';
        $content .= "</form>";
        return $content;
    }

    function notify_check() {

        $this->_log_result('POST:'.arrayeval($_POST));


        $sign = $this->create_sign($_POST['item_number'].$_POST['receiver_email'].$_POST['mc_currency']);
        if(strcmp($_POST['option_name1'], $sign) == 0) {

            $orderid = $_POST['item_number'];           //modoer orderid
            $item_name = $_POST['item_name'];           //name
            $total = $_POST['mc_gross'];                //total price
            $port_orderid = $_POST['txn_id'];           //payapl orderid
            $receiver_email = $_POST['receiver_email']; //receive email
            $payer_email = $_POST['payer_email'];       //pay email
            $payment_currency = $_POST['mc_currency'];
            $payment_status = $_POST["payment_status"];

            if (trim($payment_status) != "Completed") {
                $this->_log_result ($orderid . ':' . $port_orderid . ' : ' . $payment_status);
                return false;
            } elseif($receiver_email != $this->config['paypal_email']) {
                $this->_log_result ($orderid . ':' . $port_orderid . ' : not equal: ' . $receiver_email);
                return false;
            } else {
                //$this->log->update($orderid, $total, $port_orderid);
                $this->_log_result ($orderid . ':' . $port_orderid . ' : OK!');
                return true;
            }
        } else {
            $this->_log_result ($orderid . ':' . $port_orderid . ' : INVALID');
            return false;
        }
    }

    function create_sign($str) {
        return md5($this->global['cfg']['authkey'] . $str);
    }
}

/** end **/