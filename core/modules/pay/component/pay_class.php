<?php
!defined('IN_MUDDER') && exit('Access Denied');

define('MS_PAY_PAYMENT_PATH', MUDDER_CORE.'modules'.DS.'pay'.DS.'component'.DS.'payment'.DS);

class mc_pay {

	/**
	 * 产生的错误信息
	 * @var array
	 */
	protected static $error = array(
		'msg'	=> '',
		'code'	=> 0,
	);

	/**
	 * 实例化一个支付类
	 * @param  string $payment 支付接口标识
	 * @param  boolean $return_classname 是否值返回一个类名（用于静态调用）
	 * @return mixed
	 */
	final public static function factory($payment, $return_classname = false)
	{
		if(!$payment) return self::set_error('未指定支付接口');

		$classname = (substr($payment, 0,8)=='payment_'?'':'payment_').$payment;
		if(!class_exists($classname)) {
			if(!class_exists($classname)) {
				show_error(lang('global_class_not_found', $classname));
			}
		}

		return $return_classname ? $classname : new $classname;
	}

	/**
	 * 自动加载支付接口类
	 * @return boolean
	 */
	public static function autoload($classname)
	{
		if(class_exists($classname)) return true;
		if('payment_' != substr($classname, 0, 8)) return;
		$filepath = MS_PAY_PAYMENT_PATH . substr($classname, 8).'_class.php';
		if(!is_file($filepath)) return;
		include_once $filepath;
		return class_exists($classname);
	}

	/**
	 * 获取当前类代码执行后产生的最后一个错误
	 * @return [type] [description]
	 */
	public static function get_last_error()
	{
		return self::$error;
	}

	/**
	 * 获取所有已存在的支付接口
	 * @param  string $supper_platform 只获取支持某个平台的接口
	 * @return array 
	 */
	public static function get_payments($filter_enable = false, $supper_platform = null)
	{
		$classes = array();
		$di_cls = new \DirectoryIterator ( MS_PAY_PAYMENT_PATH );
		foreach ( $di_cls as $info ) {
			 if ($info->isFile() && substr($info->getFilename(), -10) == '_class.php') {
				//判断类文件是否是接口类
				$classname = 'payment_'.substr($info->getFilename(),0,-10);
				$RFC = new \ReflectionClass($classname);
				if($RFC->isSubclassOf('mc_pay_payment')) {
					if($filter_enable && !self::is_enable($classname)) continue;
					if($supper_platform && !$classname::support_platform($supper_platform)) continue;
					$classes[] = $classname;
				}
				
			 }
		 }
		 return $classes;
	}

	/**
	 * 检测一个支付接口是否存在
	 * @param  string $payment 接口类名
	 * @return boolean
	 */
	public static function exists($payment)
	{
		return class_exists($payment);
	}

	/**
	 * 启用一个支付接口
	 * @param  string $payment 支付接口类名
	 * @return [type]          [description]
	 */
	public static function enable($payment)
	{
		if(!self::exists($payment)) {
			return self::set_error('支付接口不存在。');
		}
		if(!$payment::check_setting()) {
			return self::set_error('支付接口配置信息未设置。');
		}
		$cfg_key = substr($payment, 8);
		G('loader')->model('config')->save_one($cfg_key, '1', 'pay');
		return true;
	}

	/**
	 * 停用一个支付接口
	 * @param  string $payment 支付接口类名
	 * @return [type]          [description]
	 */
	public static function disable($payment)
	{
		if(!self::exists($payment)) {
			return self::set_error('支付接口不存在。');
		}
		$cfg_key = substr($payment, 8);
		G('loader')->model('config')->save_one($cfg_key, '0', 'pay');
		return true;
	}

	/**
	 * 保存支付配置信息
	 * @param  string $payment [description]
	 * @return [type]          [description]
	 */
	public static function save_setting($payment)
	{
		$payment_cls = self::factory($payment);
		if(!$payment_cls) return false;
		$setting = $payment_cls->check_setting_from();
		if(!$setting) return self::set_error($payment_cls->error(),$payment_cls->erron());
		G('loader')->model('config')->save($setting, 'pay');
		return true;
	}

	/**
	 * 检测这个接口是否可用
	 * @param  [type]  $payment [description]
	 * @return boolean          [description]
	 */
	public static function is_enable($payment)
	{
		$cfg_key = substr($payment, 8);
		return S("pay:{$cfg_key}")?true:false;
	}

	protected static function set_error($msg, $code = -1)
	{
		self::$error['msg'] = $msg;
		self::$error['code'] = $code;
	}
}

spl_autoload_register( 'mc_pay::autoload' );

/* end */