<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
(!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied');

$grade_mdl = $_G['loader']->model('item:grade');
$op = _input('op','',MF_TEXT);

switch($op) {
    case 'post':
        if(is_array($_POST['grades'])) {
            $grade_mdl->update($_POST['grades']);
        }
        if($_POST['newgrade']['name']) {
            $grade_mdl->save($_POST['newgrade']);
        }
        redirect('global_op_succeed', get_forward(cpurl($module,$act,'list')));
        break;
    case 'delete':
        if(!is_array($_POST['ids'])||!$_POST['ids']) {
            redirect('global_op_unselect');
        }
        $grade_mdl->delete($_POST['ids']);
        redirect('global_op_succeed', get_forward(cpurl($module,$act,'list')));
        break;
    case 'access':
        if(check_submit('dosubmit')) {
            $post = $grade_mdl->get_post($_POST);
            $grade_mdl->save($post, $_POST['id']);
            redirect('global_op_succeed', get_forward(cpurl($module,$act,'list'),1));
        } else {
            $detail = $grade_mdl->read($_GET['id']);
            $access =& $detail['access'];
            $admin->tplname = cptpl('grade_access', MOD_FLAG);
        }
        break;
    default:
        $op = $_GET['op'] = 'list';
        $list = $grade_mdl->read_all();
        $admin->tplname = cptpl('grade_list', MOD_FLAG);
}