<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<div id="body">
<form method="post" name="myform" action="<?=cpurl($module,$act,$op)?>">
    <div class="space">
        <div class="subtitle">主题等级权限管理</div>
        <table class="maintable" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="45%" class="altbg1"><strong>等级名称:</strong>填写主题等级名称</td>
                <td width="*">
                    <input type="text" name="name" value="<?=$detail['name']?>" />
                </td>
            </tr>
            <tr>
                <td class="altbg1"><strong>禁用主题风格功能:</strong>在当前等级下的主题禁止使用主题风格</td>
                <td><?=form_bool('access[item_template_disabled]', $access['item_template_disabled'])?></td>
            </tr>
            <?php foreach($_G['modules'] as $k  => $v) :
                if($v['flag'] == MOD_FLAG) continue;
                $hookfile  = MUDDER_MODULE . $v['flag']  . DS .'admin' . DS . 'templates' . DS . 'hook_subjectgrade_access.tpl.php';
                if(!is_file($hookfile)) continue;
            ?>
            <tr class="altbg2"><td colspan="2"><center><b><?=$v['name']?></b></center></td></tr>
            <?php include $hookfile ?>
            <?php endforeach;?>
        </table>
        <center>
            <input type="hidden" name="forward" value="<?=get_forward()?>" />
            <input type="hidden" name="id" value="<?=$_GET['id']?>" />
            <input type="submit" name="dosubmit" value=" 提交 " class="btn" />
            <input type="button" value=" 返回 " onclick="history.go(-1);" class="btn" />
        </center>
    </div>
</form>
</div>