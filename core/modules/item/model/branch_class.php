<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

define('BRANCH_ERR_EXISTS', 110001);
define('BRANCH_ERR_INDEX_NOT_FOUND', 110011);

class msm_item_branch extends ms_model
{
    
    public $table = 'dbpre_subject_branch';
    public $key = 'id';

    public $index_mdl = null;

    function __construct()
    {
        parent::__construct();
        $this->model_flag = 'item';
        $this->index_mdl = _G('loader')->model('item:branch_index');
    }

    function read_by_name($name, $city_id)
    {
        return $this->db->from($this->table)
            ->where('hash', $this->create_hash($name, $city_id))
            ->get_one();
    }

    /**
     * 通过商户名称和所在城市，获取分店标识ID
     * @param  string $name 
     * @param  integer $city_id
     * @return integer
     */
    function get_id($name, $city_id)
    {
        return $this->db->from($this->table)
            ->select('id')
            ->where('hash', $this->create_hash($name, $city_id))
            ->get_value();
    }

    /**
     * 添加一个新分店统计数据
     * @param  string $name
     * @param  integer $city_id
     * @return integer          返回分店标识ID
     */
    function add_branch($sid, $name, $city_id)
    {
        $data = $this->index_mdl->read_by_sid($sid);
        if($data) return $this->add_error('数据已添加。', BRANCH_ERR_EXISTS);

        $branch_id = $this->get_id($name, $city_id);
        if(!$branch_id) {
            $post = array();
            $post['hash'] = $this->create_hash($name, $city_id);
            $post['name'] = $name;
            $branch_id = parent::save($post);
            if(!$branch_id) return;
            $branch_id = $this->db->insert_id();            
        }

        $post = array();
        $post['branch_id'] = $branch_id;
        $post['sid'] = $sid;
        $index_id = $this->index_mdl->save($post);

        if($index_id > 0) {
            $this->_update_number($branch_id, 'total', 1);
        }

        return true;
    }

    /**
     * 删除一家分店统计数据
     * @param  integer $sid 商户ID
     * @return boolean
     */
    function del_branch($sid)
    {
        $data = $this->index_mdl->read_by_sid($sid);
        if(!$data) return $this->add_error('数据不存在。', BRANCH_ERR_INDEX_NOT_FOUND);

        if($this->index_mdl->delete($data['id'])) {
            $branch = $this->read($data['branch_id']);
            if($branch['total'] <= 1) {
                //删除
                parent::delete($data['branch_id']);
            } else {
                //减少统计
                $this->_update_number($data['branch_id'], 'total', -1);
            }
        }
    }

    /**
     * 生成一个店名/品牌名的唯一hash
     * @param  [type] $name    [description]
     * @param  [type] $city_id [description]
     * @return [type]          [description]
     */
    function create_hash($name, $city_id)
    {
        return md5($name.'_'.$city_id);
    }

}

/**
 * 分店统计索引表
 */
class msm_item_branch_index extends ms_model
{
    var $table = 'dbpre_subject_branch_index';
    var $key = 'id';

    function __construct()
    {
        parent::__construct();
        $this->model_flag = 'item';
    }

    /**
     * 通过商户ID获取数据
     * @param  integer $sid
     * @return array
     */
    function read_by_sid($sid)
    {
        return $this->db->from($this->table)
            ->where('sid', $sid)
            ->get_one();
    }

    function find_sids($branch_id)
    {
        $r = parent::find_all('sid', array('branch_id'=>$branch_id));
        if(!$r) return false;

        $result = array();
        while ($v = $r->fetch_array()) {
            $result[] = $v['sid'];
        }
        return $result;
    }

}
/** end */