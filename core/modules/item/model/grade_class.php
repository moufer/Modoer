<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

class msm_item_grade extends ms_model {

    var $table = 'dbpre_subjectgrade';
    var $key = 'id';
    var $model_flag = 'item';
    var $cache_name = 'grade';
    var $auto_check_write = TRUE;

    static public $grade;

    //获取全部等级配置信息
    static function fetch_grades()
    {
        if(!self::$grade)
        {
            self::$grade = _G('loader')->variable('grade', 'item');
        }
        return self::$grade;
    }

    //通过id获取指定的等级配置信息
    static function fetch_grade($id)
    {
        $grades = self::fetch_grades();
        if( ! isset($grades[$id])) 
        {
            return;            
        }
        return $grades[$id];
    }

    function __construct() {
        parent::__construct();
        $this->modcfg = $this->variable('config');
        $this->init_field();
    }

    function init_field() {
        $this->add_field('name,color,access,price');
        $this->add_field_fun('grades,color', '_T');
        $this->add_field_fun('price', 'intval');
    }

    function read($id) {
        $result = parent::read($id);
        $result['access'] = $result['access'] ? unserialize($result['access']) : array();
        return $result;
    }

    function read_all() {
        $this->db->from($this->table)->select('id,name,color,price');
        return $this->db->get_all();
    }

    function update(& $post) {
        if(!is_array($post)) return;
        foreach($post as $key => $val) {
            $this->save($val, $key, FALSE);
        }
        $this->write_cache();
    }

    function save($post,$id=null) {
        $edit = $id!=null;
        if($edit) {
            if(!is_numeric($id) || $id < 1) redirect(lang('global_sql_keyid_invalid','id'));
            if(!$detail=$this->read($id)) redirect('等级信息不存在');
            isset($post['access']) && $post['access'] = $post['access'] ? serialize($post['access']) : '';
        }
        $id = parent::save($post,$id);
        return $id;
    }

    function write_cache($return = FALSE) {
        //debug_print_backtrace();
        $result = $access  =  array();
        $this->db->from($this->table);
        if($r=$this->db->get()) {
            $cache = ms_cache::factory();
            while($value = $r->fetch_array()) {
                $value['access'] = $value['access'] ? unserialize($value['access']) : array();
                //write_cache($this->cache_name.'_'.$value['id'], arrayeval($value), $this->model_flag);
                $result[$value['id']] = $value;
            }
            $r->free_result();

            $cache->write('item_' . $this->cache_name, $result);
            //write_cache($this->cache_name, arrayeval($result), $this->model_flag);            
        }

        if($return) return $result;
    }

    function write_cache_access($id, $return = FALSE) {
        if(!$result = $this->read($id)) return;
        $cache = ms_cache::factory()->write('item_' . $this->cache_name . '_' . $value['id'], $result);
        //write_cache($this->cache_name.'_'.$id, arrayeval($result), $this->model_flag);
        if($return) return $result;
    }

    function check_post(& $post, $edit = FALSE) {
        if($edit && isset($post['name']) && !$post['name']) redirect('未填写话题等级名称。');
        if(!$edit && !$post['name']) redirect('未填写话题等级名称。');
    }
}
?>