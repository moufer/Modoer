<?php
/**
* 主题等级组件
*/
class mc_item_grade extends ms_base
{

	//实例化过的等级组件类
	protected static $cls_cache = array();

	//所有等级数组
	protected static $grades = null;

	//权限配置
	protected $_access = null;

	/**
	 * 新建一个等级类
	 * @param  [type] $subject [description]
	 * @return [type]          [description]
	 */
	public static function create($subject)
	{
		if(!self::$cls_cache[$subject['sid']]) {
			self::$cls_cache[$subject['sid']] = new self($subject);
		}
		return self::$cls_cache[$subject['sid']];
	}

	public function __construct($subject)
	{
		parent::__construct();
		if(is_null(self::$grades)) {
			self::$grades = $this->loader->variable('grade', 'item');
		}
		$this->set_subject($subject);
	}

	/**
	 * 获取权限参数值
	 * @param  [type] $key [description]
	 * @return [type]      [description]
	 */
	public function __get($key)
	{
		return isset($this->_access[$key]) ? $this->_access[$key] : false;
	}

	/**
	 * 设置当前主题内容数组信息
	 * @param array $subject
	 */
	public function set_subject($subject)
	{
		if(is_numeric($subject)) {
			$this->subject = $this->loader->model('item:subject')->read($subject,'*',FALSE);
		} elseif(is_array($subject)) {
			$this->subject = $subject;
		}
		if(!$this->subject) return $this->add_error('item_empty');
		//载入规则配置
		$this->load_access();
	}

	/**
	 * 获取全部等级参数数组
	 * @return array
	 */
	public function fetch_all_access()
	{
		if(!$this->_access) return false;
		return $this->_access;
	}

	/**
	 * 根据等级ID获取等级权限数组
	 * @return array [description]
	 */
	private function load_access()
	{
		$grade_id = $this->subject['level'];
		$this->_access = self::$grades[$grade_id]?self::$grades[$grade_id]['access']:array();
	}

}

/* end */