<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

_G('loader')->helper('form');
$op = _input('op');
$space = new mc_space;
$space->set_user($user->uid);

switch ($op) {

	case 'upload_background':

		//上传图片
		if(_input('upload') && _post('dosubmit')) {
			$svr_obj = new ms_service();
			$upload_obj = ms_upload::image('picture', array(
				'user_watermark' => false, //不打水印
			));
			if(!$upload_obj->upload('space')) {
				$svr_obj->add_error($upload_obj);
			} else {
				$svr_obj->bg_url = $upload_obj->url;
				$space->config->bg_url = $upload_obj->url;
				$space->space_obj->save_config($space);
			}
			echo $svr_obj->fetch_json();
			output();
		} elseif(_post('reset')=='Y') {
			ms_attachment::delete_file($space->config->bg_url);
			//恢复默认设置
			$svr_obj = new ms_service();
			$space->config->bg_url = '';
			$space->space_obj->save_config($space);
			echo $svr_obj->fetch_json();
			output();
		}

		$tplname = 'upload_background';
		break;
	
	default:
		if(!check_submit('dosubmit')) {
			//自动判断和创建空间
			$space->space_obj->create($user->uid, $user->username);
			$detail = $space->space_obj->read($user->uid);

			$tplname = 'config';

		} else {

			$post = $space->space_obj->get_post($_POST);
			$space->space_obj->save($post, $user->uid);

			redirect('global_op_succeed', url("space/member/ac/$ac"));
		}
}

/** end */