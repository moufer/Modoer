<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

$_G['manage_menu']['weixin']['title'] = '微信微站';
$_G['manage_menu']['weixin'][] = '公众号设置,weixin/member/ac/g_mp';
$_G['manage_menu']['weixin'][] = '自定义指令,weixin/member/ac/g_cmd';
$_G['manage_menu']['weixin'][] = '自定义菜单,weixin/member/ac/g_menu';
//$_G['manage_menu']['weixin'][] = '对话消息管理,weixin/member/ac/g_message';
$_G['manage_menu']['weixin'][] = '微站内容管理,weixin/member/ac/g_manage';
$_G['manage_menu']['weixin'][] = '微站模板管理,weixin/member/ac/g_template';

/* end */