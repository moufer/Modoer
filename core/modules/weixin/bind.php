<?php
!defined('IN_MUDDER') && exit('Access Denied');

$bind_mdl = _G('loader')->model('weixin:member_bind');

//提交绑定
if(check_submit('dosubmit')) {

	//开始绑定
	if($bind_mdl->bind($_POST)) {
		redirect('绑定成功！', U('mobile/index'));
	} else {
		if($bind_mdl->has_error()) {
			redirect($bind_mdl->error());
		} else {
			redirect('绑定失败！');
		}
	}

} else {

	//绑定参数验证
	if($data = !$bind_mdl->check_sign()) {
		redirect($bind_mdl->error());
	}

	//检测是否已经绑定
	$bind_data = $bind_mdl->exists($data[0], $data[1]);
	if($bind_data) {
		redirect('您的微信帐号已绑定，无法二次绑定。');
	}

	$_HEAD['title'] = $header_title = "绑定微信帐号";
	include mobile_template('weixin_bind');

}