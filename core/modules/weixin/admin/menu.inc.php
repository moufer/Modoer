<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
(!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied');

$site = mc_weixin_role::read_site();
if(!$site) redirect('weixin_config_mp_empty');

$menu_obj = new mc_weixin_menu($site);

$op = _input('op',null,MF_TEXT);
switch ($op) {

	case 'edit':

		$cmd_manage_obj = new mc_weixin_cmd_manage();
		$cmds = $cmd_manage_obj->get_installed_cmds();

		_G('loader')->helper('form','weixin');
		$admin->tplname = cptpl('menu_save', MOD_FLAG);
		break;

	case 'get_menus':

		$menus = $menu_obj->get_menus();
		if(!$menus) {
			echo $menu_obj->error_json();
			output();
		}
		echo json_encode($menus);
		output();
		break;

	case 'post':

		$post = $_POST['button'];
		$result = $menu_obj->create_menus($post);
		if(!$result) {
			echo $menu_obj->error_json();
			output();
		} else {
			echo json_encode(array('code'=>200));
			output();
		}
		break;

	default:
	
		$admin->tplname = cptpl('menu', MOD_FLAG);
		break;
}
/** end **/
?>