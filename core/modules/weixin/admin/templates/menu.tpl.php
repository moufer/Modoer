<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<div id="body">
<div class="sub-menu">
    <div class="sub-menu-heading">自定义菜单管理</div>
</div>

<div class="remind">
    <p>使用自定义菜单功能，您的微信公众平台号必须通过认证才能使用。</p>
    <p>创建自定义菜单后，由于微信客户端缓存，需要24小时微信客户端才会展现出来。建议测试时可以尝试取消关注公众账号后再次关注，则可以看到创建后的效果。</p>
</div>

<form method="post" action="<?=cpurl($module,$act)?>&">
    <div class="space">
        <div class="subtitle">自定义菜单管理</div>
        <ul class="weixin-menu th">
            <li class="name">菜单名称</li>
            <li class="type">菜单类型</li>
            <li class="value">类型值</li>
            <li class="op">操作</li>
        </ul>
        <div id="menu_box"></div>
        <center id="operation_button">
            <button type="button" class="btn secondary" id="add_root_menu_btn">添加父菜单</button>
            <button type="button" class="btn" id="submit_btn">提交编辑</button>
        </center>
    </div>
</form>
<script type="text/javascript" src="static/javascript/weixin.form.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    weixin.custom_menu({
        get:'<?=cpurl($module,$act,'get_menus')?>',
        edit:'<?=cpurl($module,$act,'edit')?>',
        post:'<?=cpurl($module,$act,'post')?>'
    });
});
</script>
<style type="text/css">
    .form-item{margin-bottom:10px;}
    .form-item label{display: block; margin-bottom:5px;}
    .form-item input{margin:3px 0; display: block;}
    .form-item input.inline{display:inline-block;}
    .form-item span.helper{color: #808080;}

    .weixin-menu-root {  margin-top:10px; position: relative; }
    .weixin-menu-parent { margin-left:20px; position: relative; }

    .weixin-menu { border:1px solid #ddd;background:#FFF;position: relative; z-index:10;}
    .weixin-menu:hover { background:#F4f4f4; }
    .weixin-menu:last-child {border-bottom:1px solid #ddd;}
    .weixin-menu:after { content:" "; display:block; height:0; clear:both; visibility:hidden; }
    .weixin-menu.th { background:#EEE;}
    .weixin-menu.sub { margin-left:20px; margin-top:-1px; z-index:11; }
    .weixin-menu > li { float:left; padding:8px 10px 6px; line-height:16px; 
        white-space:nowrap;text-overflow:ellipsis; overflow-x:hidden;}
    .weixin-menu > li:last-child { border-left:0; };
    .weixin-menu > .listorder { width:80px; }
    .weixin-menu > .name { width:180px; }
    .weixin-menu > .type { width:180px; }
    .weixin-menu > .value { width:300px; }
    .weixin-menu > .op { width:230px; }
    .weixin-menu > .op > a { margin-right:5px; }

    .weixin-menu-parent > .weixin-menu { border-top:0; }
    .weixin-menu-parent > .weixin-menu > .name { width:160px; }

    #menu_box { margin-bottom:10px; }
    #menu_box > .message {
        border:1px solid #DDD;
        padding:30px 10px;
        line-height:18px;
        font-size:16px;
        text-align: center;
        background: #FFF;
        border-top: 0;
    }
</style>