<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<div id="body">
<form method="post" name="myform" action="<?=cpurl($module,$act,'post')?>">
    <div class="space">
        <div class="subtitle">微信设置</div>
        <?if($edit_links):?>
        <ul class="cptab">
            <?foreach($edit_links as $val):?>
            <li<?if($val['flag']=='weixin:subjectsetting')echo' class="selected"';?>><a href="<?=$val['url']?>" onfocus="this.blur()"><?=$val['title']?></a></li>
            <?endforeach;?>
        </ul>
        <?endif;?>
        <table class="maintable" border="0" cellspacing="0" cellpadding="0" id="config1">
            <tr>
                <td class="altbg1" valign="top" width="45%"><strong>是否允许商家绑定微信号:</strong>
                    <br />
                    <span class="font_1">此处设置为-1表示不进行分成，留空则按默认商城模块配置里的分成比率计算。</span>
                </td>
                <td width="*">
                    <?=form_radio('use_bind', array(0=>'默认',1=>'是',-1=>'否'), $setting['use_bind'])?>
                </td>
            </tr>
        </table>
    </div>
	<center>
		<input type="hidden" name="sid" value="<?=$sid?>">
		<?=form_submit('dosubmit',lang('admincp_submit'),'yes','btn')?>
	</center>
</form>
</div>