<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<form name="menu_form" method="post" action="<?=cpurl($module,$act,'post')?>">
    <div class="form-item">
        <label>菜单名称：</label>
        <input type="text" name="name" value="" class="txtbox2" />
        <span class="helper">微信内一级菜单最多4个汉字，二级菜单最多7个汉字，多出来的部分将会以“...”代替。</span>
    </div>
    <div class="form-item">
        <label>菜单类型：</label>
        <select name="type">
            <option value="root">父菜单</option>
            <option value="view">页面链接(view)</option>
            <option value="click">指令点击(click)</option>
            <option value="location_select">地理位置选择(location_select)</option>
            <option value="scancode_waitmsg">二维码扫描(scancode_waitmsg)</option>
            <option value="pic_photo_or_album">手机相册拍照传图(pic_photo_or_album)</option>
            <option value="pic_weixin">微信相册传图(pic_weixin)</option>
        </select>
    </div>
    <div class="form-item none" data-name="view" data-type="option">
        <label>页面URL：</label>
        <input type="text" name="url" id="type_url" value="http://" class="txtbox2" />
        <select id="cmds" onchange="$('#type_url').val($(this).find(':selected').val());">
            <option value="">==快捷URL==</option>
            <?=form_weixin_site_links($site_obj)?>
        </select>
        <span class="helper">请输入手机web页面url</span>
    </div>
    <div class="form-item none" data-name="click" data-type="option">
        <label>选择触发指令：</label>
        <input type="text" name="key" id="type_click" value="" class="t_input" size="20" />
        <select id="cmds" onchange="$('#type_click').val($(this).find(':selected').val());">
            <option value="">==可使用指令==</option>
            <?=form_weixin_cmds('','site','click',$site_obj)?>
        </select>
        <span class="helper">填写指令标志或其他指令识别码</span>
    </div>
    <div class="form-item none" data-name="location_select" data-type="option">
        <label>选择触发指令：</label>
        <select name="key" id="type_location_select">
            <option value="">==可使用指令==</option>
            <?=form_weixin_cmds('','site','location_select',$site_obj)?>
        </select>
    </div>
    <div class="form-item none" data-name="scancode_waitmsg" data-type="option">
        <label>选择触发指令：</label>
        <select name="key" id="type_scancode_waitmsg">
            <option value="">==可使用指令==</option>
            <?=form_weixin_cmds('','site','scancode_waitmsg',$site_obj)?>
        </select>
    </div>
    <div class="form-item none" data-name="pic_photo_or_album" data-type="option">
        <label>选择触发指令：</label>
        <select name="key" id="type_pic_photo_or_album">
            <option value="">==可使用指令==</option>
            <?=form_weixin_cmds('','site','pic_photo_or_album',$site_obj)?>
        </select>
    </div>
    <div class="form-item none" data-name="pic_weixin" data-type="option">
        <label>选择触发指令：</label>
        <select name="key" id="type_pic_weixin">
            <option value="">==可使用指令==</option>
            <?=form_weixin_cmds('','site','pic_weixin',$site_obj)?>
        </select>
    </div>
    <center>
        <button type="button" class="btn" data-type="submit">添加</button>
        <button type="button" class="btn unimportant" data-type="close">关闭</button>
    </center>
</form>