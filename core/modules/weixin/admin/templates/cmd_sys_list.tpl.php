<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<div id="body">
<div class="sub-menu">
    <div class="sub-menu-heading">对话指令管理</div>
    <a href="<?=cpurl($module,$act,'list',array('type'=>'custom'))?>" class="sub-menu-item">自定义指令</a>
    <a href="<?=cpurl($module,$act,'list',array('type'=>'sys'))?>" class="sub-menu-item selected">系统指令</a>
</div>
<div class="remind">
    <p>微信指令文件存在 core/modules/weixin/component/cmd 文件夹下，请勿删除基础指令文件（help.php 和 welcome.php）</p>
    <p>适用对象(role)：*：表示全部、site：表示系统网站、subject：表示商户微站</p>
</div>
<form method="post" action="<?=cpurl($module,$act)?>&">
    <div class="space">
        <table class="maintable" border="0" cellspacing="0" cellpadding="0" trmouse="Y">
            <tr class="altbg1">
                <td width="200">文件名</td>
                <td width="150">指令标识</td>
                <td width="300">指令作用</td>
                <td width="300">适用对象(role)</td>
                <td width="*">操作</td>
            </tr>
            <?foreach($cmd_files as $cmd):?>
            <tr>
                <?php
                    $cmd_obj = new $cmd();
                    $name=str_replace('weixin_cmd_','',$cmd);
                    $desc = $cmd_obj->describe();
                    if(!$desc) continue;
                ?>
                <td><?=$name?>.php</td>
                <td><?=$desc['keyword']?></td>
                <td><?=$desc['intro']?></td>
                <td>
                    <?=is_string($desc['roles'])?$desc['roles']:implode(',', $desc['roles'])?>
                </td>
                <td>
                    <?if(in_array($name, $installed_cmds)):?>
                        <?if(in_array($name, $base_cmds)):?>
                        <span class="font_3">基础指令，不能卸载</span>
                        <?else:?>
                        <a href="<?=cpurl($module,$act,'uninstall',array('class'=>$cmd))?>"><span class="font_2">卸载</span></a>
                        <?endif;?>
                    <?else:?>
                        <a href="<?=cpurl($module,$act,'install',array('class'=>$cmd))?>">安装</a>
                    <?endif;?>
                </td>
            </tr>
            <?endforeach;?>
            <?if(empty($cmd_files)):?>
            <tr>
                <td colspan="4">暂无信息</td>
            </tr>
            <?endif;?>
        </table>
    </div>
</form>
</div>