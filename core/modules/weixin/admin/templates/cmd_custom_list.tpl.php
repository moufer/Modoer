<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<div id="body">
<div class="sub-menu">
    <div class="sub-menu-heading">对话指令管理</div>
    <a href="<?=cpurl($module,$act,'list',array('type'=>'custom'))?>" class="sub-menu-item selected">自定义指令</a>
    <a href="<?=cpurl($module,$act,'list',array('type'=>'sys'))?>" class="sub-menu-item">系统指令</a>
</div>
<div class="remind">
    <p>当前设置的自定义指令为官方网站的微信号使用，商户的自定义指令请在前台商户管理处设置。</p>
</div>
<form method="post" action="<?=cpurl($module,$act)?>&">
    <div class="space">
<table class="maintable" border="0" cellspacing="0" cellpadding="0" trmouse="Y">
            <tr class="altbg1">
                <td width="30">选?</td>
                <td width="80">回复类型</td>
                <td width="120">关键字</td>
                <td width="*">指令介绍</td>
                <td width="100"><center>默认/关注回复</center></td>
                <td width="100"><center>已触发</center></td>
                <td width="*">操作</td>
            </tr>
            <?if($list)while($val=$list->fetch_array()):?>
            <tr>
                <td><input type="checkbox" name="ids[]" value="<?=$val['id']?>" /></td>
                <td><?=$val['msgtype']?></td>
                <td><?=$val['keyword']?></td>
                <td><?=$val['intro']?></td>
                <td>
                    <center>
                        <?=$val['is_default']?"●":"○"?>
                        &nbsp;&nbsp;
                        <?=$val['is_subscribe']?"●":"○"?>
                    </center>
                </td>
                <td><center><?=$val['replys']?></center></td>
                <td><a href="<?=cpurl($module,$act,'edit',array('id'=>$val['id']))?>">编辑</a></td>
            </tr>
            <?endwhile;?>
            <?if(empty($list)):?><tr><td colspan="8">暂无信息</td></tr><?endif;?>
        </table>
    </div>
    <center>
        <button type="button" class="btn secondary" onclick="jslocation('<?=cpurl($module,$act,'add')?>');">添加新指令</button>
        <?if($list):?>
        <button type="submit" name="dosubmit" value="yes" class="btn">删除所选</button>
        <?endif;?>
    </center>
</form>
</div>