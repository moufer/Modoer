<?php 
	(!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); 
	_G('loader')->helper('form');
	_G('loader')->helper('form','weixin');
?>
<div id="body">
<div class="sub-menu">
	<div class="sub-menu-heading">对话指令管理</div>
	<a href="<?=cpurl($module,$act,'list',array('type'=>'custom'))?>" class="sub-menu-item selected">自定义指令</a>
	<a href="<?=cpurl($module,$act,'list',array('type'=>'sys'))?>" class="sub-menu-item">系统指令</a>
</div>
<form name="cmd_save" method="post" action="<?=cpurl($module,$act,'save')?>">
	<div class="space">
		<table class="maintable" >
			<tr>
				<td class="altbg1" width="45%">
					<strong>关键字</strong>
					即用户在微信内回复的文字，如果关键字匹配，则会触发本条指令进行回复。
				</td>
				<td width="*">
					<input type="text" name="keyword" class="txtbox3" value="<?=$post['keyword']?>" />
				</td>
			</tr>
			<tr>
				<td class="altbg1">
					<strong>指令名称</strong>
					微信用户在回复help指令时显示。
				</td>
				<td>
					<input type="text" name="name" class="txtbox2" value="<?=$post['name']?>" />
				</td>
			</tr>
			<tr>
				<td class="altbg1">
					<strong>指令描述</strong>
					描述下本条指令的作用。
				</td>
				<td>
					<input type="text" name="intro" class="txtbox" value="<?=$post['intro']?>" />
				</td>
			</tr>
			<tr>
				<td class="altbg1">
					<strong>回复类型</strong>
					在公众平台高级功能页面，可以找到；微信公众平台账号如果没认证则没有AppID，也无法使用自定义菜单。
				</td>
				<td>
					<?=form_radio('msgtype',array('text'=>'文本','news'=>'图文'), $post['msgtype'])?>
				</td>
			</tr>
			<tr class="J_msgtype msgtype-text">
				<td class="altbg1">
					<strong>回复内容</strong>
					微信文本可以识别URL连接（中文连接无法识别）。
				</td>
				<td>
					<textarea name="content" class="txtarea"><?=$post['content']?></textarea>
				</td>
			</tr>
			<tr class="J_msgtype msgtype-news">
				<td class="altbg1">
					<strong>回复内容</strong>
					图文信息最多10条。第一条为大图，大图尺寸建议360*200，小图建议200*200。<br />图文描述仅在只有1条图文信息时显示在微信中。
				</td>
				<td>
					<ul class="msgtype-news-group"></ul>
					<button type="button" id="add_news_btn" class="btn2">新增一条</button>
				</td>
			</tr>
			<tr>
				<td class="altbg1">
					<strong>是否为默认回复</strong>
					当用户发送的指令无法识别时，总是回复本条指令（需要在参数设置里开启）。
					<br />当前设置为唯一性（设置当前指令后，其他指令将被自动取消）。
				</td>
				<td>
					<?=form_bool('is_default',$post['is_default'])?>
				</td>
			</tr>
			<tr>
				<td class="altbg1">
					<strong>是否为关注回复</strong>
					当用户在微信内关注当前公众平台帐号时，自动发送当前指令给关注者；<br />当前设置为唯一性。
				<td>
					<?=form_bool('is_subscribe',$post['is_subscribe'])?>
				</td>
			</tr>
		</table>
	</div>
	<center>
		<input type="hidden" name="before_op" value="<?=$op?>" />
		<?if($op=='edit'):?>
		<input type="hidden" name="id" value="<?=$id?>" />
		<?endif;?>
		<input type="hidden" name="mp_id" value="<?=$site_obj->id?>" />
		<button type="submit" name="dosubmit" value="yes" class="btn">提交表单</button>
		<button type="button" class="btn unimportant" onclick="jslocation('<?=cpurl($module,$act,'list')?>');">返回</button>
	</center>
</form>

<form class="J_form none" method="post" enctype="multipart/form-data" action="<?=U('modoer/upload/in_ajax/1')?>">
	<table class="maintable">
		<tr>
			<td class="altbg1" align="right" width="80"><span class="font_1">*</span>标题：</td>
			<td width="*">
				<?=form_input('Title','','txtbox2')?>
			</td>
		</tr>
		<tr>
			<td class="altbg1" align="right">描述：</td>
			<td width="*">
				<?=form_input('Description','','txtbox2')?>
			</td>
		</tr>
		<tr>
			<td class="altbg1" align="right"><span class="font_1">*</span>连接：</td>
			<td width="*">
				<?=form_input('Url','http://','txtbox2')?>
				<select id="cmds" onchange="$(this).prev().val($(this).find(':selected').val());">
					<option value="">==快捷URL==</option>
					<?=form_weixin_site_links($site_obj)?>
				</select>
			</td>
		</tr>
		<tr>
			<td class="altbg1" align="right"><span class="font_1">*</span>图片：</td>
			<td>
				<input type="file" name="picture" accept="image/*" />
				<input type="hidden" name="set_domain" value="N" />
			</td>
		</tr>
		</table>
		<center>
			<button type="submit" class="btn" name="dosubmit" value="yes">提交</button>&nbsp;&nbsp;
			<button type="button" class="btn unimportant" data-type="close">关闭</button>
		</center>
</form>
</div>

<style>
	.msgtype-news-item {
	    position: relative;
	    display: block;
	    padding: 10px;
	    margin-bottom: -1px;
	    background-color: #fff;
	    border: 1px solid #ddd;
	}
	.msgtype-news-item:last-child {
		margin-bottom:10px;
	}
	.msgtype-news-item .fl {
	    float:left!important;
	}
	.msgtype-news-item .fr {
	    float:right!important;
	}
	.msgtype-news-item .richtxt,
	.msgtype-news-item .richtxt-body {
	    overflow: hidden;
	   	line-height:20px;
	}
	.msgtype-news-item .richtxt .fl {
	    margin-right: 10px;
	}
	.msgtype-news-item .richtxt .richtxt-img {
	    display: block;
	    border:1px solid #ddd;
	    margin:auto;
	    width:60px;
	    height:60px;
	}
	.msgtype-news-item:first-child .richtxt .richtxt-img {
	    width:108px;
	    height:60px;
	}
	.msgtype-news-item .richtxt-body > div > span {
		display: block;
		color: #888;
	}
	.msgtype-news-item .richtxt-body > div > span.Title {
		font-weight: bold;
	}
	.msgtype-news-item .richtxt-body > .opt {

	}
	.msgtype-news-item .richtxt-body > .opt > a.blk {
		display: block;
	}
</style>
<script type="text/javascript" src="static/javascript/weixin.form.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var news_list = new Array();
		<?if($post['msgtype']=='news' && is_array($post['content']) && $post['content']):?>
		<?foreach($post['content'] as $key => $params):?>
		news_list.push({
			Title:'<?=$params['Title']?>',
			Description:'<?=$params['Description']?>',
			Url:'<?=$params['Url']?>',
			PicUrl:'<?=$params['PicUrl']?>'
		});
		<?endforeach;?>
		<?endif;?>

		var cmd = new weixin.custom_cmd();
		$.each(news_list, function(index, val) {
			cmd.add_item(val);
		});

		<?if($error):?>msgOpen("<span class=\"font_1\"><?=$error?></span>");<?endif;?>
	});
</script>