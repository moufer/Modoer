<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied');?>
<div id="body">
<div class="sub-menu" data-container="div.space > table.maintable">
    <div class="sub-menu-heading">模块配置</div>
    <a href="#" class="sub-menu-item selected">功能设置</a>
    <a href="#" class="sub-menu-item">网站微信号设置</a>
</div>
<form method="post" action="<?=cpurl($module,$act)?>">
    <input type="hidden" name="classsort" value="1" />
    <div class="space">
        <table class="maintable">
            <tr>
                <td width="45%" class="altbg1">
                    <strong>允许商户绑定微信公众号:</strong>
                    允许商户在前台主题管理内绑定微信公众号以及附属（自定义指令、菜单等）功能。
                </td>
                <td width="*">
                    <select id="use_bind" name="modcfg[use_bind][]" multiple="true">
                        <?foreach(query_item::category(array('pid'=>0)) as $val):vp($val);?>
                        <option value="<?=$val['catid']?>"<?if($modcfg['use_bind'] && in_array($val['catid'], $modcfg['use_bind']))echo' selected="selected"';?>><?=$val['name']?></option>
                        <?endforeach;?>
                    </select>
                    <script type="text/javascript">
                    $('#use_bind').mchecklist({width:'95%',height:60,line_num:2});
                    </script>
                </td>
            </tr>
            <tr>
                <td class="altbg1">
                    <strong>启用默认回复（无法识别指令时）:</strong>
                    会员用户发送的信息，系统无法识别其含义时，自动回复上面的欢迎；
                    不使用本功能时，则不会向用户发送任何信息。
                    <span class="font_1">只有打开此功能，多用户公众号设置里才能使用或者关闭默认回复。反之此处关闭，所有多用户公众号的默认回复功能都将关闭。</span>
                </td>
                <td width="*"><?=form_bool('modcfg[use_default_cmd]', $modcfg['use_default_cmd'])?></td>
            </tr>
        </table>
        <table class="maintable"style="display:none;">
            <tr>
                <td width="45%" class="altbg1">
                    <strong>网站微信号：</strong>进入<a href="https://mp.weixin.qq.com" target="_blank">微信公众平台</a> - 账号信息内查看到
                </td>
                <td width="*"><?=form_input('mp[wechat]', $site->wechat, 'txtbox')?></td>
            </tr>
            <tr>
                <td class="altbg1">
                    <strong>服务器地址：</strong>
                    把右边的URL地址填写到微信公众平台后台-开发者中心，URL（服务器地址）中。
                </td>
                <td><input type="text" class="txtbox" value="<?=S('siteurl')?>api/weixin/index.php" readonly></td>
            </tr>
            <tr>
                <td class="altbg1">
                    <strong>公众号Token:</strong>进入<a href="https://mp.weixin.qq.com" target="_blank">微信公众平台</a>后，启用高级功能里的“开发模式”，
                    可以填写Token和URL，把您填写的Token复制填写到此处，并始终保持两边一致；
                </td>
                <td width="*"><?=form_input('mp[token]', $site->token, 'txtbox')?></td>
            </tr>
            <tr>
                <td class="altbg1"><strong>AppID:</strong>获取位置同上
                </td>
                <td width="*"><?=form_input('mp[appid]', $site->appid, 'txtbox')?></td>
            </tr>
            <tr>
                <td class="altbg1"><strong>AppSecret:</strong>同上</td>
                <td width="*"><?=form_input('mp[appsecret]', $site->appsecret, 'txtbox')?></td>
            </tr>
            <tr>
                <td class="altbg1"><strong>EncodingAESKey:</strong>消息加解密密钥，获取位置同上</td>
                <td width="*"><?=form_input('mp[config][encode_key]', $site->config->encode_key, 'txtbox')?></td>
            </tr>
            <tr>
                <td class="altbg1"><strong>消息加解密方式:</strong>网站与微信通信的加密方式，选择加密模式时，请务必填写上方的EncodingAESKey;</td>
                <td width="*"><?=form_radio('mp[config][encode_type]', array('0'=>'明文模式','1'=>'加密模式'), $site->config->encode_type)?></td>
            </tr>
            <tr>
                <td class="altbg1">
                    <strong>显示默认回复：</strong>
                    当用户回复的指令无法识别时，总是回复默认指令（默认指令在自定义指令设置）。
                </td>
                <td>
                    <?=form_bool('mp[config][use_default_cmd]',$site->config->use_default_cmd)?>
                </td>
            </tr>
        </table>
        <center><button type="submit" name="dosubmit" value="yes" class="btn"> 提交 </button></center>
    </div>
</form>
</div>