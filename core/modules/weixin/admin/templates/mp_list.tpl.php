<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<style type="text/css">
.img img { max-width:80px; max-height:60px; border:1px solid #ccc; padding:1px; 
    _width:expression(this.width > 80 ? 80 : true); _height:expression(this.height > 60 ? 60 : true); }
</style>
<div id="body">
    <div class="sub-menu">
        <div class="sub-menu-heading">商家公众号管理</div>
    </div>
    <form method="get" action="<?=SELF?>">
        <input type="hidden" name="module" value="<?=$module?>" />
        <input type="hidden" name="act" value="<?=$act?>" />
        <input type="hidden" name="op" value="<?=$op?>" />
        <div class="space">
            <table class="maintable" >
                <tr>
                    <td width="100" class="altbg1">所属地区</td>
                    <td width="300">
                        <?if($admin->is_founder):?>
                        <select name="city_id">
                            <option value="">不限</option>
                            <?=form_city($_GET['city_id'], TRUE)?>
                        </select>
                        <?else:?>
                        <?=$_CITY['name']?>
                        <?endif;?>
                    </td>
                    <td width="100" class="altbg1"></td>
                    <td width="*">
                    </td>
                </tr>
                <tr>
                    <td class="altbg1">公众号表ID</td>
                    <td><input type="text" name="id" class="txtbox3" value="<?=$_GET['id']?>" /></td>
                    <td class="altbg1">主题SID</td>
                    <td><input type="text" name="sid" class="txtbox3" value="<?=$_GET['sid']?>" /></td>
                </tr>
                <tr>
                    <td class="altbg1">结果排序</td>
                    <td colspan="3">
                        <select name="orderby">
                            <option value="id"<?=$_GET['orderby']=='id'?' selected="selected"':''?>>默认排序</option>
                        </select>&nbsp;
                        <select name="ordersc">
                            <option value="DESC"<?=$_GET['ordersc']=='DESC'?' selected="selected"':''?>>递减</option>
                            <option value="ASC"<?=$_GET['ordersc']=='ASC'?' selected="selected"':''?>>递增</option>
                        </select>&nbsp;
                        <select name="offset">
                            <option value="20"<?=$_GET['offset']=='20'?' selected="selected"':''?>>每页显示20个</option>
                            <option value="50"<?=$_GET['offset']=='50'?' selected="selected"':''?>>每页显示50个</option>
                            <option value="100"<?=$_GET['offset']=='100'?' selected="selected"':''?>>每页显示100个</option>
                        </select>&nbsp;
                        <button type="submit" value="yes" name="dosubmit" class="btn2">筛选</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
    <div class="space">
        <table class="maintable" trmouse="Y">
            <tr class="altbg1">
                <td width="*">所属主题</td>
                <td width="250">公众号</td>
                <td width="150">状态</td>
                <td width="150">操作</td>
            </tr>
            <?if($total):?>
            <?while($val=$list->fetch_array()):?>
            <tr data-id="<?=$val['id']?>">
                <td><a href="<?=url("item/detail/id/$val[role_id]")?>" target="_blank"><?=$val['name'].$val['subname']?></a></td>
                <td><?=$val['wechat']?></td>
                <td><?=$val['status']>0?'正常':($val['status']<0?'关闭':'未审核')?></td>
                <td>
                    <?if($val['status']>0):?>
                    <a href="javascript:" data-name="off">关闭</a>&nbsp;
                    <?else:?>
                    <a href="<?=cpurl($module,$act,'on',array('id'=>$val['id']))?>" data-name="on">允许绑定</a>&nbsp;
                    <?endif;?>
                    <a href="javascript:" data-name="delete">删除</a>
                </td>
            </tr>
            <?endwhile;?>
            <?if($multipage):?>
            <tr class="altbg1">
                <td colspan="5" style="text-align:right;"><?=$multipage?></td>
            </tr>
            <?endif;?>
            <?else:?>
            <tr><td colspan="5">暂无信息。</td></tr>
            <?endif?>
        </table>
    </div>
</div>
<form class="J_form_off none" method="post" action="<?=cpurl($module,$act,'off')?>">
    <table class="maintable">
        <tr>
            <td class="altbg1" width="100">关闭理由：</td>
            <td width="*">
                <textarea name="message" class="txtarea2"></textarea><br />
                <span class="font_2">填写的内容会提醒给主题管理员</span>
            </td>
        </tr>
    </table>
    <center>
        <input type="hidden" name="id" value="" />
        <button type="submit" name="dosubmit" value="yes" class="btn">提交表单</button>&nbsp;
        <button type="button" class="btn unimportant" data-name="close">关闭</button>&nbsp;
    </center>
</form>
<form class="J_form_delete none" method="post" action="<?=cpurl($module,$act,'delete')?>">
    <table class="maintable">
        <tr>
            <td colspan="2" class="altbg1">
                <span class="font_1">您确定要删除当前绑定的微信公众号吗？<br />删除时，将同时删除所有相关数据（自定义指令，微站内容管理）</span>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p>关闭理由：</p>
                <textarea name="message" class="txtarea2"></textarea><br />
                <span class="font_2">填写的内容会提醒给主题管理员</span>
            </td>
        </tr>
    </table>
    <center>
        <input type="hidden" name="id" value="" />
        <button type="submit" name="dosubmit" value="yes" class="btn">删除</button>&nbsp;
        <button type="button" class="btn unimportant" data-name="close">关闭</button>&nbsp;
    </center>
</form>
<script type="text/javascript">
$(document).ready(function() {

    $('table tr[data-id]').each(function() {
        var tr = $(this);
        var id = $(this).data('id');

        //关闭公众号绑定连接处理
        tr.find('a[data-name="off"]').click(function(event) {
            event.preventDefault();
            mp_off(id);
        });

        //删除微信号处理
        tr.find('a[data-name="delete"]').click(function(event) {
            event.preventDefault();
            mp_delete(id);
        });

    });

    function mp_off(id) {
        var form = $('form.J_form_off').clone(true)
        form.find('[name="id"]').val(id);
        form.form_dialog({dialog:{width:550,title:'关闭绑定'}}).open();
    }

    function mp_delete(id) {
        var form = $('form.J_form_delete').clone(true)
        form.find('[name="id"]').val(id);
        form.form_dialog({dialog:{title:'删除微站数据'}}).open();
    }

});
</script>