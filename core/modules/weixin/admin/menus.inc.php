<?php
(!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied');
$modmenus = array(
    'weixin|模块配置|config',
    'weixin|商户公众号管理|mp',
    'weixin|对话指令管理|cmd',
    'weixin|自定义菜单管理|menu',
    'weixin|微站首页模板管理|template',
);

/** end */