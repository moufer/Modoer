<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
(!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied');

$C  = $_G['loader']->model('config');
$mp = _G('loader')->model('weixin:mp');

if($_POST['dosubmit']) {

	$use_bind = $_POST['modcfg']['use_bind'];
	if($use_bind && is_array($use_bind)) {
		$use_bind = implode(',', $use_bind);
	} else {
		$use_bind = '';
	}
	$_POST['modcfg']['use_bind'] = $use_bind;

    $C->save($_POST['modcfg'], MOD_FLAG);

    //保存网站公众号配置信息
	$detail	= $mp->read_site();
	$id = $detail['id'] ? $detail['id'] : null;

	$post	= $mp->get_post($_POST['mp']);
	$post['role'] = 'site';
	$post['role_id'] = 0;
	
	$result = $mp->save($post, $id);
	if(!$result) redirect($mp->error());

    redirect('global_op_succeed', cpurl($module, 'config'));

} else {

    $modcfg = $C->read_all(MOD_FLAG);

    $modcfg['use_bind'] = $modcfg['use_bind']?explode(',', $modcfg['use_bind']):array();

    //获取主题分类模型
    //$cate_mdl = _G('loader')->model('item:category');
    _G('loader')->helper('query','item');

    //读取网站的微信公众号配置信息
    $site = mc_weixin_role::read_site();
	!$site && $site = new stdClass;

    $admin->tplname = cptpl('config', MOD_FLAG);
}

/** end **/