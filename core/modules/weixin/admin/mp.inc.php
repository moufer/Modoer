<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
(!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied');

$mp_mdl = _G('loader')->model('weixin:mp');

$op = _input('op', null, MF_TEXT);
switch($op) {

    case 'on':
    case 'off':
        $id = _input('id', 0, MF_INT_KEY);
        $message = _post('message', '', MF_TEXT); //提醒用户
        if($mp_mdl->update_on_off($id, $op, $message)) {
            redirect('global_op_succeed', get_forward(cpurl($module,$act)));
        } elseif($mp_mdl->has_error()) {
            redirect($mp_mdl->error());
        } else {
            redirect('没有数据被更新！');
        }
        break;
    case 'delete':
        $id = _post('id', 0 , MF_INT_KEY);
        $message = _post('message', '', MF_TEXT); //提醒用户
        $mp_mdl->delete($id, $message);
        if($mp_mdl->has_error()) {
            redirect($mp_mdl->error());
        } else {
            redirect('global_op_succeed_delete', get_forward(cpurl($module,$act)));
        }
    default:
        $mp_mdl->db->join($mp_mdl->table, 'mp.role_id', 'dbpre_subject', 's.sid', 'LEFT JOIN');
        
        if(!$admin->is_founder) {
            $mp_mdl->db->where('s.city_id', $admin->check_global() ? array(0,$_CITY['aid']) : $_CITY['aid']);
        }
        if(is_numeric($_GET['city_id']) && $_GET['city_id'] >= 0) {
            $mp_mdl->db->where('s.city_id', $_GET['city_id']);
        }
        
        $mp_mdl->db->where('role','subject');
        $_GET['sid'] && $mp_mdl->db->where('role_id', _get('sid',0,MF_INT_KEY));
        //$mp_mdl->db->where('mp.status',1);

        if($total = $mp_mdl->db->count()) {
            $mp_mdl->db->select('mp.*,s.name,s.subname');
            $mp_mdl->db->sql_roll_back('from,where');
            !$_GET['orderby'] && $_GET['orderby'] = 'id';
            !$_GET['ordersc'] && $_GET['ordersc'] = 'DESC';
            $mp_mdl->db->order_by('mp.'.$_GET['orderby'], $_GET['ordersc']);
            $mp_mdl->db->limit(get_start($_GET['page'], $_GET['offset']), $_GET['offset']);
            $list = $mp_mdl->db->get();
            $multipage = multi($total, $_GET['offset'], $_GET['page'], cpurl($module,$act,'list',$_GET));
        }

        $admin->tplname = cptpl('mp_list', MOD_FLAG);
    	   break;
}

/** end */