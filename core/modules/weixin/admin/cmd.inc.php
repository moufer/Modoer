<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
(!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied');

$cmd_obj = new mc_weixin_cmd_manage();
$cmd_files = $cmd_obj->get_cmd_list();

$installed_cmds = S('weixin:cmds');
$installed_cmds = $installed_cmds ? array_unique(explode(',', $installed_cmds)) : array();
//基础指令
$base_cmds = array('welcome','help');

//网站公众号
$site_obj = mc_weixin_role::read_site();
$custom_mdl = _G('loader')->model('weixin:custom_cmd');

$op = _input('op', null, MF_TEXT);
switch($op) {
    case 'install':
        	$class = _input('class','',MF_TEXT);
        	$name = str_replace('weixin_cmd_', '', $class);
        	$filename = MUDDER_MODULE.'weixin'.DS.'component'.DS.'cmd'.DS. $name.'.php';
        	if(!is_file($filename)) redirect('安装的指令文件不存在。'.$filename);
        	if(!in_array($name, $installed_cmds)) {
        		$installed_cmds[] = $name;
        		$post = array('cmds'=>implode(',', $installed_cmds));
        		$_G['loader']->model('config')->save($post ,'weixin');
        		location(cpurl($module,$act));
        	}
        	break;

    case 'uninstall':
        	$class = _input('class','',MF_TEXT);
        	$name = str_replace('weixin_cmd_', '', $class);
        	if(in_array($name,$base_cmds)) {
        		redirect('对不起，您不能删除基础指令。');
        	}
        	$i = array_search($name, $installed_cmds);
        	if(is_numeric($i)) {
        		unset($installed_cmds[$i]);
        		$post = array('cmds'=>implode(',', $installed_cmds));
        		$_G['loader']->model('config')->save($post ,'weixin');
        		location(cpurl($module,$act));
        	}
        	break;

    case 'add':
        $post = array();
        $post['msgtype'] = 'text';

        $admin->tplname = cptpl('cmd_custom_post', MOD_FLAG);
        break;
    case 'edit':
        $id = _get('id', null, MF_INT_KEY);
        $post = $custom_mdl->read($id);
        if(!$post) redirect('对不起，数据不存在。');
        if($post['mp_id'] != $site_obj->id) redirect('对不起，您查看的数据不属于当前关联的公众平台主题。');

        if($post['msgtype']=='news' && $post['content']) {
            $post['content'] = ms_mxml::to_array($post['content']);
        }

        $admin->tplname = cptpl('cmd_custom_post', MOD_FLAG);
        break;

    case 'delete':
        $ids = _post('ids',null,MF_INT_KEY);
        $ids = $custom_mdl->get_keyids($ids);
        foreach ($ids as $id) {
            $m = $custom_mdl->read($id);
            if(!$m || $m['mp_id']!=$role_obj->id) redirect('对不起，数据不存在或不属于当前主题。');
        }
        $custom_mdl->delete($ids);

        redirect('global_op_succeed_delete', cpurl($module,$act,'',array('type'=>'custom')));
        break;

    case 'save':
        $id = null;
        if(_post('before_op')=='edit') {
            $id = _post('id', 0, MF_INT_KEY);
        }
        $post = $custom_mdl->get_post($_POST);
        $newid = $custom_mdl->save($post, $id);
        if($custom_mdl->has_error()) {
            $error = $custom_mdl->error();
            $op = $before_op;
            $admin->tplname = cptpl('cmd_custom_post', MOD_FLAG);
        } else {
            redirect('global_op_succeed', cpurl($module,$act,'',array('type'=>'custom')));
        }
        break;

    default:
        $type = _get('type','custom',MF_TEXT);
        if($type!='sys') {

            if(!$site_obj) redirect('weixin_config_mp_empty');

            $where = array(
                'mp_id' => $site_obj->id,
            );
            $list = $custom_mdl->find_all('*', $where);
            $admin->tplname = cptpl('cmd_custom_list', MOD_FLAG);

        } else {

            $admin->tplname = cptpl('cmd_sys_list', MOD_FLAG);
        }
    	   break;
}