<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

$sid = $_G['manage_subject']['sid'];

$role_obj = mc_weixin_role::read_by_sid($sid);
if(!$role_obj) redirect('对不起，您未开启微站或者未配置微站参数。',U('weixin/member/ac/g_mp'));

if(!$role_obj->check_use_bind()) {
	redirect('网站未打开微信公众号绑定功能。');
}

$custom_obj = _G('loader')->model('weixin:custom_cmd');
_G('loader')->helper('form', 'weixin');

$op = _input('op',null,MF_TEXT);
switch ($op) {

	case 'add':

		$post = array();
		$post['msgtype'] = 'text';
		$tplname = 'g_cmd_save';
		break;

	case 'edit':

		$id = _get('id', null, MF_INT_KEY);
		$post = $custom_obj->read($id);
		if(!$post) redirect('对不起，数据不存在。');
		if($post['mp_id']!=$role_obj->id) redirect('对不起，您查看的数据不属于当前关联的公众平台主题。');
		
		if($post['msgtype']=='news' && $post['content']) {
			$post['content'] = ms_mxml::to_array($post['content']);
		}

		$tplname = 'g_cmd_save';
		break;

	case 'delete':

		$ids = _post('ids',null,MF_INT_KEY);
		$ids = $custom_obj->get_keyids($ids);
		foreach ($ids as $id) {
			$m = $custom_obj->read($id);
			if(!$m || $m['mp_id']!=$role_obj->id) redirect('对不起，数据不存在或不属于当前主题。');
		}
		$custom_obj->delete($ids);

		redirect('global_op_succeed_delete', U("weixin/member/ac/$ac"));
		break;

	case 'save':
		$id = null;
		if(_post('before_op')=='edit') {
			$id = _post('id', 0, MF_INT_KEY);
		}
		$post = $custom_obj->get_post($_POST);
		$newid = $custom_obj->save($post, $id);
		if($custom_obj->has_error()) {
			$error = $custom_obj->error();
			$op = $before_op;
			$tplname = 'g_cmd_save';
		} else {
			redirect('global_op_succeed', U("weixin/member/ac/$ac"));
		}
		break;

	default:

		$where = array();
		$where['mp_id'] = $role_obj->id;
		$list = $custom_obj->find_all('*',$where);
}

/** end **/