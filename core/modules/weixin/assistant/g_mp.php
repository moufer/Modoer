<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

$sid = $_G['manage_subject']['sid'];

if($_POST['dosubmit']) {

	$mp = _G('loader')->model('weixin:mp');

	$detail = $mp->read_by_sid($sid);
	$id = $detail['id'] ? $detail['id'] : null;

	$post = $mp->get_post($_POST);
	$post['role'] = 'subject';
	$post['role_id'] = $sid;

	$result = $mp->save($post, $id);
	if(!$result) redirect($mp->error());

	redirect('global_op_succeed', url('weixin/member/ac/g_mp'));

} else {

	$role_obj = mc_weixin_role::read_by_sid($sid);
	if(!$role_obj) {
		$role_obj = new stdClass;
		$config = S('weixin:use_bind')?explode(',', S('weixin:use_bind')):array();
		$use_bind = $config && in_array($_G['manage_subject']['pid'], $config);
	} else {
		$use_bind = $role_obj->check_use_bind();
	}

	$disabled = false;
	if(!$use_bind) {
		$error = '网站未打开微信公众号绑定功能。';
		$disabled = true;
	}
}

/** end **/