<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

$sid = $_G['manage_subject']['sid'];

$role_obj = mc_weixin_role::read_by_sid($sid);
if(!$role_obj) redirect('对不起，您未开启微站或者未配置微站参数。',U('weixin/member/ac/g_mp'));

if(!$role_obj->check_use_bind()) {
	redirect('网站未打开微信公众号绑定功能。');
}

$menu_obj = new mc_weixin_menu($role_obj);

$op = _input('op',null,MF_TEXT);
switch ($op) {

	case 'edit':

		$cmd_manage_obj = new mc_weixin_cmd_manage();
		$cmds = $cmd_manage_obj->get_installed_cmds();

		_G('loader')->helper('form','weixin');

		$tplname = 'g_menu_save';
		break;

	case 'get_menus':

		$menus = $menu_obj->get_menus();
		if(!$menus) {
			echo $menu_obj->error_json();
			output();
		}
		echo json_encode($menus);
		output();
		break;

	case 'post':

		$post = $_POST['button'];
		$result = $menu_obj->create_menus($post);
		if(!$result) {
			echo $menu_obj->error_json();
			output();
		} else {
			echo json_encode(array('code'=>200));
			output();
		}
		break;

	default:

}

/** end **/