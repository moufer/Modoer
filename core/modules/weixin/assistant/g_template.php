<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

$sid = $_G['manage_subject']['sid'];

$role_obj = mc_weixin_role::read_by_sid($sid);
if(!$role_obj) redirect('对不起，您未开启微站或者未配置微站参数。',U('weixin/member/ac/g_mp'));

if(!$role_obj->check_use_bind()) {
	redirect('网站未打开微信公众号绑定功能。');
}

$site_mdl = _G('loader')->model('weixin:site');
$site_data = $site_mdl->read_by_sid($sid);

$template_mdl = _G('loader')->model('template');

$op = _input('op',null,MF_TEXT);
switch ($op) {

	case 'use':

		$template_id = _post('id', 0, MF_INT_KEY);
		$site_mdl->set_template($site_data['id'], $template_id);

		if($site_mdl->has_error()) {
			redirect($site_mdl->error());
		}
		echo 'OK';
		output();
		
		break;

	default:

		$tpl_list = $template_mdl->read_all('weixin');

}


/** end **/