<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

$sid = $_G['manage_subject']['sid'];

$role_obj = mc_weixin_role::read_by_sid($sid);
if(!$role_obj) redirect('对不起，您未开启微站或者未配置微站参数。',U('weixin/member/ac/g_mp'));

if(!$role_obj->check_use_bind()) {
	redirect('网站未打开微信公众号绑定功能。');
}

$site_mdl = _G('loader')->model('weixin:site');
$site_data = $site_mdl->read_by_sid($sid);

_G('loader')->helper('form','weixin');

$op = _input('op',null,MF_TEXT);
switch ($op) {

	case 'navs':

		if(!$site_data) redirect('对不起，您尚未设置微站。', U("weixin/member/ac/$ac"));

		if($_POST['dosubmit']) {
			$site_mdl->save_navs($site_data['id'], $_POST['array']);
			if($site_mdl->has_error()) redirect($site_mdl->error());
			redirect('global_op_succeed', U("weixin/member/ac/$ac/op/$op"));
		} else {
			$post = ms_mxml::to_array($site_data['navs']);
			$tplname = 'g_site_navs';
		}
		break;

	case 'pics':

		if(!$site_data) redirect('对不起，您尚未设置微站。', U("weixin/member/ac/$ac"));

		if($_POST['dosubmit']) {
			$site_mdl->save_pics($site_data['id'], $_POST['array']);
			if($site_mdl->has_error()) redirect($site_mdl->error());
			redirect('global_op_succeed', U("weixin/member/ac/$ac/op/$op"));
		} else {
			$post = ms_mxml::to_array($site_data['pics']);
			$tplname = 'g_site_pics';
		}
		break;

	default:

		if($_POST['dosubmit']) {
			$mp = _G('loader')->model('weixin:mp')->read_by_sid($sid);
			$_POST['mp_id'] = $mp['id'];
			$_POST['sid'] = $sid;
			$id = $site_mdl->save_config($_POST, $site_data?$site_data['id']:null);
			if(!$site_mdl->has_error()) redirect('global_op_succeed', U("weixin/member/ac/$ac"));
			$error = $site_mdl->error();
		} else {
			if(!$site_data) {
				$site_data = array();
			} else {
				$post = ms_mxml::to_array($site_data['config']);
			}
		}	
}


/** end **/