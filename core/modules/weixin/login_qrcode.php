<?php
!defined('IN_MUDDER') && exit('Access Denied');

$op = _input('op','',MF_TEXT);
if($op == 'poll') {

	$service_obj = new ms_service();
	$scan_info = G('session')->wx_scan;
	if(DEBUG) $service_obj->info = $scan_info;
	if($scan_info['passport_id']>0) {
		//扫描成功,同时数据库内已存在账号，进行登录
		$service_obj->scan = true;
		$service_obj->bind = true;
		$service_obj->login = G('user')->login->passport_login($scan_info['passport_id'], 3600*24*30);
		G('session')->wx_scan = null;
	} elseif ($scan_info['openid']) {
		//扫描成功，数据库内不存在账号，提示自动登录或绑定
		$service_obj->scan = true;
		$service_obj->bind = false;
	} else {
		$service_obj->scan = false;
	}
	echo $service_obj->fetch_all_attr('json');
	output();

} elseif($op == 'bind') {

	$openid = G('session')->wx_scan['openid'];
	$oauth_cls = ms_oauth2::factory('mpweixin');
	if(!$openid) {
		$token = $oauth_cls->getTokenFromSession();
		if(!$token) redirect('请先扫描微信二维码',U('member/login'));
		$openid = $token->id;
		$userinfo = array('username' => G('session')->wx_nickname);
	} else {
		$token = new stdClass;
		$token->name = 'mpweixin';
		$token->id = $openid;
		$userinfo = array('username' => G('session')->wx_scan['nickname']);
		G('session')->wx_nickname = G('session')->wx_scan['nickname'];
		$oauth_cls->setTokenSession($token);
		G('session')->wx_scan = null;
	}

	$passport_obj = $_G['loader']->model('member:passport');
	//检测是否已经绑定过
	$passport = $passport_obj->get_data('mpweixin', $openid);
	if($passport) {
		G('user')->login->passport_login($passport['id'], 3600*24*30);
		redirect('您已绑定，已登录。',U('member/index'));
	}

	$passport_type  = $token->name;
	$passport_id    = $token->id;
	$username       = $userinfo['username'];
	$passport       = $username && $passport_id;

	if(!$passport) redirect('请先扫描微信二维码.',U('member/login'));

	$MP = $_G['loader']->model('member:profile');

	$typename   = lang('member_passport_type_'.$oauth_cls->name);
	$title      = lang('member_passport_login', array($typename, $username, $_G['sitename']));
	$subtitle   = lang('member_login_title');

	//绑定登录表单提交URL
	$form_action = U("member/passport/op/bind/source/weixin/rand/{$_G['random']}");

	if($in_mobile) {
		require_once mobile_template('member_login');
	} else {
		require_once template('member_login');
	}


} elseif($op=='auto_reg') {

	$openid = G('session')->wx_scan['openid'];
	G('session')->wx_scan = null;

	if(!$openid) {
		redirect('请先扫描微信二维码',U('member/login'));
	}

	$passport_obj = $_G['loader']->model('member:passport');
	//检测是否已经绑定过
	$passport = $passport_obj->get_data('mpweixin', $openid);
	if($passport) {
		G('user')->login->passport_login($passport['id'], 3600*24*30);
		redirect('您已绑定，已登录。',U('member/index'));
	}

	//第三方账号在本地系统自动注册账号
	$token = new stdClass;
	$token->name = 'mpweixin';
	$token->id = $openid;

	$userinfo = array(
		'username' => G('session')->wx_scan['nickname'],
	);

	$reg_model = $_G['loader']->model('member:register');
	$uid = $reg_model->passport_add($token, $userinfo);
	if (!$uid) {
		if($reg_model->has_error()) {
			redirect($reg_model->error());
		}
		redirect('对不起，扫码登录失败！', U("member/login"));
	} else {
		//第三方网站账号绑定
		$passport_obj->bind($uid, $token);
	}

	//存在同步登录代码
	if($user->sync_code) {
		redirect(lang('member_login_succeed').$user->sync_code, U("member/index"));
	}
	location(U("member/index"));

} else {

	$ticket_time = 60;

	if($op=='refresh') {
		//新建一个二维码ticket类
		$ticket_cls = new mc_weixin_ticket(mc_weixin_role::read_site());
		//生成一个二维码内容地址，并附带自定义参数为客户端唯一参数
		$ticket = $ticket_cls->create(G('session')->get_keyid(),$ticket_time);
		//二维码图片地址
		$ticket_url = rawurlencode(str_replace(array('&amp;','&#38;'), '&', $ticket->url));
    		$qrcode_url = URLROOT . "/api/qr.php?content={$ticket_url}&mps=8";

		//刷新一张二维码
		$service_obj = new ms_service();
		$service_obj->qrcode_url = $qrcode_url;
		echo $service_obj->fetch_all_attr('json');
		output();
	}

	require_once template('weixin_login_qrcode');
}

/* end */