<?php
/**
 * 默认回复
 * 【作用于自定义回复，请绝对不要删除】
 */
class weixin_cmd_default extends mc_weixin_cmd
{
	protected $name = '默认回复';
	protected $keyword = '';
	protected $intro = "指令无法识别时的自动回复（打开默认回复功能下有效）。";

	protected $menu_types = array();

	public function describe()
	{
		return false;
	}

	public function match($message_obj, $role_obj, $session_obj)
	{
		return false;
	}

	public function execute($message_obj, $role_obj, $session_obj)
	{
		$session_obj->action = mc_weixin_session::ACTION_RESET;

		$cmd = mc_weixin_custom_cmd::read_default($role_obj->id);
		if(!$cmd) return false;
		
		//回复微信用户
		$cmd->reply($message_obj->FromUserName, $message_obj->ToUserName, $role_obj);
	}

}
/** end **/