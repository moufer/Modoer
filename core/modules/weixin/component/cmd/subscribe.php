<?php
/**
 * 关注回复
 * 微信用户在关注公众号时发送给关注着
 */
class weixin_cmd_subscribe extends mc_weixin_cmd
{
	protected $name = '关注回复';
	protected $keyword = '';
	protected $intro = "用户关注公众号时回复用户。";

	protected $menu_types = array();

	public function describe()
	{
		return false;
	}

	public function match($message_obj, $role_obj, $session_obj)
	{
		//用户关注时
		if($message_obj->MsgType == 'event' && $message_obj->Event == 'subscribe') {
			return true;
		}
	}

	public function execute($message_obj, $role_obj, $session_obj)
	{
		$session_obj->action = mc_weixin_session::ACTION_RESET;

		$cmd = mc_weixin_custom_cmd::read_subscribe($role_obj->id);
		if(!$cmd) return false;

		//回复微信用户
		$cmd->reply($message_obj->FromUserName, $message_obj->ToUserName, $role_obj);

		return true;
	}

}
/** end **/