<?php
/**
* 查看我的个人信息
*/
class weixin_cmd_my extends mc_weixin_cmd
{
	protected $name = '我的帐号信息';
	protected $keyword = 'my';
	protected $intro = "微信绑定网站帐号，显示网站帐号基本信息。";

	protected $menu_types = array('text','click');
	protected $roles = array('site','subject');

	public function match($message_obj, $site_obj, $session_obj)
	{
		$mark = strtolower($this->keyword);
		if($message_obj->MsgType == 'event' && $message_obj->Event == 'CLICK') {
			return strtolower($message_obj->EventKey) == $mark;
		} else {
			return $mark == strtolower(trim($message_obj->Content));
		}
	}

	public function execute($message_obj, $site_obj, $session_obj)
	{
		$session_obj->action = mc_weixin_session::ACTION_RESET;

		$mp_openid 		= $message_obj->ToUserName; //微信公众号的原始ID（整个微信内部唯一）
		$user_openid 	= $message_obj->FromUserName; //绑定会员微信openid(仅对当前公众号有效)
		$user_unionid 	= ''; //微信会员唯一内部id，保留

		$bind_obj = new mc_weixin_bind($mp_openid, $user_openid, $user_unionid);
		if($bind_obj->has_error()) {
			$link = '<a href="'.$bind_obj->bind_link().'">请点击这里绑定微信帐号</a>';
			$erron = $bind_obj->erron();
			if($erron == -1) {
				$content = '对不起，您的微信帐号尚未绑定，'.$link;
			} elseif($erron == -2) {
				$bind_obj->unbind();//删除绑定记录
				$content = '对不起，被绑定账号已不存在，请重新绑定新账号，'.$link;
			} else {
				$content = '未知错误！';
			}
		} else {
			$content = $this->read_member($bind_obj->get_member_data());
		}

		//回复信息
		$reply_obj = mc_weixin_reply::factory('text');
		$reply_obj->set_user($message_obj->FromUserName, $message_obj->ToUserName);
		$reply_obj->set_content($content);
		$reply_obj->send($site_obj);

		return true;
	}

	protected function read_member($member) {
		$content = '';
		$content .= '绑定账号：'.$member['username'];
		$content .= "\n帐号等级：".display('member:group',"groupid/$member[groupid]");
		$content .= "\n点评数量：".$member['reviews'];
		$content .= "\n鲜花叔量：".$member['flowers'];
		return $content;
	}
}
/** end **/