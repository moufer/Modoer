<?php
/**
 * 扫码登录
 * 微信用户扫描二维码后自动登录
 */
class weixin_cmd_qrcode_login extends mc_weixin_cmd
{
	protected $name = '扫码登录';
	protected $keyword = '';
	protected $intro = "用户扫描公众号二维码自动登录。";

	protected $menu_types 	= array('');//scancode_push
	protected $roles			= array('site');

	public function match($message_obj, $role_obj, $session_obj)
	{
		//用户关注时
		if($message_obj->MsgType == 'event') {
			if($message_obj->Event == 'subscribe' && preg_match("/qrscene_(.*)/", $message_obj->EventKey)) {
				return true;
			} elseif ($message_obj->Event == 'SCAN' && $message_obj->EventKey) {
				return true;
			}
		}
	}

	public function execute($message_obj, $role_obj, $session_obj)
	{
		$session_obj->action = mc_weixin_session::ACTION_RESET;

		$openid = $message_obj->FromUserName;
		$sessionid = $message_obj->Event == 'subscribe' ? substr($message_obj->EventKey, 8) : $message_obj->EventKey;

		//log_write('login_qrcode', array($openid, $session, '======'));
		$scan_info		= array();
		$scan_info['openid'] = trim($openid);

		$passport_obj 	= G('loader')->model('member:passport');
		$passport 		= $passport_obj->get_data('mpweixin', $scan_info['openid']);

		$content = '';
		$content .= "扫码成功！\n";
		//判断是否已经被绑定过
		if($passport['uid'] > 0) {
			$scan_info['passport_id'] = $passport['id'];
		} else {
			$scan_info['passport_id'] = 0;
			$content = "请在电脑页面选择直接登录或者绑定网站已存在账号。\n";
		}

		$content .= "OpenID:$message_obj->FromUserName \n";
		$content .= "sessionID:$sessionid \n";
		$content .= "passport_id:{$scan_info['passport_id']}\n";

		if(!$scan_info['passport_id']) {
			$user = new mc_weixin_user($role_obj);
			$info = $user->info($message_obj->FromUserName);
			if($info) {
				$content .= "unionid:$info->unionid \n";
				$content .= "NickName:$info->nickname \n";
				$content .= "subscribe_time:$info->subscribe_time \n";
				$scan_info['unionid'] = trim($info->unionid);
				$scan_info['nickname'] = trim($info->nickname);
				$scan_info['subscribe_time'] = trim($info->subscribe_time);
			}
		}

		//保存扫码用户的session
		$session = G('db')->from('dbpre_session')->where('id',$sessionid)->get_one();
		$data = $session['content'] ? unserialize($session['content']) : array();
		!$data and $data = array();
		$data['wx_scan'] = $scan_info;
		G('db')->from('dbpre_session')->where('id',$sessionid)->set('content',serialize($data))->update();

         //回复用户发送位置信息
         $reply_obj = mc_weixin_reply::factory('text');
         $reply_obj->set_content($content);

         $reply_obj->set_user($message_obj->FromUserName, $message_obj->ToUserName);
		//回复消息
		$reply_obj->send($role_obj);

		//向websocket服务器发送扫码成功消息
		require MUDDER_ROOT.'api/WebSocket/src/WebsocketClient.php';
		$address = '0.0.0.0';
		$service_port = 9555;
		$data = 'SCAN_OK:'.$sessionid;
		$WebSocketClient = new WebsocketClient($address, $service_port);
		$websocketresult = $WebSocketClient->sendData('SCAN_OK:'.$sessionid);
 		unset($WebSocketClient);
 		if(DEBUG) log_write('websocket',
			date('Y-m-d H:i:s', time())."\n$data\n$websocketresult\n=========\n");

		return true;
	}

}
/** end **/