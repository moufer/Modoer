<?php
/**
* 查看指令列表
*/
class weixin_cmd_help extends mc_weixin_cmd
{
	protected $name = '查看可用指令';
	protected $keyword = 'help';
	protected $intro = "列举出系统内可被用户主动触发的指令";

	protected $menu_types = array('text','click');

	public function match($message_obj, $role_obj, $session_obj)
	{
		$mark = strtolower($this->keyword);
		if($message_obj->MsgType == 'event' && $message_obj->Event == 'CLICK') {
			return strtolower($message_obj->EventKey) == $mark;
		} else {
			return $mark == strtolower(trim($message_obj->Content));
		}
	}

	public function execute($message_obj, $role_obj, $session_obj)
	{
		$cmds = array();
		foreach (glob(MUDDER_MODULE.'weixin'.DS.'component'.DS.'cmd'.DS.'*.php') as $filename) {
			if(basename($filename,'.php') == 'base') continue;
			$classname = "weixin_cmd_".pathinfo(strtolower($filename), PATHINFO_FILENAME);
			$cmds[] = $classname;
		}

		if ($cmds) {
			$message = "指令列表：\n";
			foreach ($cmds as $cmd_class) {
				$tmp = new $cmd_class;
				$desc = $tmp->describe_by_role_obj($role_obj, 'text');
				if($desc) {
					!is_array($desc[0]) and $desc = array($desc);
					foreach ($desc as $val) {
						$message .= "\n发送：{$val['keyword']}\n{$val['name']}\n";
					}
				}
				unset($tmp);
			}
		} else {
			$message = "没有找到任何指令信息。";
		}




		$session_obj->action = mc_weixin_session::ACTION_RESET;

		$reply_obj = mc_weixin_reply::factory('text');
		$reply_obj->set_user($message_obj->FromUserName, $message_obj->ToUserName);
		$reply_obj->set_content($message);
		//回复消息
		$reply_obj->send($role_obj);
		return true;
	}

}
/** end **/