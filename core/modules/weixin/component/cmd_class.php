<?php
/**
 * 	x
 */
abstract class mc_weixin_cmd extends ms_base
{
	protected $name = '';
	protected $keyword = '';
	protected $intro = '';

	protected $menu_types = '*';
	protected $roles = '*';

	protected $role_obj;
	protected $session_obj;
	protected $message_obj;

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 获取当前指令类的可用指令集
	 * @return array
	 */
	public function describe()
	{
		return array(
			'name' => $this->name,
			'keyword' => $this->keyword,
			'intro' => $this->intro,
			'roles' => $this->roles
		);
	}

	public function describe_by_role_type($role_type, $menu_type)
	{
		if($this->role_exists($role_type) && $this->menu_type_exists($menu_type)) {
			return $this->describe();
		}
		return false;
	}

	public function describe_by_role_obj($obj, $menu_type)
	{
		$role_type = $obj->role;
		return $this->describe_by_role_type($role_type, $menu_type);
	}

	public function role_exists($role)
	{
		if(!$role) $role = '*';
		return $this->roles=='*' || $role==$this->roles || in_array($role, $this->roles);
	}

	public function menu_type_exists($type)
	{
		if(!$type) $type = '*';
		return $this->menu_types=='*' || $type == $this->menu_types || in_array($type, $this->menu_types);
	}

	abstract public function match($message_obj, $role_obj, $session_obj);
	abstract public function execute($message_obj, $role_obj, $session_obj);
}

/** end **/