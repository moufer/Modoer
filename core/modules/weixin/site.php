<?php
!defined('IN_MUDDER') && exit('Access Denied');

$sid = _get('sid',0,MF_INT_KEY);

$role_obj = mc_weixin_role::read_by_sid($sid);
if(!$role_obj) redirect('对不起，商家尚未开通微站。');

$site_mdl = _G('loader')->model('weixin:site');
$site_data = $site_mdl->read_by_sid($sid);

if(!$site_data||!$site_data['status']) redirect('对不起，商家尚未设置或者开通微站。');
if(!$site_data['template_id']) redirect('对不起，商家尚未设置微站模板。');

$template_file = weixin_template('index', $site_data['template_id']);
if(!$template_file) redirect('对不起，商家尚未设置微站模板。');

$_SITE = $site_data;
$_SITE['config'] = ms_mxml::to_array($site_data['config']);
$_SITE['navs'] = ms_mxml::to_array($site_data['navs']);
$_SITE['pics'] = ms_mxml::to_array($site_data['pics']);

include $template_file;

/* end */