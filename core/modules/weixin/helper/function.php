<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

function weixin_template($filename, $templateid = '', $update = false)
{
    if(!$templateid||$templateid<1) return;
    return template($filename, 'weixin', $templateid, '', $update);
}

/* end */
?>
