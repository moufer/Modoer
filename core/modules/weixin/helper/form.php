<?php
/**
 * @author moufer<moufer@163.com>
 * @copyright www.modoer.com
 */

/**
 * 获取指令类列表
 * @param  string $select 默认选择指令类支
 * @param  string $role_type   被指令类支持的使用对象（subject=微站商家,site=官方网站）
 * @param  string $menu_type   被指令类支持指令类型(click,...)
 * @param  string $role_obj     公众平台配置信息
 * @return string
 */
function form_weixin_cmds($select = '', $role_type = 'site', $menu_type = '', $role_obj = null)
{
	$cmd_manage_obj = new mc_weixin_cmd_manage();
	$cmds = $cmd_manage_obj->get_installed_cmds();

	if($cmds) foreach($cmds as $key => $val) {
		$tmp = new $val;
		if($role_obj) {
			$desc = $tmp->describe_by_role_obj($role_obj, $menu_type);
		} elseif($role_type) {
			$desc = $tmp->describe_by_role_type($role_type, $menu_type);
		} else {
			$desc = $tmp->describe();
		}
		if($desc && !is_array($desc[0])) $desc = array($desc);
		if($desc) foreach($desc as $val) {
			$selected = $val['keyword'] == $select ? ' selected' : '';
			$content .= "\t<option value=\"{$val['keyword']}\"$selected>{$val['name']}({$val['keyword']})</option>\r\n";
		}
		unset($tmp);
	}
	return $content;
}

/**
 * 手机web模块页面快捷链接
 * @param  [type] $role_obj [description]
 * @return [type]           [description]
 */
function form_weixin_site_links($role_obj)
{
	$links = array();
	$links[] = array(
		'title'=>'首页',
		'url' => U("mobile/index",true),
	);
	$hooklinks = _G('hook')->hook('mobile_index_link',null,TRUE);
	foreach ($hooklinks as $link) {
		$links[]=$link;
	}
	$content='';
	foreach ($links as $val) {
		$url = $val['url'];
		if(substr($url, 0,7)!='http://') $url = trim(S('siteurl'),'/').$url;
		$content .= "\t<option value=\"{$url}\">{$val['title']}</option>\n";
	}

	return $content;
}

/**
 * 当前微站的快捷
 * @return [type] [description]
 */
function form_weixin_subject_links($role_obj)
{
	$links = array();
	$links[] = array(
		'title'=>'微站首页',
		'url' => U("weixin/site/sid/{$role_obj->role_id}",true),
	);
	$links[] = array(
		'title'=>'商铺首页',
		'url' => U("item/mobile/do/detail/id/{$role_obj->role_id}",true),
	);

	if(check_module('product') && _G('modules','product','extra')) {
		$links[] = array(
			'title'=>'商品列表页',
			'url' => U("product/mobile/do/shop/sid/{$role_obj->role_id}",true),
		);
	}

	$content='';
	foreach ($links as $val) {
		$content .= "\t<option value=\"{$val['url']}\">{$val['title']}</option>\n";
	}

	return $content;
}

/** end **/