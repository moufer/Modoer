<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');
define('SCRIPTNAV', 'coupon');

$sid = _get('sid', null, MF_INT); //主题ID

$S = G('loader')->model('item:subject');
$subject = $S->read($sid);
if(empty($subject)) redirect('item_empty');
$fullname = $subject['name'] . ($subject['subname']?"($subject[subname])":'');
$subject_field_table_tr = $S->display_sidefield($subject);

$CO = G('loader')->model(':coupon');
$where = array();
$where['c.sid'] = $sid;
$where['c.status'] = 1;
$offset = $MOD['listnum'] > 0 ? $MOD['listnum'] : 10;
$start = get_start($_GET['page'], $offset);
$select = 'c.couponid,c.catid,c.sid,c.thumb,c.subject,c.starttime,c.endtime,c.des,c.comments,c.pageview,c.effect1';
$orders = array('effect1'=>'DESC','pageview'=>'DESC','dateline'=>'DESC','comments'=>'DESC');
$_GET['order'] = isset($_GET['order']) && in_array($_GET['order'],array_keys($orders)) ? $_GET['order'] : 'dateline';
list($total,$list) = $CO->find($select, $where, 
	array('c.'.$_GET['order']=>$orders[$_GET['order']]),$start,$offset,TRUE,'s.name,s.subname,s.reviews');
if($total) $multipage = mobile_page($total, $offset, $_GET['page'],  
	url("coupon/mobile/do/item/sid/$sid/page/_PAGE_"));



if($_G['in_ajax']) {
	include mobile_template('coupon_list_li');
	output();
}

$active = array();
$active['catid'][(int)$catid] = ' class="selected"';
$active['orderby'][$orderby] = ' class="selected"';

include mobile_template('coupon_item');