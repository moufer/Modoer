<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

class hook_coupon extends ms_base {

    function __construct(&$hook) {
        parent::__construct();
        $hook->register(
            array('subject_detail_link','mobile_subject_link','mobile_index_link'), 
            $this
        );
    }

	function subject_detail_link(&$params) {
		$grade_cls = mc_item_grade::create($params);
		if($grade_cls->coupon_disabled) return;
		extract($params);
		$IB =& $this->loader->model('item:itembase');
		$model = $IB->get_model($pid,true);
		$title = '优惠券';
		return array (
			'flag' => 'coupon',
			'url' => url('coupon/item/sid/'.$sid),
			'title'=> $title,
		);
	}

	//手机主题页导航
	function mobile_subject_link($params) {
		$title = '优惠券';
		return array (
			'flag' => 'coupon',
			'url' => url('coupon/mobile/do/item/sid/'.$params['sid']),
			'title'=> $title,
		);
	}

	function mobile_index_link() {
		$title = '优惠券';
		$result[] = array (
			'flag' => 'coupon',
			'url' => url('coupon/mobile/do/list'),
			'title'=> '优惠券',
			'icon' => 'coupon',
		);
		return $result;
	}

}
?>