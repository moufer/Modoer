<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<tr>
    <td class="altbg1"><strong>禁用主题使用优惠券功能:</strong>当前级别的主题禁止添加优惠券</td>
    <td><?=form_bool('access[coupon_disabled]', $access['coupon_disabled'])?></td>
</tr>