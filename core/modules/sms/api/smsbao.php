<?php
/**
 * @author moufer<moufer@163.com>
 * @copyright (c)2001-2011 Moufersoft
 * @website www.modoer.com
 */
class msm_sms_smsbao extends msm_sms {

    protected $name = '短信宝';
    protected $descrption = '短信宝，仅适用于UTF-8版。<a href="http://www.smsbao.com/" target="_blank">http://www.smsbao.com/</a>';

    private $username = '';
    private $key = '';
    private $customstr = '';
    private $gateurl = 'http://api.smsbao.com/sms?';

    public function __construct($config) {
        parent::__construct($config);
        $this->set_account();
    }

    //发送短信息
    public function send($mobile, $message) {
        if(!$mobile) redirect('手机不能为空');
        if(!$message) redirect('发送内容不能为空');
        $maxlen = 66;
        $len = strlen_ex($message);
        if($len > $maxlen) {
            $cil = ceil($len / $maxlen);
            $messages = array();
            for ($i=0; $i < $cil; $i++) {
                $x = $i * $maxlen;
                $content = csubstr($message, $x, $maxlen);
                $params = $this->_createParam($mobile, $content[0]);
                $result = $this->_send($params);
                if(!$result) return $result;
            }
            return $result;
        } else {
            $params = $this->_createParam($mobile, $message);
            return $this->_send($params);
        }
    }

    protected function create_from() {
        $this->loader->helper('form');
        $elements = array();
        $elements[] =
            array(
            'title' => '用户名',
            'des' => '短信发送帐号名称。',
            'content' => form_input('smsbao_username', $this->config['smsbao_username'], 'txtbox2'),
        );
        $elements[] =
            array(
            'title' => '密码',
            'des' => '短信发送帐号密码。',
            'content' => form_input('smsbao_password', $this->config['smsbao_password'], 'txtbox2'),
        );
        return $elements;
    }

    protected function check_from() {
        $post = array();
        $post['smsbao_username'] = _post('smsbao_username',null,MF_TEXT);
        $post['smsbao_password'] = _post('smsbao_password',null,MF_TEXT);
        if(!$post['smsbao_username']) redirect('用户名为空！');
        if(!$post['smsbao_password']) redirect('密码为空！');
        return $post;
    }

    //设置短信帐号
    private function set_account() {
        $this->username = $this->config['smsbao_username'];
        $this->key = $this->config['smsbao_password'];
    }

    //生成短信接口的参数格式
    private function _createParam($mobile, $message) {
        $params = array (
            'u'  => $this->username,
            'p'  => md5($this->key),
            'm'  => $mobile,
            'c'   => $message
        );
        return $params;
    }

    //通过http协议的get短信息，并返回api的反馈信息(写入log文件，以便调试)
    private function _send($data) {
        $fields_string = '';
        foreach($data as $key => $value){
            $fields_string .= "{$key}=".urlencode($value)."&";
        }
        $fields_string = rtrim($fields_string,'&');
        $url = $this->gateurl.$fields_string;
        $result = http_get($url);
        if($this->_return($result)) {
            return true;
        } else {
            $this->writeLog($this->errorCode, $this->errorMsg); //记录发送不成功的返回信息
            return false;
        }
    }

    //判断返回的短信息是否发送成功
    private function _return($value=null) 
    {
       static $errlist = array(
            '0' =>'发送成功',
            '30'=> '密码错误',
            '40'=> '账号不存在',
            '41'=> '余额不足',
            '42'=> '帐号过期',
            '43'=> 'IP地址限制',
            '50'=> '内容含有敏感词',
            '51'=> '手机号码不正确',
        );
       if ($value=='0') {
            $this->errorCode = 0;
            $this->errorMsg = '';
            return TRUE;
        } else {
            $this->errorCode = $value;
            $this->errorMsg = '接口提示：' . $errlist[$value];
            return false;
        }
    }
    
}
?>