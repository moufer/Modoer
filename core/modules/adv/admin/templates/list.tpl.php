<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<div id="body">
<div class="sub-menu">
    <div class="sub-menu-heading">广告模块</div>
    <!--
    <a href="<?=cpurl($module,'config')?>" class="sub-menu-item">模块配置</a>
    <a href="<?=cpurl($module,'place')?>" class="sub-menu-item">广告位管理</a>
    -->
    <a href="<?=cpurl($module,'list')?>" class="sub-menu-item selected">广告数据管理</a>
</div>
<form method="get" action="<?=SELF?>">
    <input type="hidden" name="module" value="<?=$module?>" />
    <input type="hidden" name="act" value="<?=$act?>" />
    <input type="hidden" name="op" value="<?=$op?>" />
    <div class="space">
        <table class="maintable" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="100" class="altbg1">广告位</td>
                <td width="350">
                    <select name="apid">
                        <option value="">全部</option>
                        <?=form_adv_place($_GET['apid'])?>
                    </select>
                </td>
                <td width="100" class="altbg1">所属城市</td>
                <td width="*">
                    <?if($admin->is_founder):?>
                    <select name="city_id">
                        <option value="">不限</option>
                        <?=form_city($_GET['city_id'], TRUE)?>
                    </select>
                    <?else:?>
                    <?=$_CITY['name']?>
                    <?endif;?>
                </td>
            </tr>
            <tr>
                <td class="altbg1">类型</td>
                <td>
                    <select name="sort">
                        <option value="">全部</option>
                        <?=form_adv_sort($_GET['sort'])?>
                    </select>                </td>
                <td class="altbg1">状态</td>
                <td><?=form_bool('enabled', $_GET['enabled']=='Y')?></td>
            </tr>
            <tr>
                <td class="altbg1">结果排序</td>
                <td colspan="3">
                    <select name="orderby">
                        <option value="listorder"<?=$_GET['orderby']=='listorder'?' selected="selected"':''?>>默认排序</option>
                        <option value="adid"<?=$_GET['orderby']=='adid'?' selected="selected"':''?>>ID排序</option>
                    </select>&nbsp;
                    <select name="ordersc">
                        <option value="DESC"<?=$_GET['ordersc']=='DESC'?' selected="selected"':''?>>递减</option>
                        <option value="ASC"<?=$_GET['ordersc']=='ASC'?' selected="selected"':''?>>递增</option>
                    </select>&nbsp;
                    <select name="offset">
                        <option value="20"<?=$_GET['offset']=='20'?' selected="selected"':''?>>每页显示20个</option>
                        <option value="50"<?=$_GET['offset']=='50'?' selected="selected"':''?>>每页显示50个</option>
                        <option value="100"<?=$_GET['offset']=='100'?' selected="selected"':''?>>每页显示100个</option>
                    </select>&nbsp;
                    <button type="submit" value="yes" name="dosubmit" class="btn2">筛选</button>
                </td>
            </tr>
        </table>
    </div>
</form>
<form method="post" action="<?=cpurl($module,$act)?>&" name="myform">
	<div class="space">
		<table class="maintable" trmouse="Y">
            <tr class="altbg1">
				<td width="25">选</td>
				<td width="100">地区</td>
				<td width="80">排序</td>
                <td width="200">广告名称</td>
				<td width="200">广告位</td>
				<td width="60">类型</td>
                <td width="200">有效期</td>
                <td width="80">状态</td>
				<td width="*">操作</td>
			</tr>
            <?if($total):?>
            <?while($val=$list->fetch_array()) {?>
			<tr>
				<td><input type="checkbox" name="adids[]" value="<?=$val['adid']?>" /></td>
				<td><?=display('modoer:area',"aid/$val[city_id]")?></td>
				<td><?=form_input("adv[$val[adid]][listorder]",$val['listorder'],"txtbox5")?></td>
				<td><?=$val['adname']?></td>
				<td><?=$val['name']?></td>
				<td><?=$sorts[$val['sort']]?></td>
				<td><?=date('Y-m-d',$val['begintime'])?>～<?=$val['endtime']?date('Y-m-d',$val['endtime']):'永久'?></td>
                    <td><?=$val['enabled']=='Y'?'已启用':'<span class="font_4">已停用</span>'?></td>
				<td><a href="<?=cpurl($module,$act,'edit',array('adid'=>$val['adid']))?>">编辑</a></td>
            </tr>
            <?}?>
            <?else:?>
            <tr><td colspan="7">暂无信息</td></tr>
            <?endif;?>
		</table>
	</div>
    <div><?=$multipage?></div>
    <center>
        <input type="hidden" name="op" value="update" />
        <input type="hidden" name="dosubmit" value="yes" />
        <?if($total):?>
        <button type="button" class="btn" onclick="easy_submit('myform','update',null);">更新排序</button>&nbsp;
        <button type="button" class="btn" onclick="easy_submit('myform','delete','adids[]');">删除所选</button>&nbsp;
        <?endif;?>
        <button type="button" class="btn secondary" onclick="jslocation('<?=cpurl($module,$act,'add')?>')" />增加广告</button>
    </center>
</form>
</div>