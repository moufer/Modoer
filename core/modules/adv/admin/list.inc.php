<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
(!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied');

$op = _input('op');
$AL =& $_G['loader']->model('adv:list');

switch($op) {
    case 'delete':
        $AL->delete(_post('adids'));
        redirect('global_op_succeed_delete', cpurl($module,$act));
        break;
    case 'add':
        $admin->tplname = cptpl('save', MOD_FLAG);
        break;
	case 'edit':
		$adid = _get('adid',null,'intval');
		if(!$detail = $AL->read($adid)) redirect('adv_empty');
        $admin->tplname = cptpl('save', MOD_FLAG);
        break;
    case 'save':
        if($_POST['do']=='edit') {
            if(!$adid = _post('adid',null,'intval')) redirect(lang('global_sql_keyid_invalid','adid'));
        } else {
            $adid = null;
        }
        $post = $AL->get_post($_POST['ad']);
        $AL->save($post, $adid);
        redirect('global_op_succeed', cpurl($module,$act));
        break;
	case 'update':
		$AL->update(_post('adv'));
		redirect('global_op_succeed',get_forward(cpurl($module, $act)));
		break;
    default:
		$sorts = lang('adv_sort');
        $op = 'list';

        $AL->db->join($AL->table,'al.apid','dbpre_adv_place','ap.apid');
        if(!$admin->is_founder) {
            $AL->db->where('city_id', $admin->check_global() ? array(0,$_CITY['aid']) : $_CITY['aid']);
        } elseif(is_numeric($_GET['city_id'])) {
            $city_id = _get('city_id', null, MF_INT_KEY);
            $AL->db->where('city_id', $city_id);
        }
        if($_GET['apid'] = _get('apid', '', MF_INT_KEY)) {
            $AL->db->where('al.apid', $_GET['apid']);
        }
        if($_GET['sort'] = _get('sort', '', MF_TEXT)) {
            $AL->db->where('al.sort', $_GET['sort']);
        }
        if($_GET['enabled'] = _get('enabled','Y',MF_TEXT)) {
            $AL->db->where('al.enabled', $_GET['enabled']?'Y':'N');
        }
        if($total = $AL->db->count()) {
            $AL->db->sql_roll_back('from,where');
            !$_GET['orderby'] && $_GET['orderby'] = 'listorder';
            !$_GET['ordersc'] && $_GET['ordersc'] = 'ASC';
            $AL->db->order_by($_GET['orderby'], $_GET['ordersc']);
            $AL->db->limit(get_start($_GET['page'], $_GET['offset']), $_GET['offset']);
            $AL->db->select('adid,city_id,al.apid,al.adname,al.begintime,al.endtime,al.sort,al.listorder,al.enabled,ap.name');
            $list = $AL->db->get();
            $multipage = multi($total, $_GET['offset'], $_GET['page'], cpurl($module,$act,'list',$_GET));
        }
        $admin->tplname = cptpl('list', MOD_FLAG);
}

/** end  **/