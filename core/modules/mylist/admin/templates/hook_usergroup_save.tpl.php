<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<tr>
	<td class="altbg1">
		<strong>关闭创建榜单功能:</strong>
		关闭后，当前会员组内会员将无法添加榜单。已经添加的榜单则可以被继续维护。
	</td>
	<td><?=form_bool("access[mylist_add_closed]",$access['mylist_add_closed'])?></td>
</tr>
<tr>
	<td class="altbg1">
		<strong>会员榜单创建数量限制:</strong>
		限制会员创建榜单的数量，留空（默认）按照榜单模块配置里设置的数量。
	</td>
	<td><?=form_input("access[mylist_count_limit]",$access['mylist_count_limit']>0?$access['mylist_count_limit']:'', 'txtbox4')?></td>
</tr>