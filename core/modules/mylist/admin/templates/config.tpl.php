<?php (!defined('IN_ADMIN') || !defined('IN_MUDDER')) && exit('Access Denied'); ?>
<div id="body">
<?=form_begin(cpurl($module,$act))?>
    <div class="space">
        <div class="subtitle"><?=$MOD['name']?>&nbsp;模块配置</div>
        <ul class="cptab">
            <li class="selected" id="btn_config1"><a href="javascript:;" onclick="tabSelect(1,'config');" onfocus="this.blur()">功能设置</a></li>
        </ul>
        <table class="maintable" border="0" cellspacing="0" cellpadding="0" id="config1">
            <tr>
                <td width="45%" class="altbg1">
                    <strong>关闭添加榜单功能:</strong>
                    此处关闭后，前台的添加榜单功能将完全关闭，如果只针对某些会员组关闭，请到会员组权限管理处设置。
                </td>
                <td width="*">
                    <?=form_bool('modcfg[add_closed]', (bool)$modcfg['add_closed']);?>
                </td>
            </tr>
            <tr>
                <td class="altbg1">
                    <strong>会员榜单创建数量限制:</strong>
                    此处仅设置默认的最大数量，如果针对某些会员组设置数量限制，请到会员组权限管理处设置。默认为20
                </td>
                <td>
                    <?=form_input("modcfg[count_limit]",$modcfg['count_limit']>0?$modcfg['count_limit']:20, 'txtbox4')?>
                </td>
            </tr>
        </table>
    </div>
    <center><?=form_submit('dosubmit',lang('admincp_submit'),'yes','btn')?></center>
<?=form_end()?>
</div>