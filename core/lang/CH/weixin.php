<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
return array(

    'weixin_template_title' => '微信微站模板管理',

    'weixin_config_mp_empty' => '您尚未配置网站的微信公众号。',

    'weixin_notice_mp_on' => '恭喜！您的微信公众号（%s）已被允许绑定商户。',
    'weixin_notice_mp_off' => '很遗憾，您的微信公众号（%s）绑定已被网站管理员关闭，关闭理由：%s',
    'weixin_notice_mp_delete' => '很遗憾，您的微信公众号（%s）绑定记录和微站信息已被管理员删除，关闭理由：%s',
);
?>