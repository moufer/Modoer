<?php
return array(

    'pwucenter_member_checkname_-1' => '用户名不合法',
    'pwucenter_member_checkname_-2' => '包含要允许注册的词语',
    'pwucenter_member_checkname_-3' => '<span class="font_1">用户名已经存在</span>',

    'pwucenter_member_checkemail_-4' => 'Email 格式有误',
    'pwucenter_member_checkemail_-5' => 'Email 不允许注册',
    'pwucenter_member_checkemail_-6' => '该 Email 已经被注册',

    'pwucenter_member_add_-1' => '对不起，您输入的用户名不合法，请返回。',
    'pwucenter_member_add_-2' => '对不起，您输入的用户名包含不允许注册的词语，请返回。',
    'pwucenter_member_add_-3' => '对不起，您输入的用户名已经存在，请返回。',
    'pwucenter_member_add_-4' => '对不起，您输入的 Email 格式有误，请返回。',
    'pwucenter_member_add_-5' => '对不起，您输入的 Email 不允许注册，请返回。',
    'pwucenter_member_add_-6' => '对不起，您输入的 Email 已经被注册，请返回。',

    'pwucenter_member_edit_-1' => '旧密码不正确，请返回。',
    'pwucenter_member_edit_-4' => 'Email 格式有误，请返回。',
    'pwucenter_member_edit_-5' => 'Email 不允许注册，请返回。',
    'pwucenter_member_edit_-6' => '该 Email 已经被注册，请返回。',
    'pwucenter_member_edit_-7' => '没有做任何修改，请返回。',
    'pwucenter_member_edit_-8' => '该用户受保护无权限更改，请返回。',

    'pwucenter_login_-1' => '对不起，用户不存在，或者被删除。',
    'pwucenter_login_-2' => '对不起，您输入的用户名或密码错误。',
    'pwucenter_login_-3' => '对不起，您输入的安全提问错误。',

    'pwucenter_activation_invalid' => '对不起，激活参数无效。',
    'pwucenter_activation_error' => '很遗憾，帐号激活失败。',
    'pwucenter_activation_succeed' => '恭喜您，帐号激活成功！',
);
?>