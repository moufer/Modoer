<?php
/**
* 图片延迟加载处理
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

class ms_delayed_load
{
	/**
	 * 准备替换的内容
	 * @var string
	 */
	protected $content = '';
	/**
	 * 一些参数设置
	 * @var null
	 */
	protected $options = null;

	/**
	 * 实例化內链处理累
	 * @param string $content 准备替换的内容
	 * @param array $links   內链链接
	 * @param array $options 参数
	 */
	public function __construct($content, $options = null)
	{
		$this->set_content($content);
		if(!$options) $this->set_option($options);
	}

	/**
	 * 设置准备替换的文本内容
	 * @param string $content
	 */
	public function set_content($content)
	{
		$this->content= $content;
	}

	/**
	 * 设置一个或一组配置参数（未使用）
	 * @param string $variable 参数名称
	 * @param mixed $value    参数值
	 */
	public function set_option($variable, $value = null)
	{
		if(!$this->options) $this->options = new ms_attr;

		if(!is_array($value) || is_null($value)) return;
		if(is_array($variable)) {
			foreach ($variable as $key => $val) {
				$this->options->set_attr($key, $val);
			}
		} else {
			$this->options->set_attr($variable, $value);
		}
		return true;
	}

	/**
	 * 对文本内的img标签图片进行延迟加载
	 * @return string 已经加入延迟标签的文本内容
	 */
	public function process()
	{
		//返回 最终内容
		return $this->repalce_img($content);
	}

	protected function repalce_img($content)
	{
		return preg_replace_callback('/<img(.*?)src=["|\'](.+)["|\'](.*?)>/i', function($match){
			return "<img{$match[1]}src=\"{$match[2]}\"{$match[3]}>";
		}, $content);
	}

}

/** end */