<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');
require_once(MUDDER_ROOT.'api'.DS.'third_party'.DS.'geetest'.DS.'class.geetest.php');

class ms_geetest {

    protected static $geetest = null;

    public function get_class()
    {
        static $geetest = null;

        if($geetest) return $geetest;

        if(!S('geetest_id')) return false;
        if(!S('geetest_key')) return false;

        $geetest = new Geetest();
        $geetest->set_captchaid(S('geetest_id'));
        $geetest->set_privatekey(S('geetest_key'));

        return $geetest;
    }

    public function is_enabled()
    {
        $cls = static::get_class();
        return $cls && $cls->register();
    }

    public function embed($ssl = false, $get_url = false)
    {
        if(!$cls = static::get_class()) return false;
        return $cls->get_widget('embed', '', $ssl, $get_url);
    }

    public function float($ssl = false, $get_url = false)
    {
        if(!$cls = static::get_class()) return false;
        return $cls->get_widget('float', '', $ssl, $get_url);
    }

    public function popup($popup_id = 'show_seccode', $ssl = false, $get_url = false)
    {
        if(!$cls = static::get_class()) return false;
        return $cls->get_widget('popup',$popup_id, $ssl, $get_url);
    }

    public function validate($keypre = 'geetest')
    {
        if(!$cls = static::get_class()) return false;

        $challenge = _post('geetest_challenge');
        $validate = _post('geetest_validate');
        $seccode = _post('geetest_seccode');

        return $cls->validate($challenge, $validate, $seccode);
    }
}

/* end */