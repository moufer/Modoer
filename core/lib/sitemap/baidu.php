<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
class sitemap_baidu extends se_sitemap  {

    public function set_url($array) {
        $content = "<url>\n";
        $content .= "\t<loc>{$array['loc']}</loc>\n";
        $content .= "\t<lastmod>{$array['lastmod']}</lastmod>\n";
        $content .= "\t<data>{$array['data']}</data>\n";
        $content .= "</url>";
        $this->_urls[] = $content;
        return $this;
    }

    public function get_url_xml() {

        $content = '<?xml version="1.0" encoding="utf-8"?>';
        //$content = str_replace('UTF-8', $this->_charset, $content) . "\n";
        $content .= "\n<urlset>\n";
        if($this->_urls) {
            $index = 1;
            foreach ($this->_urls as $urls) {
                $content .= $urls."\n";
                if($index++ > 50000) break;
            }
        }
        $content .= "</urlset>";

        return $content;
    }
}
?>