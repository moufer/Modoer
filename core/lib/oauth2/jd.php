<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

/**
* JD登录二次封装
*/
class oauth2_jd extends ms_oauth2
{
	public $name = 'jd';
	
	private $openid = '';

	function __construct()
	{
		parent::__construct();
		if(!class_exists('JDOAuthV2')) {
			require(MUDDER_ROOT.'api'.DS.'auth_callback'.DS.'jd'.DS.'oauth2_class.php');
		}
		$this->auth = new JDOAuthV2(
			S('member:passport_jd_appkey'),
			S('member:passport_jd_appappsecret')
		);
	}

	function getAuthorizeURL($callback_url)
	{
		return $this->auth->getAuthorizeURL($callback_url);
	}

	function getAccessToken($callback_url)
	{
		$token = $this->auth->getAccessToken( $_REQUEST['code'] , $callback_url);
		if(!$token) return;
        $access_token = _T($token->access_token);
        $expires_in = _G('timestamp') + $token->expires_in;

		$newtoken = array(
			'name' 			=> $this->user_nick, 
			'id'			=> $token->uid,
			'expires_in' 	=> $expires_in,		//授权剩余时间
			'access_token' 	=> $access_token, 
		);
		return $this->_createToken($newtoken);
	}

	function getUserInfo($token)
	{
	    $userinfo = array();
	    $userinfo['username'] = $token->name;
	    $userinfo['passport_id'] = $token->uid;

	    if(strtoupper(_G('charset')) != 'UTF-8') {
	        foreach($userinfo as $k => $v) {
	            if(is_string($v)) $userinfo[$k] = charset_convert($v,'utf-8',_G('charset'));
	        }
	    }

	    return $userinfo;
	}
}

/** end **/