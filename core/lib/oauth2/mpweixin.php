<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

/**
* 
*/
class oauth2_mpweixin extends ms_oauth2
{
	public $name = 'mpweixin';
	
	private $openid = '';

	function __construct()
	{
		parent::__construct();
		if(!class_exists('MPWeiXinOAuth2')) {
			require(MUDDER_ROOT.'api'.DS.'auth_callback'.DS.'mpweixin'.DS.'oauth2_class.php');
		}
		$this->auth = new MPWeiXinOAuth2(
			S('member:passport_mpweixin_appid'),
			S('member:passport_mpweixin_appsecret')
		);
	}

	function getAuthorizeURL($callback_url)
	{
		return $this->auth->getAuthorizeURL($callback_url);
	}

	function getAccessToken($callback_url)
	{
		$token = $this->auth->getAccessToken( $_REQUEST['code'] , $callback_url);
		if($token && $token['openid']) {
			G('session')->token_code = $_REQUEST['code'];
			G('session')->token_data = $token;
		} else {
			if(G('session')->token_code == $_REQUEST['code'] && G('session')->token_data) {
				$token = G('session')->token_data?:null;
				if($token) {
					@log_write('weixin_login', array('code:'.$_REQUEST['code'], json_encode($token)));
				}
			}
		}
		//log_write('weixin_gettoken', $token?$token:'false');
		if(!$token || $token['errcode']) return;
		$access_token = _T($token['access_token']);
		$expires_in = _G('timestamp') + $token['expires_in'];
		$this->openid = $access_token ? _T($token['openid']) : '';

		$newtoken = array(
			'name' 			=> $this->name, 
			'id'			=> $this->openid,
			'access_token' 	=> $access_token, 
			'expires_in' 	=> $expires_in,
			'refresh_token' 	=> $token['refresh_token'],
			'scope' 			=> $token['scope'],
			'unionid'		=> $token['unionid'],
		);
		return $this->_createToken($newtoken);
	}

	function getUserInfo($token)
	{
		if(!$this->openid) {
			$this->openid = $token->id;
		}
		$c = new MPWeiXinClient(
				S('member:passport_mpweixin_appid'),
				S('member:passport_mpweixin_appsecret'), 
				$token->access_token, 
				$this->openid
		);
		$me = $c->get_user_info();

		$userinfo = $me?:array();
		$userinfo['username'] = $me['nickname'];
		$userinfo['passport_id'] = $me['openid'];
		$userinfo['face'] = $me['headimgurl'];
		$userinfo['email'] = '';

		return $userinfo;
	}
}