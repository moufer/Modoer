<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

/**
* 
*/
class oauth2_weixin extends ms_oauth2
{
	public $name = 'weixin';
	
	private $openid = '';

	function __construct()
	{
		parent::__construct();
		if(!class_exists('WeiXinOAuthV2')) {
			require(MUDDER_ROOT.'api'.DS.'auth_callback'.DS.'weixin'.DS.'oauth2_class.php');
		}
		$this->auth = new WeiXinOAuthV2(
			S('member:passport_weixin_appid'),
			S('member:passport_weixin_appsecret')
		);
	}

	function getAuthorizeURL($callback_url)
	{
		return $this->auth->getAuthorizeURL($callback_url);
	}

	function getAccessToken($callback_url)
	{
		$token = $this->auth->getAccessToken( $_REQUEST['code'] , $callback_url);
		if(!$token || $token['errcode']) return;
		$access_token = _T($token['access_token']);
		$expires_in = _G('timestamp') + $token['expires_in'];
		$this->openid = $access_token ? _T($token['openid']) : '';

		$newtoken = array(
			'name' 			=> $this->name, 
			'id'			=> $this->openid,
			'access_token' 	=> $access_token, 
			'expires_in' 	=> $expires_in,
			'refresh_token' 	=> $token['refresh_token'],
			'scope' 			=> $token['scope'],
		);
		return $this->_createToken($newtoken);
	}

	function getUserInfo($token)
	{
		if(!$this->openid) {
			$this->openid = $token->id;
		}
		$c = new WeiXinClientV2(
				S('member:passport_weixin_appid'),
				S('member:passport_weixin_appsecret'), 
				$token->access_token, 
				$this->openid
		);
		$me = $c->get_user_info();
		if(strtoupper(_G('charset')) != 'UTF-8') {
			foreach($me as $k => $v) {
				if(is_string($v)) $me[$k] = charset_convert($v,'utf-8',_G('charset'));
			}
		}

		$userinfo = array();
		$userinfo['username'] = $me['nickname'];
		$userinfo['passport_id'] = $me['openid'];
		$userinfo['face'] = $me['headimgurl'];
		$userinfo['unionid'] = $me['unionid'];
		$userinfo['email'] = '';

		return $userinfo;
	}
}