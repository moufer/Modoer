<?php

/**
 * 模块类
 */
class ms_module extends ms_base
{
    //static $modules = null;

    /**
     * 获取全部模块列表
     * @return array
     */
    static function get_modules()
    {
        return G('modules');

        // if(is_null(static::$modules)) {
        // 	static::$modules = _G('loader')->variable('modules');
        // }
        // return static::$modules;
    }

    /**
     * 取得一个模块信息
     * @param string $module_flag 模块标识
     * @param string $key 获取模块的信息简明，例如：name,moduleid 等@all表示以数组方式返回全部信息
     * @return array
     */
    static function get_module($module_flag, $key = '@all')
    {
        $modules = static::get_modules();
        return isset($modules[$module_flag]) ? ($key == '@all' ? $modules[$module_flag] : $modules[$module_flag][$key]) : null;
    }

    /**
     * 检测模块是否存在且已经启用
     * @param string $module_flag 模块标识
     * @return boolean
     */
    static function exists($module_flag)
    {
        if ($module_flag == 'modoer' || $module_flag == 'index') return true;
        $module = static::get_module($module_flag);
        return !is_null($module) && is_array($module) && !$module['disable'];
    }

}

/** end **/