<?php
/**
* redisSession
*/
class ms_redis_session
{
	public static $_this = null;

	public static function start($return_class = true)
	{
		global $_G;
		if(!class_exists("redis")){
			show_error("PHP必须安装redis扩展。");
		}
		if(!$_G['redis']['enabled']){
			show_error("系统配置未启用redis。");
		}
		ini_set("session.save_handler", "redis");
		ini_set("session.save_path","tcp://{$_G['redis']['host']}:{$_G['redis']['port']}");
		ini_set("session.cookie_domain",G('cookiedomain'));
		session_start();
		//返回一个封装的session操作类
		if($return_class) {
			if(!static::$_this) static::$_this = new self;
			return static::$_this;
		}
	}

	//从session中设置自定义变量
	public function __set($key, $value)
	{
		if($value === null) {
			$this->clear($key);
		} else {
			$_SESSION[$key] = $value;
		}
	}

	//从session中获取自定义变量
	public function __get($key)
	{
		if(isset($_SESSION[$key])) {
			return $_SESSION[$key];
		} else {
			return;
		}
	}

	public function clear($key)
	{
		if(isset($_SESSION[$key])) unset($_SESSION[$key]);
	}

	//获取全部session保存的自定义参数
	public function fetch_all()
	{
		return $_SESSION;
	}

	//获取session_id
	public function get_id()
	{
		return session_id();
	}

	//获取sesion表主键ID
	public function get_keyid()
	{
		return session_id();
	}

	//设置session_id关联的UID
	public function set_uid($uid)
	{
		return true;
	}

	//获取在线人数，基于session数量进行统计
	public function get_online_total()
	{
		//PHPREDIS_SESSION
		return 0;
	}

} 

/** end **/