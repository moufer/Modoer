<?php
/**
 * http user agent 解析类
 * @authors moufer (moufer@163.com)
 * @date    2015-01-04 13:35:53
 * @version $Id$
 */

class ms_http_agent {
	
	//判断是否手机浏览器
	function is_mobile()
	{
		static $result = null;
		if(is_bool($result)) return $result;
		if($ua = strtolower($_SERVER['HTTP_USER_AGENT'])) {
			$uachar = "/(mobile|iphone|ipad|android|webos|ios|wap|blackberry|meizu|mobi|uc)/i";
			if(preg_match($uachar, $ua)) {
				$result = true;
			}
		}
		$result = false;
		return $result;
	}

	//判断是否ios系统
	function is_ios()
	{
		static $result = null;
		if(is_bool($result)) return $result;
		if($ua = strtolower($_SERVER['HTTP_USER_AGENT'])) {
			$uachar = "/(iphone|ipad|ios)/i";
			if(preg_match($uachar, $ua)) {
				$result = true;
			}
		}
		$result = false;
		return $result;
	}

	//判断是否搜索引擎蜘蛛
	function is_spider()
	{
		static $result = null;
		if(is_bool($result)) return $result;
		$agent = strtolower($_SERVER['HTTP_USER_AGENT']);
		if (!empty($agent)) {
			$spiderSite = array (
				"TencentTraveler", "Baiduspider+", "BaiduGame", "Googlebot", "msnbot", "Sosospider+", "Sogou web spider", "ia_archiver",
				"Yahoo! Slurp", "YoudaoBot", "Yahoo Slurp", "MSNBot", "Java (Often spam bot)", "BaiDuSpider", "Voila", "Yandex bot", "BSpider",
				"twiceler", "Sogou Spider", "Speedy Spider", "Google AdSense", "Heritrix", "Python-urllib", "Alexa (IA Archiver)", "Ask",
				"Exabot", "Custo", "OutfoxBot/YodaoBot", "yacy", "SurveyBot", "legs", "lwp-trivial", "Nutch", "StackRambler", "The web archive (IA Archiver)",
				"Perl tool", "MJ12bot", "Netcraft", "MSIECrawler", "WGet tools", "larbin", "Fish search",
			);
			foreach($spiderSite as $val) {
				$str = strtolower($val);
				if (strpos($agent, $str) !== false) {
					$result = true;
				}
			}
		} else {
			$result = false;
		}
		return $result;
	}

}