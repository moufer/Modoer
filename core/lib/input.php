<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
class ms_input {

    var $_get = null;
    var $_post = null;
    var $_cookie = null;
    var $_files = null;
    var $_server = null;
    var $_env = null;

    function __construct() {
        $this->set_name();
    }

    function _clear_xss() {
    }

    function _clear_sql() {
        if(!defined('IN_ADMIN')) {
            $_POST = strip_sql($_POST);
            $_GET = strip_sql($_GET);
            $_COOKIE = strip_sql($_COOKIE);
        }
    }

    function _slashes() {
    }

}
?>