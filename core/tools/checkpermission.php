<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2011 Moufersoft
* @website www.modoer.com
*/
_G('loader')->model('tool',FALSE);
class msm_tool_checkpermission extends msm_tool {

    protected $name = '检查文件系统权限';
    protected $descrption = '检测系统文件夹和文件权限是否正常';
    protected $acttype = 'other';

    private $files = array();

    public function run() {
        $this->_check()->display();
        output();
    }

    private function _check() {

        $accessfiles = array('data','data/config.php','data/backupdata','data/cachefiles','data/datacall','data/logs',
            'data/templates','data/templates/block','uploads');

        if(S('ucenter:uc_enable')) {
            array_push($accessfiles, 'uc_client/data/cache');
        }

        $result = array();

        foreach ($accessfiles as $file) {
            $checkfile = MUDDER_ROOT.$file;
            if(!file_exists($checkfile)) {
                $result[] = array($file,'<span style="color:red;">不存在</span>');
            } elseif(!is_readable($checkfile)) {
                $result[] = array($file,'<span style="color:red;">不可读</span>');
            } elseif(!is__writable($checkfile)) {
                $result[] = array($file,'<span style="color:red;">不可写</span>');
            } else {
                $result[] = array($file,'<span style="color:green;">OK</span>');
            }
        }

        $this->files = $result;

        return $this;
    }

    private function display() {
        if(!$this->files) redirect('没有找到任何文件。');
        echo '<table width="100%" border="1" cellspacing="1" cellpadding="5" style="border:1px solid #ccc">';
        echo '<tr><th width="*">文件名</th>'.
            '<th width="120">需要权限</th>'.
            '<th width="120">当前权限</th></tr>';
        foreach ($this->files as $file) {
            echo '<tr><td>'.str_replace(MUDDER_ROOT,DS,$file[0])
            .'</td><td>可写'
            .'</td><td>'.$file[1].'</td></tr>';
        }
        echo '</table>';
    }

}
/* end */