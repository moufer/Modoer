<?php
/**
* @author moufer<moufer@163.com>
* @copyright (c)2001-2009 Moufersoft
* @website www.modoer.com
*/
class msm_template extends ms_model {

    var $table = 'dbpre_templates';
    var $key = 'templateid';

    function __construct() {
        parent::__construct();
    }

    function update($post, $tpltype='main') {
        foreach($post as $templateid => $val) {
            $val['tpltype'] = $tpltype;
            $this->save($val, $templateid, FALSE);
        }
        $this->write_cache();
    }

    function delete($ids) {
        if(is_array($ids)) {
            $this->db->where_in('templateid', $ids);
            $this->db->from($this->table);
            $this->db->delete();
        } else {
            parent::delete($ids);
        }
        $this->write_cache();
    }

    /**
     * 获取某个类型下的已安装模板数据
     * @param  string $type 模板类型
     * @return array       模板数据列表
     */
    function read_all($type='main')
    {
        return $this->db->where("tpltype", $type)
            ->from($this->table)
            ->get_all();
    }

    /**
     * 获取某个类型下的模板文件夹
     * @param  string $type 模板类型(main,item等等)
     * @return array       模板文件夹名（一个文件夹表示一套模板）
     */
    function load_dirs($type='main')
    {
        if(!$type) return $this->add_error('未指定模板类型。');
        $result = array();
        $directory = MUDDER_TEMPLATE.$type.DS;
        try {
            $iterator = new DirectoryIterator ( $directory );
            foreach ( $iterator as $info ) {
                if($info->isDir() && !$info->isDot() && $info->getFilename()!='default') {
                    $result[] = $info->getFilename();
                }
            }
        } catch (Exception $e) {
            return $this->add_error($e->getMessage());
        }
        return $result;
    }

    function write_cache($return = FALSE) {
        $result = array();
        $this->db->from($this->table);
        $this->db->order_by(array('tpltype'=>'ASC','templateid'=>'ASC'));
        $row = $this->db->get();
        while($value = $row->fetch_array()) {
            $result[$value['tpltype']][$value['templateid']] = $value;
        }
        $row->free_result();
        ms_cache::factory()->write('modoer_templates', $result);
        //write_cache('templates', arrayeval($result));
        if($return) return $result;
    }

    function check_post(&$post) {
        if(!$post['name']) redirect('admincp_template_empty_name');
        if(!$post['directory']) redirect('admincp_template_empty_dir');
        if(!$post['tpltype']) redirect('admincp_template_empty_type');
        if(!$this->_is_template_dir($post['directory'], $post['tpltype'])) redirect('admincp_template_invalid');
    }

    function post_file_content() {
        if(!$content = trim($_POST['content'])) redirect('admincp_template_edit_content_empty');
        if(isset($_POST['filedir'])) { //new
            if(!$_POST['filename']) redirect('admincp_template_add_filename_empty');
            $filename = MUDDER_ROOT . $_POST['filedir'] . $_POST['filename'];
            if(file_exists($filename)) redirect(lang('global_file_exists'));
            file_put_contents($filename, $content);
            chmod($filename, 0777);
        } else { //edit
            if(!$_POST['filename']) redirect('admincp_template_edit_filename_empty');
            file_put_contents($_POST['filename'], $content);
            chmod($_POST['filename'], 0777);
        }
    }

    function delete_files($files) {
        if(!$files||!is_array($files)) redirect('global_op_unselect');
        foreach($files as $f) {
            if(!$f || strlen($f) < 10 || !is_file(MUDDER_ROOT . $f)) continue;
            @unlink(MUDDER_ROOT . $f);
        }
    }

    function manage($root_dir, $files) {
        if(!strposex($root_dir, 'templates')) {
            redirect('只能修改模板文件名称。');
        }
        if(!$root_dir || strlen($root_dir) < 10) redirect('admincp_template_manage_rootdir_empty');
        if(!$files||!is_array($files)) redirect('global_op_nothing');
        $template_des = array();
        foreach($files as $val) {
            $template_des[$val['newfilename']] = $val['des'];
            if($val['filename'] == $val['newfilename']) continue;
            if(!$val['filename'] || !$val['newfilename']) redirect('admincp_template_manage_filename_empty');
            if(!is_file(MUDDER_ROOT . $root_dir . DS . $val['filename'])) redirect(lang('global_file_not_exist', $val['filename']));
            rename(MUDDER_ROOT . $root_dir . DS . $val['filename'], MUDDER_ROOT . $root_dir . DS . $val['newfilename']);
        }
        $this->_write_des($root_dir, $template_des);
    }

    function _is_template_dir($directory, $type = 'main') {
        $dir = MUDDER_ROOT . 'templates' . DS . $type . DS . $directory;
        return is_dir($dir);
    }

    function _write_des($root_dir, $data) {
        $content = "<?php\r\n!defined('IN_MUDDER') && exit('Access Denied');\r\nreturn " . arrayeval($data) . "; \r\n?>";
        if(!@file_put_contents(MUDDER_ROOT.$root_dir.DS.'template.php', $content)) {
            redirect('文件'.$root_dir.DS.'template.php写入失败请检查文件权限。');
        }
    }
}
?>