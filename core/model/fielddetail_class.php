<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');

class msm_fielddetail extends ms_model {

	// 字段属性
	var $field = array();
	var $config = array();
	// 全部数据
	var $all_data = null;
	var $pagemod = 'detail';
	var $format = "";
	var $style = "";
	var $width = "*";
	var $class = "";
	var $align = "right";
	var $td_num = 2;
	var $nourl = false;

	var $note_width = "150";
	
	var $value = "";
	var $empty_exception_types = array();

	function __construct() {
		parent::__construct();
	}

	function all_data($data) {
		$this->all_data = $data;
	}

	function style() {
		$style = '';
		if($this->width) $style .= " width='$this->width'";
		if($this->class) $style .= " class='$this->class'";
		if($this->align) $style .= " align='$this->align'";
		$this->style = $style;

		$this->format = "<tr>\r\n";
		$this->format .= "\t<td $this->style>%s：" . ($this->td_num > 1 ? "</td>\r\n" : "");
		$this->format .= ($this->td_num > 1 ? "\t<td width=\"*\">" : "")."%s</td>\r\n";
		$this->format .= "</tr>\r\n";
	}
	
	function exception_add($type)
	{
		if(is_array($type)) {
			$this->empty_exception_types = array_merge($this->empty_exception_types,$type);
		} else {
			$this->empty_exception_types[] = $type;
		}
		array_unique($this->empty_exception_types);
	}

	function detail($field, $val=null, $no_format = false)
	{
		$content = '';
		if(empty($field)) return $content;
		if($no_format) {
			$this->format = '%s{{|}}%s';
		} else  {
			$this->style();
		}
		$this->field = $field;
		$this->config = $field['config'];
		!is_array($this->config) && (array)$this->config;
		$fun = '_'.$field['type'];
		if(empty($val) && !in_array($field['type'], $this->empty_exception_types)) {
			return;
		}
		$this->value = $val;
		if(!$fun && !$val) return;
		$result = $this->$fun($val);
		if($this->nourl) {
			//$result = preg_replace("/<a[^>]*href=[^>]*>|<\/[^a]*a[^>]*>/i", "", $result);
		}
		if($no_format) {
			$result = substr($result, strpos($result, '{{|}}')+5);
		}
		return $result;
	}

	function __template_parse(& $val)
	{
		$template = $this->field['template'];
		if(is_string($this->field['template']) && strposex($template,'<?xml')) {
			$template = ms_mxml::to_array($template);
		}
		if(G('in_mobile')) {
			$template = isset($template['mobile'])&&$template['mobile']?$template['mobile']:(is_array($template)?$template['pc']:$template);
		} else {
			$template = is_array($template)?$template['pc']:$template;
		}
		if($template) {
			_G('loader')->helper('passport');
			$re = array('{value}','{urlcode:value}','{city_name}','{encode:value}');
			$va = array($val, rawurlencode($val), $this->global['city']['name'], rawurlencode(passport_encrypt($val,S('authkey'))));
			$str = str_replace($re, $va, $template);
			$str = preg_replace_callback("/\{display:([a-z]+):([a-z0-9A-Z\_]+)(.*)\}/i", array(&$this, '__template_parse_func'), $str);
		} else {
			$str = $val;
		}
		/*
		if(preg_match_all("/\{display:([a-z]+):([a-z0-9A-Z\_]+)\}/i", $str, $match)) {
			$params = array('value'=>$val,'config'=>$this->config,'field'=>$this->field);
			$module = $match[1][0];
			$func = $match[2][0];
			foreach($match as $k => $v) {
				if(!$k) continue;
			}
			
			$module = $match[0][1];
			$func = $match[0][2];
			$this->loader->helper('display',$module);
			$class = 'display_' . $module;
			$result = call_user_func(array($class, $func), $params);
			preg_replace("/display:$module:$func/i", $result, $str);
		}
		*/
	
		return $str;
	}
	
	function __template_parse_func($match) {
		if(isset($match[3])) {
			//存在参数
			$pa = explode(' ', trim($match[3]));
			$params = array();
			foreach ($pa as $value) {
				list($k,$v) = explode('=', $value);
				$params[$k] = $v;
			}
		}
		$params = array('value'=>$this->value,'config'=>$this->config,'field'=>$this->field,'params'=>$params);
		if($match[1]=='modoer') {
			$module = '';
			$class = 'display';
		} else {
			$module = $match[1];
			$class = 'display_' . $module;
		}
		$func = $match[2];
		$this->loader->helper('display', $module);
		$result = call_user_func(array($class, $func), $params);
		return $result;
	}

	function _text($val) {
		$text = $val . ($this->field['unit'] ? "&nbsp;{$this->field['unit']}" : '');
		if($this->field['template']) {
			$text = $this->__template_parse($text);
		}
		return sprintf($this->format, $this->field['title'], $text);
	}

	function _numeric($val) {
		return $this->_text($val);
	}

	function _textarea($val) {
		return $this->_text($val);
	}

	function _option($val) {
		$fun = '__' . $this->config['type'];
		if(!method_exists($this, $fun)) return;
		$val = $this->$fun($val);
		return $this->_text($val);
	}

	function _date($val) {
		return $this->_text(newdate($val,$this->config['format']));
	}

	function _area($val) {
		$A =& $this->loader->model('area');
		if(!$city_id = $A->get_parent_aid($val,1)) return;
		if(!$area = $this->loader->variable('area_' . $city_id)) return;
		// 载入地区
		if($area[$val]['level'] == 2) {
			$paid = 0;
		} else {
			$paid = $area[$val]['pid'];
		}
		$urlpath = array();
		if($paid) {
			$urlpath[] = url_path($area[$paid]['name'], url('item/list/catid/' . SUBJECT_CATID . '/aid/'.$paid));
		}
		if($paid != $val) {
			$urlpath[] = url_path($area[$val]['name'], url('item/list/catid/' . SUBJECT_CATID . '/aid/'.$val));
		}
		if($this->nourl) {
			$content = preg_replace("/<a[^>]*href=[^>]*>|<\/[^a]*a[^>]*>/i","",implode('&nbsp;&gt;&nbsp;', $urlpath));
		}else {
			$content = implode('&nbsp;&gt;&nbsp;', $urlpath);
		}
		return sprintf($this->format, $this->field['title'], $content);
	}

	function __select($val) {
		$option = "";
		$list = explode("\r\n", preg_replace("/\s*(\r\n|\n\r|\n|\r)\s*/", "\r\n", $this->config['value']));
		foreach($list as $sval) {
			$v = explode("=",$sval);
			if($v[0] == $val) {
				$option = $v[1];
			}
		}
		if(!$option) $option = "N/A";
		return $option;
	}

	function __check($val) {
		if($val) $val = explode(",", $val);
		$option = "";
		$list = explode("\r\n", preg_replace("/\s*(\r\n|\n\r|\n|\r)\s*/", "\r\n", $this->config['value']));
		!$this->config['display_split'] && $this->config['display_split'] = '&nbsp;';
		if($this->config['display_split'])
		foreach($list as $sval) {
			$v = explode("=",$sval);
			if($val && in_array($v[0], $val)) {
				$option .= $split . $v[1];
				$split = $this->config['display_split'];
			}
		}
		if(!$option) $option = "N/A";
		return $option;
	}

	function __radio($val) {
		return $this->__select($val);
	}
}

/** end */