<?php
/**
 * 系统全局变量声明
 * @author moufer<moufer@163.com>
 * @copyright (C)2001-2012 Moufersoft
 */

//运行标记,始终保持TRUE
define('IN_MUDDER',	TRUE);	

//本地运行,正式建站,请设置为FALSE
define('IN_LOCAL',	FALSE);

//调试模式,正式建站,请设置为FALSE
define('DEBUG',		FALSE);

//调试模式下打开PHP报错信息
if(DEBUG) {
	error_reporting(E_ALL & ~(E_STRICT|E_NOTICE|E_WARNING));
	@ini_set('display_errors', 'On');
} else {
	error_reporting(0);
	@ini_set('display_errors', 'Off');
}

//文件夹路径定义
define('DS',		DIRECTORY_SEPARATOR);
define('MUDDER_CORE',		__DIR__.DS);
define('MUDDER_ROOT',		dirname(MUDDER_CORE).DS);
define('MUDDER_DATA',		MUDDER_ROOT.'data'.DS);
define('MUDDER_CACHE',		MUDDER_DATA.'cachefiles'.DS);
define('MUDDER_MODULE',		MUDDER_CORE.'modules'.DS);
define('MUDDER_PLUGIN',		MUDDER_CORE.'plugins'.DS);
define('MUDDER_UPLOAD',		MUDDER_ROOT.'uploads'.DS);
define('MUDDER_TEMPLATE',	MUDDER_ROOT.'templates'.DS);
define('MUDDER_DOMAIN',		$_SERVER['HTTP_HOST'] ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME']);

/* end */