<?php
class msubb {

	function clear($content) {
		return preg_replace("/\[\/\S+?\/\]/", "", $content);
	}
	
	function pares($content, $detail = null) {
        $content = msubb::smilies($content);
        $content = msubb::username($content);
        $content = msubb::image($content, $detail['pictures']);
        $content = msubb::video($content);
        //return nl2br(str_replace('&#13;', "\n", $content));
        return nl2br($content);
    }

    function smilies($content) {
        return preg_replace("/\[\/([0-9]{1,2})\/\]/", "<img class=\"ubb-face\" src=\"".URLROOT."/static/images/smilies/\\1.gif\" />", $content);
    }

    function username($content) {
        	$username_arr = msubb::get_username($content);
        	if(!$username_arr) return $content;
        	$search = $replace = array();
        	foreach ($username_arr as $k => $value) {
        		$search[$k] = "[/@$value/]";
        		$replace[$k] = "<a href='".url("space/index/username/$value")."' target=\"_blank\">@$value</a>";
        	}
        	return str_replace($search, $replace, $content);
    }

    function image($content, $pictures) {
        $pictures = is_serialized($pictures) ? unserialize($pictures) : '';
        if(!$pictures) return $content;
        $img_arr = msubb::get_image($content);
        if(!$img_arr) return $content;
        $search = $replace = array();
        foreach ($img_arr as $k => $value) {
            $imgsrc = $pictures[$value];
            if(!$imgsrc) continue;
            $search[$k] = "[/img:$value/]";
            $replace[$k] = "<img src=\"".URLROOT."/$imgsrc\" class=\"ubb_show_image\" />";
        }
        return str_replace($search, $replace, $content);
    }

    function video($content) {
        $video_arr = msubb::get_video($content);
        if(!$video_arr) return $content;
        $search = $replace = array();
        foreach ($video_arr as $k => $value) {
            $search[$k] = $value['replace'];
            //检测是否允许播放的域名
            if(check_flash_domain($value['play'])) {
                $text = '视频加载中...';
                if(is_ios()) {
                    $text = isset($value['url']) ? '<a href="'.$value['url'].'">进入播放页面</a>' : '视频不支持ios系统播放';
                }
                $params = "'video':'{$value['play']}'";
                if(isset($value['url'])) $params .= ",'url':'{$value['url']}'";
                if(is_mobile()) $text = self::_write_video($value['play']);
                $replace[$k] = "<div class=\"show_video\" id=\"video_$k\" params=\"{{$params}}\">".$text."</div>";
            } else {
                $replace[$k] = "<div class=\"block_video\">视频已屏蔽，管理员限制了 ".get_fl_domain($value['play'])."  下的视频在本站显示。</div>";
            }
        }
        return str_replace($search, $replace, $content);
    }

    function get_username($content) {
    	 if ( ! preg_match_all('%\[/@(\S+?)/\]%', $content, $matches)) return;
    	 return $matches[1];
    }

    function get_image($content) {
         if ( ! preg_match_all('%\[/img:(\S+?)/\]%', $content, $matches)) return;
         return $matches[1];
    }

    function get_video($content) {
         if ( ! preg_match_all('%\[/video:(.*?)/\]%', $content, $matches)) { 
            return;
        }
        $result = array();
        foreach ($matches[1] as $key => $value) {
            if(preg_match('/^url=(\S+)\s+play=(\S+)$/i', $value, $match)) {
                $result[] = array('replace'=>$matches[0][$key], 'url'=>$match[1], 'play'=>$match[2]);
            } else {
                $result[] = array('replace'=>$matches[0][$key], 'play'=>$value);
            }
        }
        
        return $result;
    }

    function _write_video($play, $width=500, $height=350) {
        $content = '<embed type="application/x-shockwave-flash" src="'.$play.'" width="'.$width.'" height="'.$height.'" style="undefined" bgcolor="#FFF" quality="high" allowscriptaccess="never" allowfullscreen="true" wmode="transparent">';
        return false;
    }
}
?>