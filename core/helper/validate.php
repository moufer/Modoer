<?php
/**
* @author moufer<moufer@163.com>
* @copyright www.modoer.com
*/
!defined('IN_MUDDER') && exit('Access Denied');
class validate 
{

    /**
     * 验证IP地址是否为标准的IPV4格式
     * @param  string  $ip IP地址
     * @return boolean
     */
    function is_ip($ip) 
    {
        return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
    }

    /**
     * 邮箱有效性验证
     * @param  string  $email 
     * @return boolean
     */
    function is_email($email) 
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * 验证是否是个URL地址
     * @param  string  $url
     * @return boolean
     */
    function is_url($url) 
    {
        return filter_var($url, FILTER_VALIDATE_URL);
    }

    function is_date($date) 
    {
        return preg_match("/^[0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}$/", $date);
    }

    function is_datetime($datetime) 
    {
        return preg_match("/^[0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}\s[0-9]{1,2}:[0-9]{1,2}(:[0-9]{1,2}|)$/", $datetime);
    }

    function is_numeric($number, $alow_zero = 1, $alow_minus = 1) 
    {
        if(!is_numeric($number)) return false;
        if(!$alow_zero && !$number) return false;
        if(!$alow_minus && $number < 0) return false;
        return true;
    }

    function is_mobile($mobile) 
    {
        $preg_match = S('sms:preg_match');
        $match = $preg_match ?: '/^1[1-9]{1}[0-9]{9}$/';
        return preg_match($match, $mobile);
    }

    /**
     * 身份证号验证
     * @param  string  $idcard
     * @return boolean
     */
    function is_idcard($idcard)
    {
        if(empty($idcard)){
            return false;
        }
        $City = array(11=>"北京",12=>"天津",13=>"河北",14=>"山西",15=>"内蒙古",21=>"辽宁",22=>"吉林",23=>"黑龙江",
            31=>"上海",32=>"江苏",33=>"浙江",34=>"安徽",35=>"福建",36=>"江西",37=>"山东",41=>"河南",42=>"湖北",
            43=>"湖南",44=>"广东",45=>"广西",46=>"海南",50=>"重庆",51=>"四川",52=>"贵州",53=>"云南",54=>"西藏",
            61=>"陕西",62=>"甘肃",63=>"青海",64=>"宁夏",65=>"新疆",71=>"台湾",81=>"香港",82=>"澳门",91=>"国外");
        $iSum = 0;
        $idCardLength = strlen($idcard);
        //长度验证
        if(!preg_match('/^\d{17}(\d|x)$/i',$idcard) and !preg_match('/^\d{15}$/i',$idcard)) {
            return false;
        }
        //地区验证
        if(!array_key_exists(intval(substr($idcard,0,2)),$City)) {
           return false;
        }
        // 15位身份证验证生日，转换为18位
        if ($idCardLength == 15) {
            $sBirthday = '19'.substr($idcard,6,2).'-'.substr($idcard,8,2).'-'.substr($idcard,10,2);
            $d = new DateTime($sBirthday);
            $dd = $d->format('Y-m-d');
            if($sBirthday != $dd){
                return false;
            }
            $idcard = substr($idcard, 0, 6)."19".substr($idcard, 6, 9); //15to18
            $Bit18 = validate::getVerifyBit($idcard);//算出第18位校验码
            $idcard = $idcard.$Bit18;
        }
        // 判断是否大于2078年，小于1900年
        $year = substr($idcard, 6, 4);
        if ($year < 1900 || $year > 2078 ) {
            return false;
        }

        //18位身份证处理
        $sBirthday = substr($idcard,6,4).'-'.substr($idcard,10,2).'-'.substr($idcard,12,2);
        $d = new DateTime($sBirthday);
        $dd = $d->format('Y-m-d');
        if($sBirthday != $dd) {
            return false;
         }
        //身份证编码规范验证
        $idcard_base = substr($idcard,0,17);
        if(strtoupper(substr($idcard,17,1)) != validate::getVerifyBit($idcard_base)) {
           return false;
        }
        return $idcard;
    }

    // 计算身份证校验码，根据国家标准GB 11643-1999
    function getVerifyBit($idcard_base) 
    {
        if(strlen($idcard_base) != 17) {
            return false;
        }
        //加权因子
        $factor = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
        //校验码对应值
        $verify_number_list = array('1', '0', 'X', '9', '8', '7', '6', '5', '4','3', '2');
        $checksum = 0;
        for ($i = 0; $i < strlen($idcard_base); $i++) {
            $checksum += substr($idcard_base, $i, 1) * $factor[$i];
        }
        $mod = $checksum % 11;
        $verify_number = $verify_number_list[$mod];
        return $verify_number;
    }

}

/* end */