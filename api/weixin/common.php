<?php
define('URLROOT_PRE', '/api/weixin');
require dirname(dirname(__DIR__)).'/core/init.php';

if(DEBUG) {
	log_write('weixin_debug', request_uri()."\n".arrayeval($_GET)."\n".$GLOBALS['HTTP_RAW_POST_DATA']);	
}

//网站未打开绑定功能
if(!check_module('weixin')) {
	exit;
}

$_G['in_ajax'] = true;
/** end */