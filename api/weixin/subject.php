<?php
require __DIR__.'/common.php';

$role_obj = mc_weixin_role::read_by_sid((int)$_GET['sid']);
if($role_obj->check_use_bind()) {
	$role_obj->get_weixin()->response();
} else {
	log_write('weixin', request_uri()."\n{$role_obj->role_id} \t not bind.");
	exit;
}

/** end **/