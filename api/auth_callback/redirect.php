<?php

if(!$_GET) exit('invaild');

//$auth_name = 'google';
$get = $_GET;

$initfile = realpath('../../../').'/core/init.php';
require($initfile);

//$auth = ms_oauth2::factory($auth_name);
//$auth->getAccessToken();

$callbackurl = _G('session')->callbackurl;
if(!$callbackurl) {
	//default callback url
	$callbackurl = S('siteurl')."index.php?m=member&act=passport&op=callback&type=".$auth_name;
} else {
	$callbackurl = str_replace(array('&amp;','&#38;'), '&', $callbackurl);
}

//append params
foreach ($get as $key => $value) {
	$callbackurl .= "&$key=".urlencode($value);
}

//redirect
location($callbackurl);

/* end */