<?php

error_reporting(0);

error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));
@ini_set('display_errors', 'On');

@define('IN_MUDDER', TRUE);
@define('IN_UCENTER', TRUE);
define('DS', DIRECTORY_SEPARATOR);

define('P_W','admincp');

define('MUDDER_ROOT', substr(dirname(__FILE__), 0, -3));

$_G = array();
$_G['pwucenter'] = include MUDDER_ROOT.'./data/cachefiles/pwucenter_config.php';
if(!$_G['pwucenter']['enable']) exit('Invalid Request');
require_once MUDDER_ROOT.'./data/config_pw.php';
if(strlen(UC_KEY)<6) exit('Invalid Request');
$_G['cfg'] = include MUDDER_ROOT.'./data/cachefiles/modoer_config.php';
require_once MUDDER_ROOT.'./data/config.php';
require_once MUDDER_ROOT.'./api/api_common.php';

$config = array();
$config['uc_appid'] = $uc_appid = UC_APPID;
$config['uc_key'] = $uc_key = UC_KEY;

define('URLROOT', get_urlroot());
define('TIMESTAMP', time());
$_G['timestamp'] = TIMESTAMP;
$timestamp = TIMESTAMP;

define('R_P',MUDDER_ROOT);
define('D_P',R_P);

require_once(R_P.'/api/pw_api/security.php');
require_once(R_P.'/api/pw_api/pw_common.php');
require_once(R_P.'/api/pw_api/class_base.php');

log_write('pw_api', request_uri()."\n".arrayeval($_GET)."\n".arrayeval($_POST)."\n==================\n");

$mysql = new ms_activerecord($_G['dns']);

$api = new api_client();
$response = $api->run($_POST + $_GET);

if ($response) {
	echo $api->dataFormat($response);
}

/** end **/