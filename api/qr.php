<?php
define('MUDDER_ROOT', substr(dirname(__FILE__), 0, -3));
require MUDDER_ROOT . "/core/lib/qrcode.php";
$value = $_GET['content'];
$mps = isset($_GET['mps'])?(int)$_GET['mps']:5;
header("Content-type: image/png");
$errorCorrectionLevel = 'L';
$matrixPointSize = $mps > 1 ? $mps : 5;
QRcode::png($value, false, $errorCorrectionLevel, $matrixPointSize);
exit;