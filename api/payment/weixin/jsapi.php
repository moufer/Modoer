<?php
define('URLROOT_PRE', '/api/payment/weixin');
require dirname(dirname(dirname(__DIR__))).'/core/init.php';

//获取code码，以获取openid
$code = $_GET['code'];
if(!$code) {
	redirect('Code代码未指定。');
}

$wxpay = G('session')->wxpay;
if(!$wxpay||!$wxpay['payid']) {
	redirect('未指定PayID');
}
$payid = $wxpay['payid'];
if(!$pay = G('loader')->model(':pay')->read($payid)) {
	redirect('pay_order_empty');
} elseif($pay['status']) {
	redirect('pay_order_error_status_'.$pay['status']);
}
$succeed_url = $pay['callback_url_mobile']?:$pay['callback_url'];

include_once("sdk/WxPayPubHelper.php");

$payment = mc_pay::factory('wxpay', true);
$config = $payment::create_config();
//使用jsapi接口
$jsApi = new JsApi_pub($config);
$jsApi->setCode($code);
$openid = $jsApi->getOpenId();
if(!$openid) {
	redirect('Openid获取失败。');
}
//G('session')->wxpay_openid = $openid;

//使用统一支付接口
$unifiedOrder = new UnifiedOrder_pub($config);

//设置必填参数
$unifiedOrder->setParameter("openid","$openid");//商品描述
$unifiedOrder->setParameter("body",$wxpay['body']);//商品描述
$unifiedOrder->setParameter("out_trade_no",$wxpay['out_trade_no']);//商户订单号 
$unifiedOrder->setParameter("total_fee",$pay['price']*100);//总金额
$unifiedOrder->setParameter("notify_url",$config->notify_url);//通知地址 
$unifiedOrder->setParameter("trade_type","JSAPI");//交易类型

$prepay_id = $unifiedOrder->getPrepayId();
//=========步骤3：使用jsapi调起支付============
$jsApi->setPrepayId($prepay_id);

$jsApiParameters = $jsApi->getParameters();
//echo $jsApiParameters;
?>

<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" >
    <title>微信安全支付</title>

	<script type="text/javascript">

		function jslocation (url) {
			url = url.replace(/(&amp;)|(&#38;)/ig, "&");
			location.href = url;
		}

		//调用微信JS api 支付
		function jsApiCall() {
			WeixinJSBridge.invoke(
				'getBrandWCPayRequest',
				<?php echo $jsApiParameters; ?>,
				function(res){
					WeixinJSBridge.log(res.err_msg);
					//console.debug(res.err_code+res.err_desc+res.err_msg);
					switch (res.err_msg){
						case 'get_brand_wcpay_request:ok': 
							alert('支付完成！');
							jslocation("<?=$succeed_url?>"); 
							break;
						default:
							alert(res.err_msg);
							break;
					}
				}
			);
		}

		function callpay() {
			if (typeof WeixinJSBridge == "undefined"){
			    if( document.addEventListener ){
			        document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
			    }else if (document.attachEvent){
			        document.attachEvent('WeixinJSBridgeReady', jsApiCall); 
			        document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
			    }
			}else{
			    jsApiCall();
			}
		}
	</script>

	<style type="text/css">
		a {
			color:white;
		}
		.btn {
			display:block;
			margin:10px 0; 
			width:95%;
			height:50px;
			line-height:50px;
			cursor: pointer;  
			color:white;  
			font-size:16px;
			text-decoration: none;
		}
		.pay_btn {
			background-color:#FE6714; 
			border:0px #FE6714 solid;
		}
		.oth_brn {
			background-color:green; 
			border:0px #FE6714 green;
		}
	</style>
</head>
<body>
	<p style="line-height:25px;font-size:14px;margin:10 auto;width:95%;">
		订单号：<?=$wxpay['out_trade_no']?></br>
		订单标题：<?=$wxpay['body']?></br>
		订单价格：<?=$pay['price']?> 元
	</p>
	<div align="center">
		<a class="btn pay_btn" href="#" onclick="callpay()" >微信支付</a>
		<a class="btn oth_brn" href="<?=U('member/mobile')?>" >我的助手</a>
	</div>
</body>
</html>