<?php
define('URLROOT_PRE', '/api/payment/weixin');
require dirname(dirname(dirname(__DIR__))).'/core/init.php';

$payid = _input('payid');
if(!$pay = G('loader')->model(':pay')->read($payid)) redirect('pay_order_empty');
if($pay['status']) redirect('pay_order_error_status_'.$pay['status']);

$total_fee = _input('total_fee','',MF_TEXT);
$out_trade_no = _input('out_trade_no','',MF_TEXT);
$body = _input('body','',MF_TEXT);

include_once("sdk/WxPayPubHelper.php");

$payment = mc_pay::factory('wxpay', true);
$config = $payment::create_config();

$unifiedOrder = new UnifiedOrder_pub($config);

//设置统一支付接口参数
$unifiedOrder->setParameter("body","$body");//商品描述
$unifiedOrder->setParameter("out_trade_no","$out_trade_no");//商户订单号 
$unifiedOrder->setParameter("total_fee",$pay['price']*100);//总金额
$unifiedOrder->setParameter("notify_url",$config->notify_url);//通知地址 
$unifiedOrder->setParameter("trade_type","NATIVE");//交易类型

//获取统一支付接口结果
$unifiedOrderResult = $unifiedOrder->getResult();

//商户根据实际情况设置相应的处理流程
if ($unifiedOrderResult["return_code"] == "FAIL") 
{
	//商户自行增加处理流程
	echo "通信出错：".$unifiedOrderResult['return_msg']."<br>";
}
elseif($unifiedOrderResult["result_code"] == "FAIL")
{
	//商户自行增加处理流程
	echo "错误代码：".$unifiedOrderResult['err_code']."<br>";
	echo "错误代码描述：".$unifiedOrderResult['err_code_des']."<br>";
}
elseif($unifiedOrderResult["code_url"] != NULL)
{
	//从统一支付接口获取到code_url
	$code_url = $unifiedOrderResult["code_url"];
	//商户自行增加处理流程
	$succeed_url = $pay['callback_url_mobile']?:$pay['callback_url'];
}

?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<title>微信安全支付</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" >
</head>
<body>
	<div align="center" id="qrcode">
	</div>
	<div align="center">
		<p>订单：<?php echo $body; ?></p>
		<p>订单号：<?php echo $out_trade_no; ?></p>
	</div>
	<div align="center">
		<a href="<?php echo U('index')?>">返回首页</a>
		|
		<a href="<?php echo U('member/index')?>">我的助手</a>
		<?if($succeed_url):?>
		|
		<a href="<?php echo U('member/index')?>">支付成功</a>
		<?endif;?>
	</div>
</body>
	<script src="./qrcode.js"></script>
	<script>
		if(<?php echo $unifiedOrderResult["code_url"] != NULL; ?>)
		{
			var url = "<?php echo $code_url;?>";
			//参数1表示图像大小，取值范围1-10；参数2表示质量，取值范围'L','M','Q','H'
			var qr = qrcode(10, 'Q');
			qr.addData(url);
			qr.make();
			var wording=document.createElement('p');
			wording.innerHTML = "请使用微信扫一扫支付<br><br>手机微信访问时长按二维码图片识别";
			var code=document.createElement('DIV');
			code.innerHTML = qr.createImgTag();
			var element=document.getElementById("qrcode");
			element.appendChild(wording);
			element.appendChild(code);
		}
	</script>
</html>